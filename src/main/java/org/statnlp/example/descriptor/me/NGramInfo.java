package org.statnlp.example.descriptor.me;

public class NGramInfo {

	public int fIdx;
	public int start;
	public int end;
	
	public NGramInfo(int fIdx, int start, int end) {
		this.fIdx = fIdx;
		this.start = start;
		this.end = end;
	}
}
