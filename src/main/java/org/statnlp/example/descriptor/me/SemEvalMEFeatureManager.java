package org.statnlp.example.descriptor.me;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.example.descriptor.emb.WordEmbedding;
import org.statnlp.example.descriptor.me.SemEvalMENetworkCompiler.NodeType;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;


public class SemEvalMEFeatureManager extends FeatureManager {

	private static final long serialVersionUID = -2015365232682775780L;

	private enum FeaType {
		a1, a1w, a2, a2w, a1a2w, betw, a1t, a2t, a1a2t, a1left, a1right, a2left, a2right,
		dist,bettseq, loutside, routside, pref5bet,
		a1hypernym, a2hypernym, bethypernym, a1a2hypernym, a1ner, a2ner, betner, emb, betbg, bettg}
	
	WordEmbedding emb = null;
	
	/**Store the test features**/
	public List<List<NGramInfo>> testFeats;
	
	public SemEvalMEFeatureManager(GlobalNetworkParam param_g, WordEmbedding emb, List<List<NGramInfo>> testFeats) {
		super(param_g);
		this.emb = emb;
		this.testFeats = testFeats;
	}
	
	@Override
	protected FeatureArray extract_helper(Network network, int parent_k, int[] children_k, int children_k_index) {
		int[] paArr = network.getNodeArray(parent_k);
		int nodeType = paArr[0];
		if (nodeType != NodeType.NODE.ordinal())
			return FeatureArray.EMPTY;
		RelInstance inst = (RelInstance)network.getInstance();
		CandidatePair input = inst.getInput();
		Sentence sent = input.sent;

		int leftSpanIdx = input.leftSpanIdx;
		int rightSpanIdx = input.rightSpanIdx;
		int arg1SpanIdx = leftSpanIdx;
		int arg2SpanIdx = rightSpanIdx;
		Span arg1Span = inst.getInput().spans.get(arg1SpanIdx);
		Span arg2Span = inst.getInput().spans.get(arg2SpanIdx);
		FeatureArray startFa = this.createFeatureArray(network, new int[0]);
		FeatureArray currFa = startFa;
		int relId = paArr[1];
		String relForm = RelationType.get(relId).form;
		currFa = this.addRelFeatures(currFa, network, sent, arg1Span, arg2Span, relForm, parent_k, children_k_index);
		currFa = this.addHypernymFeatures(currFa, network, sent, arg1Span, arg2Span, relForm, parent_k, children_k_index);
		currFa = this.addNERFeatures(currFa, network, sent, arg1Span, arg2Span, relForm, parent_k, children_k_index);
		if (this.emb != null) {
			this.addEmbeddingFeats(network, currFa, parent_k, children_k_index, relId, sent, arg1Span, arg2Span, input, relForm);
		}
		return startFa;
	}
	
	private void addEmbeddingFeats(Network network, FeatureArray currFa, int parent_k, int children_k_index, 
			int relId, Sentence sent, Span arg1Span, Span arg2Span, CandidatePair input, String relType) {
		if (this.emb != null) {
			List<Integer> embFeats = new ArrayList<>(2 * this.emb.getDimension());
			List<Double> embFeatVal = new ArrayList<>(2 * this.emb.getDimension());
			double[] avg = new double[2 * this.emb.getDimension()];
			for (int i = arg1Span.start; i <= arg1Span.end; i++) {
				double[] curr = this.emb.getEmbedding(sent.get(i).getForm());
				for (int d = 0; d< this.emb.getDimension(); d++) {
					avg[d] += curr[d];
				}
			}
			for (int i = arg2Span.start; i <= arg2Span.end; i++) {
				double[] curr = this.emb.getEmbedding(sent.get(i).getForm());
				for (int d = 0; d< this.emb.getDimension(); d++) {
					avg[this.emb.getDimension() + d] += curr[d];
				}
			}
			for (int d = 0; d < this.emb.getDimension(); d++) {
				avg[d] /= (arg1Span.end - arg1Span.start + 1);
				embFeats.add(this._param_g.toFeature(network, FeaType.emb.name() + "-" + (d+1), relType, ""));
				embFeatVal.add(avg[d]);
			}
			for (int d = this.emb.getDimension(); d < 2 * this.emb.getDimension(); d++) {
				avg[d] /= (arg2Span.end - arg2Span.start + 1);
				embFeats.add(this._param_g.toFeature(network, FeaType.emb.name() + "-" + (d+1), relType, ""));
				embFeatVal.add(avg[d]);
			}
			currFa = currFa.addNext(this.createFeatureArray(network, embFeats, embFeatVal));
		}
	}
	
	private FeatureArray addRelFeatures (FeatureArray currFa, Network network, Sentence sent, 
			Span arg1Span, Span arg2Span, String relType, int parent_k, int children_k_index) {
		String relationLabel = relType;
		//arg1 features
		List<Integer> a1a2 = new ArrayList<>(arg1Span.end - arg1Span.start + arg2Span.end - arg2Span.start  + 2);
		List<Integer> a1s = new ArrayList<>(arg1Span.end - arg1Span.start + 2);
		StringBuilder a1Seq = new StringBuilder("");
		for (int i = arg1Span.start; i <= arg1Span.end; i++) {
			String curr_word = sent.get(i).getForm();
			String curr_tag = sent.get(i).getTag();
			if (i == arg1Span.start)
				a1Seq.append(curr_word);
			else a1Seq.append(" " + curr_word);
			a1s.add(this._param_g.toFeature(network, FeaType.a1w.name(), relationLabel, curr_word));
			a1s.add(this._param_g.toFeature(network, FeaType.a1t.name(), relationLabel, curr_tag));
			a1a2.add(this._param_g.toFeature(network, FeaType.a1a2w.name(), relationLabel, curr_word));
			a1a2.add(this._param_g.toFeature(network, FeaType.a1a2t.name(), relationLabel, curr_tag));
		}
		a1s.add(this._param_g.toFeature(network, FeaType.a1.name(), relationLabel, a1Seq.toString()));
		currFa = currFa.addNext(this.createFeatureArray(network, a1s));

		
		//arg2 features
		List<Integer> a2s = new ArrayList<>(arg2Span.end - arg2Span.start + 2);
		StringBuilder a2Seq = new StringBuilder("");
		for (int i = arg2Span.start; i <= arg2Span.end; i++) {
			String curr_word = sent.get(i).getForm();
			String curr_tag = sent.get(i).getTag();
			if (i == arg2Span.start)
				a2Seq.append(curr_word);
			else a2Seq.append(" " + curr_word);
			a2s.add(this._param_g.toFeature(network, FeaType.a2w.name(), relationLabel, curr_word));
			a2s.add(this._param_g.toFeature(network, FeaType.a2t.name(), relationLabel, curr_tag));
			a1a2.add(this._param_g.toFeature(network, FeaType.a1a2w.name(), relationLabel, curr_word));
			a1a2.add(this._param_g.toFeature(network, FeaType.a1a2t.name(), relationLabel, curr_tag));
		}
		a2s.add(this._param_g.toFeature(network, FeaType.a2.name(), relationLabel, a2Seq.toString()));
		currFa = currFa.addNext(this.createFeatureArray(network, a2s));
		currFa = currFa.addNext(this.createFeatureArray(network, a1a2));
		
		//words between
		boolean isTest = this.isTesting(network.getInstance());
		List<Integer> bet = new ArrayList<>();
		StringBuilder tagSeq = new StringBuilder();
		for (int i = arg1Span.end + 1; i < arg2Span.start; i++) {
			String curr_word = sent.get(i).getForm();
			String curr_tag_f = sent.get(i).getTag().substring(0, 1);
			int fug = this._param_g.toFeature(network, FeaType.betw.name(), relationLabel, curr_word);
			bet.add(fug);
			if (isTest && fug >= 0) {
				synchronized (testFeats) {
					testFeats.get(network.getInstance().getInstanceId() - 1).add(new NGramInfo(fug, i, i));
				}
			}
			if (i == arg1Span.end + 1) {
				tagSeq.append(curr_tag_f);
			} else {
				tagSeq.append("_" + curr_tag_f);
			}
			String pref = curr_word.length() >= 5 ? curr_word.substring(0, 5) : curr_word;
			bet.add(this._param_g.toFeature(network, FeaType.pref5bet.name(), relationLabel, pref));
			
			if (i < arg2Span.start - 1) {
				String rw = sent.get(i + 1).getForm();
				int fbg = this._param_g.toFeature(network, FeaType.betbg.name(), relationLabel, curr_word + "&" + rw);
				bet.add(fbg);
				if (isTest && fbg >= 0) {
					synchronized (testFeats) {
						testFeats.get(network.getInstance().getInstanceId() - 1).add(new NGramInfo(fbg, i, i+1));
					}
				}
				if (i < arg2Span.start - 2) {
					String rrw = sent.get(i + 2).getForm();
					int ftg = this._param_g.toFeature(network, FeaType.bettg.name(), relationLabel, curr_word + "&" + rw + "&" + rrw);
					bet.add(ftg);
					if (isTest && ftg >= 0) {
						synchronized (testFeats) {
							testFeats.get(network.getInstance().getInstanceId() - 1).add(new NGramInfo(ftg, i, i+2));
						}
					}
				}
			}
		}
		bet.add(this._param_g.toFeature(network, FeaType.bettseq.name(), relationLabel, tagSeq.toString()));
		currFa = currFa.addNext(this.createFeatureArray(network, bet));
		
		int[] dis = new int[1];
		dis[0] = this._param_g.toFeature(network, FeaType.dist.name(), relationLabel, (arg2Span.start - arg1Span.end) + "");
		currFa = currFa.addNext(this.createFeatureArray(network, dis));
		
		//word outside
		String arg1lw = arg1Span.start - 1 >= 0 ? sent.get(arg1Span.start - 1).getForm() : "START";
		String arg2rw = arg2Span.end + 1 < sent.length()? sent.get(arg2Span.end + 1).getForm() : "END";
		List<Integer> outside = new ArrayList<>();
		outside.add(this._param_g.toFeature(network, FeaType.loutside.name(), relationLabel, arg1lw));
		outside.add(this._param_g.toFeature(network, FeaType.routside.name(), relationLabel, arg2rw));
		currFa = currFa.addNext(this.createFeatureArray(network, outside));
		
		return currFa;
	}
	
	private FeatureArray addHypernymFeatures (FeatureArray currFa, Network network, Sentence sent, 
			Span arg1Span, Span arg2Span, String relType, int parent_k, int children_k_index) {
		String relationLabel = relType;
		List<Integer> a1a2 = new ArrayList<>(arg1Span.end - arg1Span.start + arg2Span.end - arg2Span.start  + 2);
		List<Integer> a1s = new ArrayList<>(arg1Span.end - arg1Span.start + 2);
		for (int i = arg1Span.start; i <= arg1Span.end; i++) {
			String currHypernym = sent.get(i).getHypernym();
			if(!currHypernym.equals("0")) {
				currHypernym = currHypernym.split("\\.")[1];
				a1s.add(this._param_g.toFeature(network, FeaType.a1hypernym.name(), relationLabel, currHypernym));
				a1a2.add(this._param_g.toFeature(network, FeaType.a1a2hypernym.name(), relationLabel, currHypernym));
			}
		}
		currFa = currFa.addNext(this.createFeatureArray(network, a1s));
		List<Integer> a2s = new ArrayList<>(arg2Span.end - arg2Span.start + 2);
		for (int i = arg2Span.start; i <= arg2Span.end; i++) {
			String currHypernym = sent.get(i).getHypernym();
			if(!currHypernym.equals("0")) {
				currHypernym = currHypernym.split("\\.")[1];
				a2s.add(this._param_g.toFeature(network, FeaType.a2hypernym.name(), relationLabel, currHypernym));
				a1a2.add(this._param_g.toFeature(network, FeaType.a1a2hypernym.name(), relationLabel, currHypernym));
			}
		}
		currFa = currFa.addNext(this.createFeatureArray(network, a2s));
		currFa = currFa.addNext(this.createFeatureArray(network, a1a2));
		List<Integer> bet = new ArrayList<>();
		for (int i = arg1Span.end + 1; i < arg2Span.start; i++) {
			String currHypernym = sent.get(i).getHypernym();
			if(!currHypernym.equals("0")) {
				currHypernym = currHypernym.split("\\.")[1];
				bet.add(this._param_g.toFeature(network, FeaType.bethypernym.name(), relationLabel, currHypernym));
			}
		}
		currFa = currFa.addNext(this.createFeatureArray(network, bet));
		return currFa;
	}
	
	private FeatureArray addNERFeatures (FeatureArray currFa, Network network, Sentence sent, 
			Span arg1Span, Span arg2Span, String relType, int parent_k, int children_k_index) {
		String relationLabel = relType;
		List<Integer> a1s = new ArrayList<>(arg1Span.end - arg1Span.start + 2);
		for (int i = arg1Span.start; i <= arg1Span.end; i++) {
			String currNER = sent.get(i).getEntity();
			if(!currNER.equals("0")) {
				String[] vals = currNER.split(":");
				if (vals.length == 2) currNER = vals[1];
				else if (vals.length == 3) currNER = vals[2];
				else throw new RuntimeException("length not 2 or 3: " + currNER);
				a1s.add(this._param_g.toFeature(network, FeaType.a1ner.name(), relationLabel, currNER));
			}
		}
		currFa = currFa.addNext(this.createFeatureArray(network, a1s));
		List<Integer> a2s = new ArrayList<>(arg2Span.end - arg2Span.start + 2);
		for (int i = arg2Span.start; i <= arg2Span.end; i++) {
			String currNER = sent.get(i).getEntity();
			if(!currNER.equals("0")) {
				String[] vals = currNER.split(":");
				if (vals.length == 2) currNER = vals[1];
				else if (vals.length == 3) currNER = vals[2];
				else throw new RuntimeException("length not 2 or 3: " + currNER);
				a2s.add(this._param_g.toFeature(network, FeaType.a2ner.name(), relationLabel, currNER));
			}
		}
		currFa = currFa.addNext(this.createFeatureArray(network, a2s));
		return currFa;
	}
	
	
	private boolean isTesting(Instance inst) {
		return !inst.isLabeled() && inst.getInstanceId() > 0;
	}

}
