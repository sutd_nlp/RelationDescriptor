package org.statnlp.example.descriptor.me;

import java.util.Arrays;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class SemEvalMENetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -4060742941914357025L;

	public enum NodeType {LEAF, NODE, ROOT};
	private boolean DEBUG = false;
	
	static {
		//nodeType, leftIndex, rightIndex, relation type.
		NetworkIDMapper.setCapacity(new int[]{4, 120});
	}
	
	public SemEvalMENetworkCompiler() {
	}

	private long toNode_leaf() {
		return toNode(NodeType.LEAF, 0); 
	}
	
	private long toNode_Rel(int label) {
		return toNode(NodeType.NODE, label);
	}
	
	private long toNode_root() {
		return toNode(NodeType.ROOT, 0);
	}
	
	private long toNode(NodeType nodeType, int labelId) {
		return NetworkIDMapper.toHybridNodeID(new int[]{nodeType.ordinal(), labelId});
	}
	
	@Override
	public Network compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance lgInst = (RelInstance)inst;
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(lgInst.getOutput().getType().id);
		builder.addNode(node);
		builder.addEdge(node, new long[]{leaf});
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network))
				System.err.println("not contains");
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long root = toNode_root();
		builder.addNode(root);
		for (int l = 0; l < RelationType.RELS.size(); l++) {
			long node = this.toNode_Rel(l);
			builder.addNode(node);
			builder.addEdge(node, new long[]{leaf});
			builder.addEdge(root, new long[]{node});
		}
		return builder.build(networkId, inst, param, this);
	}
	

	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root();
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
		int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
		int labelId = arr[1];
		RelationType predType = RelationType.get(labelId);
		nodeIdx = baseNetwork.getMaxPath(labeledNodeIdx)[0];
		RelationDescriptor prediction = new RelationDescriptor(predType, -1, -1);
		inst.setPrediction(prediction);
		return inst;
	}

}
