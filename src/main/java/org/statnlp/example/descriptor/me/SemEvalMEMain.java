package org.statnlp.example.descriptor.me;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationDescriptorEvaluator;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.example.descriptor.emb.GloveWordEmbedding;
import org.statnlp.example.descriptor.emb.GoogleWordEmbedding;
import org.statnlp.example.descriptor.emb.TurianWordEmbedding;
import org.statnlp.example.descriptor.emb.WordEmbedding;
import org.statnlp.example.descriptor.semeval.SemEvalDataReader;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;
import org.statnlp.hypergraph.neural.GlobalNeuralNetworkParam;
import org.statnlp.hypergraph.neural.NeuralNetworkCore;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class SemEvalMEMain {
	
	public static double l2val = 0.0001;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static String trainFile = "data/semeval/sd_parseTag_train.txt";
	public static String testFile = "data/semeval/sd_parseTag_test.txt";
	public static int trainNum = -1;
	public static int testNum = -1;
	public static String resultFile = "results/me_semeval_res.txt";
	public static boolean saveModel = false;
	public static boolean readModel = false;
	public static String modelFile = "models/me_semeval_model.m";
	public static int embeddingDimension = 300;
	public static String embedding = "random";
	public static OptimizerFactory optimizer = OptimizerFactory.getLBFGSFactory();
	public static boolean readPreLabels = true;
	public static String preLabelsFile = "data/semeval/prevlabels.txt";
	
	public static boolean useEmbFeat = false; //lstm
	public static String googleEmbeddingFile = "nn-crf-interface/neural_server/google/GoogleNews-vectors-negative300.txt";
//	public static String googleEmbeddingFile = "nn-crf-interface/neural_server/google/googleexample.txt";
	public static String gloveEmbeddingFile = "/home/lihao/corpus/embedding/glove.6B.300d.txt";
	public static String turianEmbeddingFile = "nn-crf-interface/neural_server/turian/turian_embeddings.50.txt";
//	public static String gloveEmbeddingFile = "nn-crf-interface/neural_server/google/gloveExample.txt";
	public static String testVocabFile = "data/semeval/sd_parseTag_test_vocab.txt";
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		setArgs(args);
		
		/***
		 * Parameter settings and model configuration
		 */
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.OPTIMIZE_NEURAL = true;
		NetworkConfig.PRINT_BATCH_OBJECTIVE = false;
		NetworkConfig.USE_FEATURE_VALUE = true;
		if (readPreLabels) {
			readPreLabels(preLabelsFile);
		}
		SemEvalDataReader reader = new SemEvalDataReader();

		/**debug info**/
		/***/
		
		Instance[] trainData = reader.read(trainFile, true, trainNum);
		reader.putInMaxLen(trainData);
		Instance[] testData = reader.read(testFile, false, testNum);
		reader.putInMaxLen(testData);
		List<List<NGramInfo>> testFeats = new ArrayList<>();
		for (int i = 0 ; i < testData.length; i++) testFeats.add(new ArrayList<>());
		
		System.out.println(RelationType.RELS.toString());
		System.out.println("#labels: " + RelationType.RELS.size());
		RelationDescriptorEvaluator evaluator = new RelationDescriptorEvaluator(true);
		NetworkModel model = null;
		if (readModel) {
			System.out.println("[Info] Reading Model....");
			ObjectInputStream in = RAWF.objectReader(modelFile);
			model = (NetworkModel)in.readObject();
			WordEmbedding emb = null;
            if (useEmbFeat) {
            	emb = obtainEmbedding();
            }
            SemEvalMEFeatureManager fm = (SemEvalMEFeatureManager)model.getFeatureManager();
            fm.emb = emb;
		} else {
			List<NeuralNetworkCore> nets = new ArrayList<>();
			WordEmbedding emb = null;
            if (useEmbFeat) {
            	emb = obtainEmbedding();
            }
            GlobalNeuralNetworkParam gnnp = new GlobalNeuralNetworkParam(nets);
            GlobalNetworkParam gnp = new GlobalNetworkParam(optimizer, gnnp);
            gnp.setStoreFeatureReps();
            SemEvalMEFeatureManager tfm = new SemEvalMEFeatureManager(gnp, emb, testFeats);
            SemEvalMENetworkCompiler tnc = new SemEvalMENetworkCompiler();
			model = DiscriminativeNetworkModel.create(tfm, tnc);
			model.train(trainData, numIteration);
			gnp.getStringIndex().buildReverseIndex();
		}
		if (saveModel) {
			ObjectOutputStream out = RAWF.objectWriter(modelFile);
			out.writeObject(model);
			out.close();
		}
		
		/**
		 * Testing Phase
		 */
		Instance[] results = model.test(testData);
		evaluator.evaluateRelation(results);
		SemEvalMEFeatureManager fm = (SemEvalMEFeatureManager)model.getFeatureManager();
		GlobalNetworkParam gnp = model.getFeatureManager().getParam_G();
		for (int i = 0; i < results.length; i++) {
			List<NGramInfo> fs = fm.testFeats.get(i);
			double max = Double.NEGATIVE_INFINITY;
			int bestLeft = -1;
			int bestRight = -1;
			for (int f = 0 ; f < fs.size(); f++) {
				NGramInfo ng = fs.get(f);
				double weight = gnp.getWeight(ng.fIdx);
				if (weight > max) {
					bestLeft = ng.start;
					bestRight = ng.end;
					max = weight;
				}
			}
			RelInstance inst = (RelInstance)results[i];
			RelationDescriptor pred = inst.getPrediction();
			pred.setLeft(bestLeft);
			pred.setRight(bestRight);
		}
		
		//print the results
		PrintWriter pw = RAWF.writer(resultFile);
		for (Instance res : results) {
			RelInstance inst = (RelInstance)res;
			Sentence sent = inst.getInput().sent;
			List<Span> spans = inst.getInput().spans;
			Span arg1Span = spans.get(inst.getInput().leftSpanIdx);
			Span arg2Span = spans.get(inst.getInput().rightSpanIdx);
			RelationDescriptor gold = inst.getOutput();
			RelationDescriptor pred = inst.getPrediction();
			String goldForm = gold.getType().form;
			String predForm = pred.getType().form;
			pw.println(sent.toString());
			pw.println(arg1Span.start+","+arg1Span.end + " " + arg2Span.start+","+arg2Span.end + " "
					+ gold.getLeft() + "," + gold.getRight() + " " + pred.getLeft() + "," + pred.getRight());
			pw.println(goldForm + "," + predForm);
			pw.println();
		}
		pw.close();
		
		//Write out scoring results
		/**** Verified. Our evaluation is same as the scorer.
		PrintWriter pw1 = RAWF.writer("results/proposed.txt");
		PrintWriter pw2 = RAWF.writer("results/key.txt");
		for (Instance res : results) {
			RelInstance inst = (RelInstance)res;
			RelationDescriptor gold = inst.getOutput();
			RelationDescriptor pred = inst.getPrediction();
			String goldForm = gold.getType().form.endsWith("-rev") ? gold.getType().form.replace("-rev", "(e2,e1)") : 
				gold.getType().form.equals("Other") ? gold.getType().form : gold.getType().form + "(e1,e2)";
			String predForm = pred.getType().form.endsWith("-rev") ? pred.getType().form.replace("-rev", "(e2,e1)") : 
				pred.getType().form.equals("Other") ? pred.getType().form: pred.getType().form + "(e1,e2)";
			pw1.println(inst.getInstanceId() + "\t" + predForm);
			pw2.println(inst.getInstanceId() + "\t" + goldForm);
		}
		pw1.close();
		pw2.close();
		**/
	}
		
	
	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Latent Linear-chain CRF Model for Relation Extraction");
		parser.addArgument("-t", "--thread").type(Integer.class).setDefault(8).help("number of threads");
		parser.addArgument("--l2").type(Double.class).setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--iter").type(Integer.class).setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--trainFile").setDefault(trainFile).help("The path of the traininigFile file");
		parser.addArgument("--testFile").setDefault(testFile).help("The path of the testFile file");
		parser.addArgument("--trainNum").type(Integer.class).setDefault(trainNum).help("The number of training instances");
		parser.addArgument("--testNum").type(Integer.class).setDefault(testNum).help("The number of testing instances");
		parser.addArgument("--saveModel").type(Boolean.class).setDefault(saveModel).help("whether to save the model");
		parser.addArgument("--readModel").type(Boolean.class).setDefault(readModel).help("whether to read the model");
		parser.addArgument("--modelFile").setDefault(modelFile).help("specify the model file");
		parser.addArgument("--resFile").setDefault(resultFile).help("specify the result file");
		parser.addArgument("--embedding").setDefault(embedding).help("specify the embedding to be used");
		parser.addArgument("--embeddingDimension").type(Integer.class).setDefault(embeddingDimension).help("The dimension of the embeddings.");
		parser.addArgument("--os").setDefault(NetworkConfig.OS).help("linux, osx");
		parser.addArgument("--useEmbFeat").type(Boolean.class).setDefault(useEmbFeat).help("use embedding features");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = ns.getInt("thread");
        l2val = ns.getDouble("l2");
        numIteration = ns.getInt("iter");
        trainFile = ns.getString("trainFile");
        testFile = ns.getString("testFile");
        trainNum = ns.getInt("trainNum");
        testNum = ns.getInt("testNum");
        saveModel = ns.getBoolean("saveModel");
        readModel = ns.getBoolean("readModel") ;
        modelFile = ns.getString("modelFile");
        resultFile = ns.getString("resFile");
        embeddingDimension = ns.getInt("embeddingDimension");
        embedding = ns.getString("embedding");
        useEmbFeat = ns.getBoolean("useEmbFeat");
        NetworkConfig.OS = ns.getString("os");
        if (saveModel && readModel) {
        	throw new RuntimeException("save model and read model can't be both true");
        }
        System.err.println(ns.getAttrs().toString());
	}
	
	/**
	 * Reading the label list
	 * @param file
	 */
	private static void readPreLabels(String file) {
		BufferedReader br;
		try {
			br = RAWF.reader(file);
			String line = null; 
			while ((line = br.readLine()) != null) {
				RelationType.get(line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static WordEmbedding obtainEmbedding() {
		if (embedding.equals("google")) {
			embeddingDimension = 300;
			return new GoogleWordEmbedding(googleEmbeddingFile );
		} else if (embedding.equals("glove")) {
			return new GloveWordEmbedding( gloveEmbeddingFile);
		} else if (embedding.equals("turian")) {
			embeddingDimension = 50;
			return new TurianWordEmbedding(turianEmbeddingFile);
		} else {
			throw new RuntimeException("unknown word embedding type: " + embedding);
		}
	}
}
