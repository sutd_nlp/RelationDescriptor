package org.statnlp.example.descriptor.ace;

import java.util.Arrays;
import java.util.Set;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.Config;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class ACENetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -5848479927223001779L;

	public enum NodeType {LEAF, SPAN, NODE, ROOT};
	private boolean DEBUG = true;
	
	private boolean latentDescriptor;
	private boolean useDescriptor;
	private Set<String> rules;
	private int maxDescriptorLen = 2;
	
	static {
		//nodeType, leftIndex, rightIndex, relation type.
		NetworkIDMapper.setCapacity(new int[]{4, 200, 200, 14});
	}
	
	public ACENetworkCompiler(boolean useLatentSpan, boolean useDescriptor, Set<String> rules) {
		this.latentDescriptor = useLatentSpan;
		this.useDescriptor = useDescriptor;
		this.rules = rules;
	}

	private long toNode_leaf() {
		return toNode(NodeType.LEAF, 0, 0, 0); 
	}
	
	private long toNode_noSpan(int sentLen){
		return toNode(NodeType.SPAN, sentLen, sentLen, 0);
	}
	
	private long toNode_Span(int leftIndex, int rightIndex){
		return toNode(NodeType.SPAN, leftIndex, rightIndex, 0);
	}
	
	private long toNode_Rel(int label) {
		return toNode(NodeType.NODE, 0, 0, label);
	}
	
	private long toNode_root() {
		return toNode(NodeType.ROOT, 0, 0, 0);
	}
	
	private long toNode(NodeType nodeType, int leftIndex, int rightIndex, int labelId) {
		return NetworkIDMapper.toHybridNodeID(new int[]{nodeType.ordinal(), leftIndex, rightIndex, labelId});
	}

	@Override
	public BaseNetwork compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance lgInst = (RelInstance)inst;
		RelationDescriptor descriptor = lgInst.getOutput();
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(lgInst.getOutput().getType().id);
		builder.addNode(node);
		if (this.useDescriptor) {
			if (this.latentDescriptor) {
				if (!lgInst.getOutput().getType().form.equals(Config.NR)) {
					for (int pos = 0; pos < lgInst.size(); pos++) {
						for (int right = pos; right < pos + maxDescriptorLen; right++) {
							long spanNode = this.toNode_Span(pos, right);
							builder.addNode(spanNode);
							builder.addEdge(spanNode, new long[]{leaf});
							builder.addEdge(node, new long[]{spanNode});
						}
					}
				} else {
					long noSpanNode = this.toNode_noSpan(lgInst.size());
					builder.addNode(noSpanNode);
					builder.addEdge(noSpanNode, new long[]{leaf});
					builder.addEdge(node, new long[]{noSpanNode});
				}
			} else {
				long spanNode = -1;
				if (descriptor.getLeft() < 0 && descriptor.getRight() < 0) {
					spanNode = this.toNode_noSpan(lgInst.size());
				} else {
					spanNode = this.toNode_Span(descriptor.getLeft(), descriptor.getRight());
				}
				builder.addNode(spanNode);
				builder.addEdge(spanNode, new long[]{leaf});
				builder.addEdge(node, new long[]{spanNode});
			}
		} else {
			//no descriptor;
			builder.addEdge(node, new long[]{leaf});
		}
		
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network))
				System.err.println("not contains");
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance relInst = (RelInstance) inst;
		CandidatePair cp = relInst.getInput();
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long root = toNode_root();
		builder.addNode(root);
		String leftForm = cp.spans.get(cp.leftSpanIdx).entity;
		String rightForm = cp.spans.get(cp.rightSpanIdx).entity;
		for (int l = 0; l < RelationType.RELS.size(); l++) {
			if (this.rules != null) {
				String rule = RelationType.get(l).form + "," + leftForm + "," + rightForm;
				if (!rules.contains(rule)) continue;
			}
			long node = this.toNode_Rel(l);
			builder.addNode(node);
			if (this.useDescriptor) {
				if (RelationType.get(l).form.equals(Config.NR)) {
					long noSpanNode = this.toNode_noSpan(inst.size());
					builder.addNode(noSpanNode);
					builder.addEdge(noSpanNode, new long[]{leaf});
					builder.addEdge(node, new long[]{noSpanNode});
				} else {
					for (int pos = 0; pos < inst.size(); pos++) {
						for (int right = pos; right < pos + maxDescriptorLen; right++) {
							long spanNode = this.toNode_Span(pos, right);
							builder.addNode(spanNode);
							builder.addEdge(spanNode, new long[]{leaf});
							builder.addEdge(node, new long[]{spanNode});
						}
					}
				}
			} else {
				builder.addEdge(node, new long[]{leaf});
			}
			builder.addEdge(root, new long[]{node});
		}
		return builder.build(networkId, inst, param, this);
	}
	
	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root();
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
		int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
		int labelId = arr[3];
		RelationType predType = RelationType.get(labelId);
		int left = -1;
		int right = -1;
		if (this.useDescriptor) {
			nodeIdx = baseNetwork.getMaxPath(labeledNodeIdx)[0];
			arr = baseNetwork.getNodeArray(nodeIdx);
			left = arr[1];
			right = arr[2];
			if (arr[1] == inst.size()) {
				left = -1;
				right = -1;
			}
		} 
		RelationDescriptor prediction = new RelationDescriptor(predType, left, right);
		inst.setPrediction(prediction);
		return inst;
	}

}
