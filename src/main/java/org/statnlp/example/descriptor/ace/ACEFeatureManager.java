package org.statnlp.example.descriptor.ace;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.Config;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.example.descriptor.ace.ACENetworkCompiler.NodeType;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;

public class ACEFeatureManager extends FeatureManager {

	private boolean useDescriptor;
	
	private enum FeaType {hm1, hm2, hm12, wm1, wm2,  wbnull, wbfl, wbf, wbl, wbo, lexbigram, bm1f, bm1l, am2f, am2l,et12,h1head,h2head,
		cm1m1, cp1p1, cm2m1, cm1p1, cp1p2, nummb, numwb,m1cm2, m2cm1, et12m1cm2, et12m2cm1, hm12m1cm2, hm12m2cm1,
		word,
		tag,
		bigram_word,
		bigram_tag,
		fo_word,
		fo_tag,
		fo_bigram_word,
		fo_bigram_tag,
		transition,
		contextual,
		path,
		phrase_boundary,
		nil}
	
	public ACEFeatureManager(GlobalNetworkParam param_g, boolean useDescriptor) {
		super(param_g);
		this.useDescriptor = useDescriptor;
	}

	private static final long serialVersionUID = -3346604057232887235L;

	@Override
	protected FeatureArray extract_helper(Network network, int parent_k, int[] children_k, int children_k_index) {
		int[] paArr = network.getNodeArray(parent_k);
		int nodeType = paArr[0];
		if (nodeType != NodeType.NODE.ordinal())
			return FeatureArray.EMPTY;
		RelInstance inst = (RelInstance)network.getInstance();
		CandidatePair input = inst.getInput();
		List<Span> spans = input.spans;
		Sentence sent = input.sent;
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		int relId = paArr[3];
		String rel = relId + "";
		String relForm = RelationType.get(relId).form;
		int arg1Idx = leftIdx;
		int arg2Idx = rightIdx;
		if (relForm.endsWith(Config.REV_SUFF)) {
			arg1Idx = rightIdx;
			arg2Idx = leftIdx;
		}
		Span arg1Span = spans.get(arg1Idx);
		Span arg2Span = spans.get(arg2Idx);
		String arg1Ent = arg1Span.entity;
		String arg2Ent = arg2Span.entity;
		
		int hm1Idx = this.getHeadIdx(sent, arg1Span);
		int hm2Idx = this.getHeadIdx(sent, arg2Span); 
		String hm1 = sent.get(hm1Idx).getForm();
		String hm2 = sent.get(hm2Idx).getForm();
		
		List<Integer> headWordList = new ArrayList<>();
		List<Integer> depWordList = new ArrayList<>();
		List<Integer> bgWords = new ArrayList<>();
		List<Integer> boundaryWords = new ArrayList<>();
		List<Integer> bigramWords = new ArrayList<>();
		List<Integer> wordffixs = new ArrayList<>();
		List<Integer> types = new ArrayList<>();
		
		headWordList.add(this._param_g.toFeature(network, FeaType.hm1.name(), rel, hm1));
		headWordList.add(this._param_g.toFeature(network, FeaType.hm2.name(), rel, hm2));
		headWordList.add(this._param_g.toFeature(network, FeaType.hm12.name(), rel, hm1 + " " + hm2));
		for (int i = arg1Span.start; i <= arg1Span.end; i++) {
			bgWords.add(this._param_g.toFeature(network, FeaType.wm1.name(), rel, sent.get(i).getForm()));
		}
		for (int i = arg2Span.start; i <= arg2Span.end; i++) {
			bgWords.add(this._param_g.toFeature(network, FeaType.wm2.name(), rel, sent.get(i).getForm()));
		}
		int comp = arg1Span.comparePosition(arg2Span);
		Span leftSpan = comp < 0 ? arg1Span : arg2Span;
		Span rightSpan = comp < 0 ? arg2Span : arg1Span;
		if (rightSpan.start - leftSpan.end <= 1) {
			//means no word in-between
			boundaryWords.add(this._param_g.toFeature(network, FeaType.wbnull.name(), rel, ""));
		} else if (rightSpan.start - leftSpan.end == 2) {
			//one word in-between
			String wib = sent.get(leftSpan.end + 1).getForm();
			boundaryWords.add(this._param_g.toFeature(network, FeaType.wbfl.name(), rel, wib));
		} else if (rightSpan.start - leftSpan.end >= 3) {
			//two words in-between
			String first = sent.get(leftSpan.end + 1).getForm();
			String last = sent.get(rightSpan.start - 1).getForm();
			boundaryWords.add(this._param_g.toFeature(network, FeaType.wbf.name(), rel, first));
			boundaryWords.add(this._param_g.toFeature(network, FeaType.wbl.name(), rel, last));
			//at least three words in between
			for (int i = leftSpan.end + 2; i < rightSpan.start - 1; i++) {
				String mid = sent.get(i).getForm();
				boundaryWords.add(this._param_g.toFeature(network, FeaType.wbo.name(), rel, mid));
			}
			//bigram
			for (int i = leftSpan.end + 1; i < rightSpan.start - 1; i++) {
				String l = sent.get(i).getForm();
				String r = sent.get(i+1).getForm();
				bigramWords.add(this._param_g.toFeature(network, FeaType.lexbigram.name(), rel, l + " " + r));
			}
		} 
		
		int bm1fIdx = arg1Idx - 1;
		int bm1lIdx = arg1Idx - 2;
		int am2fIdx = arg2Idx + 1;
		int am2lIdx = arg2Idx + 2;
		String bm1f = bm1fIdx >= 0 ? sent.get(bm1fIdx).getForm() : "STR1";
		String bm1l = bm1lIdx >= 0 ? sent.get(bm1lIdx).getForm() : bm1fIdx >= 0 ?  "STR1": "STR2";
		String am2f = am2fIdx < sent.length() ? sent.get(am2fIdx).getForm() : "END1";
		String am2l = am2lIdx < sent.length() ? sent.get(am2lIdx).getForm() : am2fIdx < sent.length() ? "END1" : "END2" ;
		
		wordffixs.add(this._param_g.toFeature(network, FeaType.bm1f.name(), rel, bm1f));
		wordffixs.add(this._param_g.toFeature(network, FeaType.bm1l.name(), rel, bm1l));
		wordffixs.add(this._param_g.toFeature(network, FeaType.am2f.name(), rel, am2f));
		wordffixs.add(this._param_g.toFeature(network, FeaType.am2l.name(), rel, am2l));
		
		types.add(this._param_g.toFeature(network, FeaType.et12.name(), rel, arg1Ent + " " + arg2Ent));
		
		
		List<Integer> numMentions = new ArrayList<>();
		List<Integer> numWords = new ArrayList<>();
		//number of mention in between
		int nummb = 0;
		for (int i = leftIdx + 1; i < rightIdx; i++) {
			Span span = spans.get(i);
			if (span.start > leftSpan.end && span.end < rightSpan.start) {
				nummb++;
			}
		}
		numMentions.add(this._param_g.toFeature(network, FeaType.nummb.name(), rel, nummb + ""));
		
		//number of words in between
		int numwb = rightSpan.start - leftSpan.end - 1;
		numwb = numwb > 0 ? numwb : 0;
		numWords.add(this._param_g.toFeature(network, FeaType.numwb.name(), rel, numwb + ""));
		
		
		List<Integer> bwms = new ArrayList<>();
		//whether m1 includes m2
		boolean m1cm2 = arg1Span.start <= arg2Span.start && arg1Span.end >= arg2Span.end ? true : false;
		//whether m2 includes m1
		boolean m2cm1 = arg2Span.start <= arg1Span.start && arg2Span.end >= arg1Span.end ? true : false;	
		bwms.add(this._param_g.toFeature(network, FeaType.m1cm2.name(), rel, m1cm2 + ""));
		bwms.add(this._param_g.toFeature(network, FeaType.m2cm1.name(), rel, m2cm1 + ""));
		bwms.add(this._param_g.toFeature(network, FeaType.et12m1cm2.name(), rel, arg1Ent + " " + arg2Ent + " " + m1cm2));
		bwms.add(this._param_g.toFeature(network, FeaType.et12m2cm1.name(), rel, arg1Ent + " " + arg2Ent + " " + m2cm1));
		bwms.add(this._param_g.toFeature(network, FeaType.hm12m1cm2.name(), rel, hm1 + " " + hm2 + " " + m1cm2 ));
		bwms.add(this._param_g.toFeature(network, FeaType.hm12m2cm1.name(), rel, hm1 + " " + hm2 + " " + m2cm1));
		
		
		int hm1HeadIdx = sent.get(hm1Idx).getHeadIndex();
		String hm1Head = hm1HeadIdx == -1 ? "<ROOT>" : sent.get(hm1HeadIdx).getForm();
		depWordList.add(this._param_g.toFeature(network, FeaType.h1head.name(), rel, hm1 + " " + hm1Head));
		int hm2HeadIdx = sent.get(hm2Idx).getHeadIndex();
		String hm2Head = hm2HeadIdx == -1 ? "<ROOT>" : sent.get(hm2HeadIdx).getForm();
		depWordList.add(this._param_g.toFeature(network, FeaType.h2head.name(), rel, hm2 + " " + hm2Head));
		
		FeatureArray fa1 = this.createFeatureArray(network, headWordList);
		FeatureArray fa2 = this.createFeatureArray(network, bgWords);
		FeatureArray fa3 = this.createFeatureArray(network, boundaryWords);
		FeatureArray fa4 = this.createFeatureArray(network, bigramWords);
		FeatureArray fa5 = this.createFeatureArray(network, wordffixs);
		FeatureArray fa6 = this.createFeatureArray(network, types);
		FeatureArray fa7 = this.createFeatureArray(network, depWordList);
		FeatureArray fa8 = this.createFeatureArray(network, bwms);
		FeatureArray fa9 = this.createFeatureArray(network, numMentions);
		FeatureArray fa10 = this.createFeatureArray(network, numWords);
		FeatureArray last = fa1.addNext(fa2).addNext(fa3).addNext(fa4).addNext(fa5).addNext(fa6).addNext(fa7)
		.addNext(fa8).addNext(fa9).addNext(fa10);
		
		
		/***Create the feature array for the relation descriptor span***/
		if (this.useDescriptor) {
			List<Integer> dwords = new ArrayList<>();
			List<Integer> dtags = new ArrayList<>();
			List<Integer> dphrase = new ArrayList<>();
			List<Integer> dbigramwords = new ArrayList<>();
			List<Integer> dbigramtags = new ArrayList<>();
			List<Integer> dbigramphrases = new ArrayList<>();
			List<Integer> trans = new ArrayList<>();
			int[] child = network.getNodeArray(children_k[0]);
			String relNoRev = relForm.replaceAll(Config.REV_SUFF, "");
			int dl = child[1];
			int dr = child[2];
			for (int i = 0; i < sent.length(); i++) {
				//use zero order first;
				String currLabel = i == dl ? "B-" + relNoRev : (i > dl && i <= dr) ? 
					"I-" + relNoRev : "O" + relNoRev;
				String output = null;
				int prevIdx = i - 1;
				String prevLabel = prevIdx == dl ? "B-" + relNoRev : (prevIdx > dl && prevIdx <= dr) ?
						"I-" + relNoRev : "O" + relNoRev;
				output = currLabel;
				for (int j = i - 2; j <= i + 2; j++) {
					int add = j - sent.length();
					int relPos = j - i;
					String word = j < 0 ? "START" + j : j >= sent.length() ? "END" + add :  sent.get(j).getForm();
					String tag  = j < 0 ? "START_TAG" + j : j >= sent.length() ? "END_TAG" + add :  sent.get(j).getTag();
					
					dwords.add(this._param_g.toFeature(network, FeaType.word.name() + relPos, output, word));
					dtags.add(this._param_g.toFeature(network, FeaType.tag.name() + relPos, output, tag));
					
					dwords.add(this._param_g.toFeature(network, FeaType.fo_word.name() + relPos, output, word + " " + prevLabel));
					dtags.add(this._param_g.toFeature(network, FeaType.fo_tag.name() + relPos, output, tag + " " + prevLabel));
					
				}
				for (int j = i - 1; j <= i + 2; j++) {
					int k = j - 1;
					int relPos = j - i;
					int addJ = j - sent.length();
					int addK = k - sent.length();
					String prevWord = k < 0 ? "START" + k : k >= sent.length() ? "END" + addK :  sent.get(k).getForm();
					String prevTag = k < 0 ? "START_TAG" + k : k >= sent.length() ? "END_TAG" + addK :  sent.get(k).getTag();
					String word = j < 0 ? "START" + j : j >= sent.length() ? "END" + addJ:  sent.get(j).getForm();
					String tag  = j < 0 ? "START_TAG" + j : j >= sent.length() ? "END_TAG" + addJ:  sent.get(j).getTag();
					
					dbigramwords.add(this._param_g.toFeature(network, FeaType.bigram_word.name() + relPos, output, prevWord + " " + word));
					dbigramtags.add(this._param_g.toFeature(network, FeaType.bigram_tag.name() + relPos, output, prevTag + " " + tag));
					
					dbigramwords.add(this._param_g.toFeature(network, FeaType.fo_bigram_word.name() + relPos, output, prevWord + " " + word + " " + prevLabel));
					dbigramtags.add(this._param_g.toFeature(network, FeaType.fo_bigram_tag.name() + relPos, output, prevTag + " " + tag + " " + prevLabel));
				}
				trans.add(this._param_g.toFeature(network, FeaType.transition.name(), currLabel, prevLabel));
				FeatureArray dfa1 = this.createFeatureArray(network, dwords);
				FeatureArray dfa2 = this.createFeatureArray(network, dtags);
				FeatureArray dfa3 = this.createFeatureArray(network, dphrase);
				FeatureArray dfa4 = this.createFeatureArray(network, dbigramwords);
				FeatureArray dfa5 = this.createFeatureArray(network, dbigramtags);
				FeatureArray dfa6 = this.createFeatureArray(network, dbigramphrases);
				FeatureArray dfa7 = this.createFeatureArray(network, trans);
				last.addNext(dfa1).addNext(dfa2).addNext(dfa3).addNext(dfa4).addNext(dfa5).addNext(dfa6).addNext(dfa7);
			}
		}
		return fa1;
	}
	
	
	/**
	 * Find the head word of a phrase according to the paper Zhou et al., 2005. 
	 * @param sent
	 * @param span
	 * @return
	 */
	private int getHeadIdx (Sentence sent, Span span) {
		//a mention is from the start to the head end according to Zhou 2005
		for (int i = span.start; i <= span.end; i++) {
			if (i > span.start && sent.get(i).getTag().equals("IN")) {
				return i -1;
			}
		}
		return span.end;
	}
	
	@SuppressWarnings("unused")
	private boolean isOnLeft(int idx, int relLeft, int relRight) {
		if (idx < relLeft) {
			return true; 
		} else if ( idx > relRight) {
			return false;
		} else {
			System.out.println(idx + "," + relLeft + ", " + relRight);
			throw new RuntimeException("index is in between the two arguments?");
		}
	}
	
	@SuppressWarnings("unused")
	private boolean violateBoundary (Sentence sent, int relLeft, int relRight) {
		if (!sent.get(relLeft).getPhraseTag().startsWith("B-")) return true;
		String rpt = sent.get(relRight).getPhraseTag();
		if (relRight == sent.length() - 1) return rpt.equals("O");
		String next_rpt = sent.get(relRight + 1).getPhraseTag();
		if (rpt.startsWith("B-") || rpt.startsWith("I-")) {
			return next_rpt.startsWith("I-");
		} else { //rpt = "O"
			return true;
		}
	}


}
