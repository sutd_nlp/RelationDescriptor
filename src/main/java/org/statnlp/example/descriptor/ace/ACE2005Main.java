package org.statnlp.example.descriptor.ace;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationDescriptorEvaluator;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class ACE2005Main {

	public static double l2val = 0.01;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static int trainNum = -1;
	public static int testNum = -1;
	public static String trainFile = "data/ace2005/train.data";
	public static String devFile = "data/ace2005/dev.data";
	public static String testFile = "data/ace2005/test.data";
	public static String resultFile = "results/ace2005_res.txt";
	public static boolean saveModel = true;
	public static boolean readModel = false;
	public static String modelFile = "models/ace2005_model.m";
	
	public static boolean contextualFeature = true;
	public static boolean pathFeature = true;
	public static boolean boundaryFeature = true;
	public static boolean useLatentSpan = false;
	public static boolean useRules = false;
	public static boolean useDescriptor = true;

	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		setArgs(args);
		
		/***
		 * Parameter settings and model configuration
		 */
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		ACEDataReader reader = new ACEDataReader();
		
		Set<String> rules = useRules ? new HashSet<String>() : null;
		/**debug info**/
//		saveModel = false;
//		readModel = false;
//		data = "wikipedia";
//		debugMain(reader);
//		System.exit(0);
//		useDescriptor = true;
//		useLatentSpan = true;
//		saveModel = true;
//		readModel = false;
//		trainFile = "data/ace2005/debug.data";
		/***/

		
		Instance[] trainInsts = reader.readInsts(trainFile, true, trainNum, rules);
		System.out.println("#Relations: " + RelationType.RELS.size());
		System.out.println("Relations: " + RelationType.RELS.toString());
		//RelationType.lock();
	
		
		NetworkModel model = null;
		if (readModel) {
			System.out.println("[Info] Reading Model....");
			ObjectInputStream in = RAWF.objectReader(modelFile);
			model = (NetworkModel)in.readObject();
		} else {
			GlobalNetworkParam gnp = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
			ACEFeatureManager tfm = new ACEFeatureManager(gnp, useDescriptor);
			ACENetworkCompiler tnc = new ACENetworkCompiler(useLatentSpan, useDescriptor, rules);
			model = DiscriminativeNetworkModel.create(tfm, tnc);
			model.train(trainInsts, numIteration);
		}
		if (saveModel) {
			ObjectOutputStream out = RAWF.objectWriter(modelFile);
			out.writeObject(model);
			out.close();
		}
//		System.exit(0);
		
		/**
		 * Testing Phase
		 */
//		testFile = trainFile;
		Instance[] testData = reader.readInsts(testFile, false, testNum, rules);
		Instance[] results = model.decode(testData);
		RelationDescriptorEvaluator evaluator = new RelationDescriptorEvaluator();
		evaluator.evaluateRelation(results);
		if (useDescriptor) 
			evaluator.evaluateOverlapAndExactMatchDescriptor(results);
		
		//print the results
		PrintWriter pw = RAWF.writer(resultFile);
		for (Instance res : results) {
			RelInstance inst = (RelInstance)res;
			Sentence sent = inst.getInput().sent;
			RelationDescriptor gold = inst.getOutput();
			RelationDescriptor pred = inst.getPrediction();
			pw.println(gold.toString());
			pw.println(pred.toString());
			for (int i = 0; i < inst.size(); i++) {
				String goldLabel = i >= gold.getLeft() && i <= gold.getRight() ? 
						i == gold.getLeft() ? "B-" + gold.getType().form : "I-"+ gold.getType().form: "O";
				String predLabel = i >= pred.getLeft() && i <= pred.getRight() ? 
						i == pred.getLeft() ? "B-"+ pred.getType().form : "I-"+ pred.getType().form : "O";
				pw.println(sent.get(i).getForm() + "\t" + goldLabel + "\t" + predLabel);
			}
			pw.println();
		}
		pw.close();
		
	}
	
	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Logistic Regression Model for Relation Extraction");
		parser.addArgument("-t", "--thread").setDefault(8).help("number of threads");
		parser.addArgument("--l2").setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--iter").setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--trainNum").setDefault(trainNum).help("The number of iteration.");
		parser.addArgument("--testNum").setDefault(testNum).help("The number of iteration.");
		parser.addArgument("--trainFile").setDefault(trainFile).help("The path of the data file");
		parser.addArgument("--devFile").setDefault(devFile).help("The path of the data file");
		parser.addArgument("--testFile").setDefault(testFile).help("The path of the data file");
		parser.addArgument("--saveModel").setDefault(saveModel).help("whether to save the model");
		parser.addArgument("--readModel").setDefault(readModel).help("whether to read the model");
		parser.addArgument("--modelFile").setDefault(modelFile).help("specify the model file");
		parser.addArgument("--resFile").setDefault(resultFile).help("specify the result file");
		parser.addArgument("--contextual").setDefault(contextualFeature).help("whether to use contextual features");
		parser.addArgument("--path").setDefault(pathFeature).help("whether to use path features");
		parser.addArgument("--boundary").setDefault(boundaryFeature).help("whether to use boundary features");
		parser.addArgument("--latentspan").setDefault(useLatentSpan).help("whether to use path features");
		parser.addArgument("--rules").setDefault(useRules).help("whether to use path features");
		parser.addArgument("--descriptor").setDefault(useDescriptor).help("whether to use descriptor");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = Integer.valueOf(ns.getString("thread"));
        l2val = Double.valueOf(ns.getString("l2"));
        numIteration = Integer.valueOf(ns.getString("iter"));
        trainFile = ns.getString("trainFile");
        devFile = ns.getString("devFile");
        testFile = ns.getString("testFile");
        saveModel = Boolean.valueOf(ns.getString("saveModel"));
        readModel = Boolean.valueOf(ns.getString("readModel")) ;
        modelFile = ns.getString("modelFile");
        resultFile = ns.getString("resFile");
        contextualFeature = Boolean.valueOf(ns.getString("contextual")) ;
        pathFeature = Boolean.valueOf(ns.getString("path")) ;
        boundaryFeature = Boolean.valueOf(ns.getString("boundary")) ;
        useLatentSpan = Boolean.valueOf(ns.getString("latentspan")) ;
        useRules = Boolean.valueOf(ns.getString("rules"));
        trainNum = Integer.valueOf(ns.getString("trainNum"));
        testNum = Integer.valueOf(ns.getString("testNum"));
        useDescriptor = Boolean.valueOf(ns.getString("descriptor")) ;
        System.err.println(ns.getAttrs().toString());
	}
}
