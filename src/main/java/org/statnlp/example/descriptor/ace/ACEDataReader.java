package org.statnlp.example.descriptor.ace;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.Config;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;

public class ACEDataReader {
	
	private boolean useHeadSpan = true;
	private boolean readRulesFromData = false;
	
	/**
	 * This reader read the head spans.
	 * @param file
	 * @param number
	 * @return
	 * @throws IOException
	 */
	public RelInstance[] readInsts(String file, boolean isTrainingInsts, int number, Set<String> rules) throws IOException {
		ArrayList<RelInstance> insts = new ArrayList<RelInstance>();
		String line = null;
		BufferedReader br = RAWF.reader(file);
		int instId = 1;
		int numSent = 0;
		int numRel = 0;
		int numEntityPair = 0;
		int numMentions = 0;
		int numSentsHasOverlapEntities = 0;
		while ((line = br.readLine())!= null ) {
			String[] words = line.split(" ");
			line = br.readLine(); // read the pos tag.
			String[] tags = line.split(" ");
			String headStr = br.readLine();
			String depStr = br.readLine();
			String[] heads = headStr.split(" ");
			String[] deps = depStr.split(" ");
			WordToken[] wts = new WordToken[words.length];
			for (int t = 0; t < wts.length; t++) {
				wts[t] = new WordToken(words[t], tags[t], Integer.valueOf(heads[t]), null, deps[t]);
			}
			Sentence sent = new Sentence(wts);
			String ners = br.readLine();
			List<Span> spans = new ArrayList<>();
			if (!ners.equals("")) {
				String[] spanInfos = ners.split("\\|");
				for (String spanInfo : spanInfos) {
					String[] spanArr = spanInfo.split(" ");
					String[] indexInfo = spanArr[0].split(",");
					int start = useHeadSpan ? Integer.valueOf(indexInfo[2]) : Integer.valueOf(indexInfo[0]);
					int end = useHeadSpan ? Integer.valueOf(indexInfo[3]) - 1 : Integer.valueOf(indexInfo[1]) - 1;
					Span span = new Span(start, end, spanArr[1]);
					if (!spans.contains(span)) {
						spans.add(span);
					}
				}
				Collections.sort(spans);
			}
			if (checkOverlap(spans)) {
				numSentsHasOverlapEntities++;
			}
			numSent++;
			numMentions += spans.size();
			String allRelations = br.readLine();
			int[][] filled = new int[spans.size()][spans.size()];
			if (spans.size() == 0) {
				numSent--;
			}
			if (allRelations.equals("")) {
				
			} else {
				String[] vals = allRelations.split("\\|");
				for (String oneRelation : vals) {
					numRel++;
					String[] relInfos = oneRelation.split("::");
					String relType = relInfos[0];
					String[] indexInfo = relInfos[1].split(" ");
					String[] descriptorSpan = indexInfo[0].split(",");
					int dStart = Integer.valueOf(descriptorSpan[0]);
					int dEnd = Integer.valueOf(descriptorSpan[1]) - 1;
					
					String[] arg1IndexInfo = indexInfo[1].split(",");
					String arg1Entity = indexInfo[2];
					String[] arg2IndexInfo = indexInfo[3].split(",");
					String arg2Entity = indexInfo[4];
					int start1 = useHeadSpan ? Integer.valueOf(arg1IndexInfo[2]) : Integer.valueOf(arg1IndexInfo[0]);
					int end1 = useHeadSpan ? Integer.valueOf(arg1IndexInfo[3]) - 1: Integer.valueOf(arg1IndexInfo[1]) - 1;
					int start2 = useHeadSpan ? Integer.valueOf(arg2IndexInfo[2]) : Integer.valueOf(arg2IndexInfo[0]);
					int end2 = useHeadSpan ? Integer.valueOf(arg2IndexInfo[3]) - 1: Integer.valueOf(arg2IndexInfo[1]) - 1;
					Span span1 = new Span(start1, end1, arg1Entity);
					Span span2 = new Span(start2, end2, arg2Entity);
					int span1Idx = spans.indexOf(span1);
					int span2Idx = spans.indexOf(span2);
					int leftIdx = span1Idx < span2Idx ? span1Idx : span2Idx;
					int rightIdx = span1Idx < span2Idx ? span2Idx : span1Idx;
					filled[leftIdx][rightIdx] = 1;
					String direction = span1Idx < span2Idx ? "" : Config.REV_SUFF;
					relType = relType + direction;
					RelationDescriptor rd = new RelationDescriptor(RelationType.get(relType), dStart, dEnd);
					CandidatePair input = new CandidatePair(sent, spans, leftIdx, rightIdx);
					//define the direction.
					/**means the reversed direction for the relation**/
					if (rules != null  && readRulesFromData && isTrainingInsts) {
						rules.add(relType+","+spans.get(leftIdx).entity+","+spans.get(rightIdx).entity);
					}
					RelInstance inst = new RelInstance(instId, 1.0, input, rd);
					if (isTrainingInsts)
						inst.setLabeled();
					else inst.setUnlabeled();
					insts.add(inst);
					instId++;
					if (span1Idx < 0 || span2Idx < 0)
						throw new RuntimeException("smaller than 0?");
				}
			}
			for (int i = 0; i < spans.size(); i++) {
				for (int j = i + 1; j < spans.size(); j++) {
					numEntityPair++;
					if (filled[i][j] == 0) {
						CandidatePair input = new CandidatePair(sent, spans, i, j);
						RelationType nr  = RelationType.get(Config.NR);
						if (rules != null  && readRulesFromData && isTrainingInsts) {
							rules.add(nr.form+","+spans.get(i).entity+","+spans.get(j).entity);
						}
						RelInstance inst = new RelInstance(instId, 1.0, input, new RelationDescriptor(nr));
						if (isTrainingInsts) {
							inst.setLabeled();
						} else {
							inst.setUnlabeled();
						}
						insts.add(inst);
						instId++;
					}
				}
			}
			if (number != -1 && insts.size() > number) {
				break;
			}
			spans = new ArrayList<>();
			line = br.readLine(); //empty line
		}
		br.close();
		RelationType.get(Config.NR);
		System.out.println("number of sents with entities: " + numSent);
		System.out.println("number of relations:" + numRel);
		System.out.println("number of mention pairs:" + numEntityPair);
		System.out.println("number of mentions:" + numMentions);
		System.out.println("number of sentences have overlapping mentions:" + numSentsHasOverlapEntities+ " [not ignore]" );
		return insts.toArray(new RelInstance[insts.size()]);
	}
	
	private boolean checkOverlap(List<Span> spans) {
		for (int i = 0; i < spans.size(); i++) {
			for (int j = i + 1; j < spans.size(); j++) {
				if (spans.get(i).overlap(spans.get(j)))
					return true;
			}
		}
		return false;
	}
	
}
