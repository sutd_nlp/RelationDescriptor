package org.statnlp.example.descriptor.eval;

public class Candidate {

	public String[] sent;
	public int[] answerDes;
	
	public int[] betDes;
	
	public Candidate(String[] sent, int[] answerDes) {
		this.sent = sent;
		this.answerDes = answerDes;
	}
	
}
