package org.statnlp.example.descriptor.eval;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import org.statnlp.commons.io.RAWF;
import org.statnlp.example.descriptor.eval.EvalMain.SetSize;

public class EvalReader {

	public static final long randomSeed = 1234;
	/**
	 * Split the original annotation file into validation set and test set.
	 * No need for the union, because union is all.
	 * @param all
	 * @param aDevFile
	 * @param aDevIds
	 * @param aTestFile
	 * @param aTestIds
	 * @param bDevFile
	 * @param bDevIds
	 * @param bTestFile
	 * @param bTestIds
	 */
	public static void splitAnnotation(Annotation[] all, String aDevFile, String aDevIds, String aTestFile, String aTestIds, 
			String bDevFile, String bDevIds, String bTestFile, String bTestIds,
			String cDevFile, String cDevIds, String cTestFile, String cTestIds) {
		List<Annotation> list = Arrays.asList(all);
		Collections.shuffle(list, new Random(randomSeed));
		List<Annotation> aDev = new ArrayList<>();
		List<Annotation> aTest = new ArrayList<>();
		List<Annotation> bDev = new ArrayList<>();
		List<Annotation> bTest = new ArrayList<>();
		List<Annotation> cDev = new ArrayList<>();
		List<Annotation> cTest = new ArrayList<>();
		placeIn(SetSize.allAgree, list, aDev, aTest, 287);
		placeIn(SetSize.twoAgree, list, bDev, bTest, 337);
		placeIn(SetSize.allUnion, list, cDev, cTest, 217);
		Collections.sort(aDev, Comparator.comparing(Annotation::getId));
		Collections.sort(aTest, Comparator.comparing(Annotation::getId));
		Collections.sort(bDev, Comparator.comparing(Annotation::getId));
		Collections.sort(bTest, Comparator.comparing(Annotation::getId));
		Collections.sort(cDev, Comparator.comparing(Annotation::getId));
		Collections.sort(cTest, Comparator.comparing(Annotation::getId));
		writeAnnotationToFile(aDev, aDevFile, aDevIds);
		writeAnnotationToFile(aTest, aTestFile, aTestIds);
		writeAnnotationToFile(bDev, bDevFile, bDevIds);
		writeAnnotationToFile(bTest, bTestFile, bTestIds);
		writeAnnotationToFile(cDev, cDevFile, cDevIds);
		writeAnnotationToFile(cTest, cTestFile, cTestIds);
	}
	
	private static void placeIn(SetSize ss, List<Annotation> all, List<Annotation> dev, List<Annotation> test, int devNum) {
		for (Annotation ann : all) {
			if (ss == SetSize.allAgree && !ann.allAgree) continue;
			if (ss == SetSize.twoAgree && !ann.twoSame) continue;
			if (dev.size() == devNum) {
				test.add(ann);
			} else {
				dev.add(ann);
			}
		}
		System.out.println("ss " + ss + " dev: " + dev.size()  + " test: " + test.size());
	}
	
	private static void writeAnnotationToFile(List<Annotation> list, String file, String idFile) {
		PrintWriter pw;
		PrintWriter pwId;
		try {
			pw = RAWF.writer(file);
			pwId = RAWF.writer(idFile);
			for(Annotation ann : list) {
				//pw.println("sentId: " + ann.id);
				pw.println(ann.sent2Str());
				pw.println(ann.ann2Str());
				pwId.println(ann.id);
			}
			pw.close();
			pwId.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Annotation[] readAnnotations(String file) {
		List<Annotation> list = new ArrayList<>();
		BufferedReader br;
		try {
			br = RAWF.reader(file);
			String line = null;
			while((line = br.readLine()) != null) {
				String[] sent = line.split(" ");
				String[][] as = new String[3][];
				line = br.readLine();
				as[0] = line.split(" ");
				line = br.readLine();
				as[1]= line.split(" ");
				line = br.readLine();
				as[2] = line.split(" "); 
				br.readLine(); //empty
				int[][] anns = new int[3][sent.length];
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < sent.length; j++)
						anns[i][j] = Integer.valueOf(as[i][j]);
				} 
				Annotation annotation = new Annotation(list.size(), sent, anns);
				list.add(annotation);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list.toArray(new Annotation[list.size()]);
	}
	
	public static Candidate[] readRDCandidate(String file) {
		List<Candidate> list = new ArrayList<>();
		BufferedReader br;
		try {
			br = RAWF.reader(file);
			String line = null;
			while((line = br.readLine()) != null) {
				String[] sent = line.split(" ");
				String[] vals = br.readLine().split(" ");
				String idxStr = vals[3];
				String[] desIdxs = idxStr.split(",");
				int left = Integer.valueOf(desIdxs[0]);
				int right = Integer.valueOf(desIdxs[1]);
				int[] ans = new int[sent.length];
				for (int i = 0; i < sent.length; i++) {
					if (i >= left && i <= right) ans[i] = 1;
					else ans[i] = 0;
				}
				Candidate can = new Candidate(sent, ans);
				int[] betANS = new int[sent.length];
				int e1End = Integer.valueOf(vals[0].split(",")[1]);
				int e2Start = Integer.valueOf(vals[1].split(",")[0]);
				for (int i = 0; i < sent.length; i++) {
					if (i > e1End && i < e2Start) betANS[i] = 1;
					else betANS[i] = 0;
				}
				can.betDes = betANS;
				list.add(can);
				br.readLine();
				br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list.toArray(new Candidate[list.size()]);
	}
}
