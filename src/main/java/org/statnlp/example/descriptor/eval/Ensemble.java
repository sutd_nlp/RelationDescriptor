package org.statnlp.example.descriptor.eval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationDescriptorEvaluator;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;

public class Ensemble {

	public static String middleResFile = "results/NN_GRU_500_0.5_batch_25_fe_true_tg_true_desEmb_load_false.res833.txt";
	public static String extentResFile = "results/NN_GRU_500_0.5_batch_25_fe_true_tg_true_extend.res.txt";
	public static String fullResFile = "results/FULL_NN_GRU_500_0.5_batch_25_fe_true.res.txt";
	
	private static void readPreLabels() {
		BufferedReader br;
		try {
			br = RAWF.reader("data/semeval/prevlabels.txt");
			String line = null; 
			while ((line = br.readLine()) != null) {
				RelationType.get(line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		readPreLabels();
		
		RelationDescriptorEvaluator evaluator = new RelationDescriptorEvaluator(true);
		Instance[] middInsts = readResultFile(middleResFile);
		Instance[] extendInsts = readResultFile(extentResFile);
		Instance[] fullInsts = readResultFile(fullResFile);
		Instance[] results = new Instance[middInsts.length];
		for (int i = 0; i < middInsts.length; i++) {
			RelationDescriptor output = (RelationDescriptor)middInsts[i].getOutput();
			CandidatePair input = (CandidatePair)middInsts[i].getInput();
			results[i] = new RelInstance(i + 1, 1.0, input, output);
			RelationDescriptor p1 = (RelationDescriptor)middInsts[i].getPrediction();
			RelationDescriptor p2 = (RelationDescriptor)extendInsts[i].getPrediction();
			RelationDescriptor p3 = (RelationDescriptor)fullInsts[i].getPrediction();
			String p1f = p1.getType().form;
			String p2f = p2.getType().form;
			String p3f = p3.getType().form;
			System.out.println(p1f);
			if (p1f.equals(p2f) && p2f.equals(p3f)) {
				results[i].setPrediction(p1);
			} else {
				if (p1f.equals(p2f) || p1f.equals(p3f)) {
					results[i].setPrediction(p1);
				} else if (p2f.equals(p3f)) {
					results[i].setPrediction(p2);
				} else {
					results[i].setPrediction(p1);
				}
			}
		}
		evaluator.evaluateRelation(results);
	}
	
	public static Instance[] readResultFile(String file) {
		List<RelInstance> insts = new ArrayList<RelInstance>();
		BufferedReader br;
		try {
			br = RAWF.reader(file);
			String line = null;
			int instId = 1;
			while ((line = br.readLine()) != null) {
				String[] vals = line.split(" ");
				br.readLine(); //index seq.
				String[] relations = br.readLine().split(",");
				String goldRelation = relations[0];
				String predRelation = relations[1];
				WordToken[] wts = new WordToken[vals.length];
				for (int p = 0; p < wts.length; p++) {
					wts[p] = new WordToken(vals[p]);
				}
				Sentence sent = new Sentence(wts);
				CandidatePair cp = new CandidatePair(sent, new ArrayList<Span>(), 0, 1);
				RelationDescriptor descriptor = new RelationDescriptor(RelationType.get(goldRelation));
				RelInstance inst = new RelInstance(instId, 1.0, cp, descriptor);
				inst.setPrediction( new RelationDescriptor(RelationType.get(predRelation)));
				insts.add(inst);
				instId++;
				br.readLine(); //empty line;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return insts.toArray(new RelInstance[insts.size()]);
	}
}
