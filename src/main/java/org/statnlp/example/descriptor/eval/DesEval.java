package org.statnlp.example.descriptor.eval;

import java.util.Arrays;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.Span;
import org.statnlp.example.descriptor.eval.EvalMain.SetSize;

public class DesEval {

	
	/**
	 * Performing on all agree
	 * @param annotations
	 * @param candidates
	 * @return
	 */
	public static double[][] evaluateTokenAndExactMatchDescriptor(Annotation[] annotations, Candidate[] candidates, SetSize ss,
			List<String> rels, String specificRel) {
		double[][] metrics = new double[3][4];
		int overlapCount = 0;
		int totalCount = 0;
		int overlapCorr = 0;
		int totalPredict = 0;
		int totalExpected = 0;
		int exactCorr = 0;
		int exactCount = 0;
		
		int totalDesTokenPredict = 0;
		int totalDesTokenExpected = 0;
		int desTokenTp = 0;
		int wrongNum = 0;
		int shorter = 0;
		
		for (int i = 0 ; i < annotations.length; i++) {
			Annotation ann = annotations[i];
			Candidate can = candidates[i];
			
			int goldIdx = -1;
			int[] annANS = null;
			if (ss == SetSize.allAgree) {
				if(!ann.allAgree)  continue;
				goldIdx = 0;
				annANS = ann.anns[goldIdx];
			} else if (ss == SetSize.twoAgree) {
				if (!ann.twoSame) continue;
				goldIdx = ann.twoSameIdx;
				annANS = ann.anns[goldIdx];
			} else if (ss == SetSize.allUnion) {
				annANS = ann.allUnion;
			}
			if (specificRel != null && !rels.get(i).startsWith(specificRel)) {
				//System.out.println(rels.get(i));
				continue;
			}
			
			
			int[] canANS = can.answerDes;
			
			if (containsDes(canANS) && containsDes(annANS)) {
				for (int m = 0; m < canANS.length; m++) {
					if (canANS[m] == annANS[m] && annANS[m] == 1) desTokenTp++;
				}
				if (overlap(canANS, annANS)) {
					overlapCorr++;
					overlapCount++;
				}
				if (Arrays.equals(canANS, annANS)) {
					exactCorr++;
					exactCount++;
				} else {
					wrongNum++;
					if (sum(canANS) < sum(annANS)) {
//						System.out.println(Arrays.toString(can.sent));
//						System.out.println(Arrays.toString(canANS));
//						System.out.println(Arrays.toString(annANS));
						shorter++;
					}
				}
			} else if (!containsDes(canANS) && !containsDes(annANS)){
				overlapCount++;
				exactCount++;
			}
			
			if (containsDes(annANS)) {
				totalExpected++;
				for (int m = 0; m < canANS.length; m++) {
					if (annANS[m] == 1)
						totalDesTokenExpected++;
				}
			}
			if (containsDes(canANS)) {
				totalPredict++;
				for (int m = 0; m < canANS.length; m++) {
					if (canANS[m] == 1)
						totalDesTokenPredict++;
				}
			}
			
			totalCount++;
		}
		metrics[0][0] = overlapCount * 1.0 / totalCount *100;
		metrics[0][1] = overlapCorr * 1.0 / totalPredict * 100;
		metrics[0][2] = overlapCorr * 1.0 / totalExpected * 100;
		metrics[0][3] = 2.0 * overlapCorr / (totalPredict + totalExpected) * 100;
		metrics[1][0] = exactCount * 1.0 / totalCount * 100;
		metrics[1][1] = exactCorr * 1.0 / totalPredict * 100;
		metrics[1][2] = exactCorr * 1.0 / totalExpected * 100;
		metrics[1][3] = 2.0 * exactCorr / (totalPredict + totalExpected) * 100;
		metrics[2][0] = 0 * 100;
		metrics[2][1] = desTokenTp * 1.0 / totalDesTokenPredict * 100;
		metrics[2][2] = desTokenTp * 1.0 / totalDesTokenExpected * 100;
		metrics[2][3] = 2.0 * desTokenTp / (totalDesTokenExpected + totalDesTokenPredict) * 100;
//		System.out.printf("[Result Token Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", metrics[2][0], metrics[2][1], metrics[2][2], metrics[2][3]);
//		System.out.printf("[Result Overlap Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", metrics[0][0], metrics[0][1], metrics[0][2], metrics[0][3]);
//		System.out.printf("[Result Exact Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", metrics[1][0], metrics[1][1], metrics[1][2], metrics[1][3]);
		System.out.printf("%.1f & %.1f & %.1f\n", metrics[2][1], metrics[2][2], metrics[2][3]);
		System.out.printf("%.1f & %.1f & %.1f\n", metrics[1][1], metrics[1][2], metrics[1][3]);
		System.out.println("tok tp:" + desTokenTp + " exact tp: " + exactCorr);
		System.out.println("tok pred:" + totalDesTokenPredict + " tok expect: " + totalDesTokenExpected);
		System.out.println("wrong num: " + wrongNum + ", shorter num: " + shorter + ",total pred: " + totalPredict +
				", total expected: " + totalExpected);
		return metrics;
	}
	
	private static boolean containsDes(int[] a) {
		for (int x : a) {
			if (x == 1) return true;
		}
		return false;
	}
	
	private static int sum(int[] a) {
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}
	
	private static boolean overlap(int[] a, int[] b){
		for (int i = 0; i < a.length; i++) {
			if (a[i] == 1) {
				if (a[i] == b[i])
					return true;
			}
		}
		return false;
	}
	
	/**
	 * This method is used to evaluate the predicted refined SDP
	 * on the refined SDP with test set.
	 * @param res
	 * @param anns
	 * @param ss
	 */
	public static void evalutateDep (Instance[] res, Annotation[] anns, SetSize ss) {
		double[][] metrics = new double[3][4];
		int overlapCount = 0;
		int totalCount = 0;
		int overlapCorr = 0;
		int totalPredict = 0;
		int totalExpected = 0;
		int exactCorr = 0;
		int exactCount = 0;
		int totalDesTokenPredict = 0;
		int totalDesTokenExpected = 0;
		int desTokenTp = 0;
		
		int shortWrongNum = 0;
		int longWrongNum = 0;
		int bothHasDesNum = 0;
		int separate = 0;
		for (int i = 0; i < res.length; i++) {
			Instance ins = res[i];
			Annotation ann = anns[i];
			if (ss == SetSize.allAgree) {
				if(!ann.allAgree)  continue;
			} else if (ss == SetSize.twoAgree) {
				if (!ann.twoSame) continue;
			} else if (ss == SetSize.allUnion) {
			}
			
			RelInstance inst = (RelInstance)ins;
			RelationDescriptor pred = inst.getPrediction();
			int predLeft = pred.getLeft();
			int predRight = pred.getRight();
			Sentence sent = inst.getInput().sent;
			List<Span> spans = inst.getInput().spans;
			Span arg1Span = spans.get(inst.getInput().leftSpanIdx);
			Span arg2Span = spans.get(inst.getInput().rightSpanIdx);
			int[] sdps = getShortestDepPath(sent, arg1Span, arg2Span);
			int[] startEnds = getStartEndFromSDPs(arg1Span.end, arg2Span.start, sdps);
			int goldLeft = startEnds[0];
			int goldRight = startEnds[1];
			
			if (predLeft >= 0 && predRight >=0 && goldLeft >=0 && goldRight >= 0) {
				bothHasDesNum++;
				for (int m = 0; m < sent.length(); m++) {
					if ( m>=goldLeft && m <= goldRight && m>=predLeft && m <= predRight) desTokenTp++;
				}
				if (predLeft > goldRight || predRight < goldLeft) {
					//do nothing
					separate++;
				} else {
					overlapCorr++;
					overlapCount++;
					if (predRight - predLeft + 1 < goldRight - goldLeft + 1) {
						shortWrongNum++;
					}
					if (predRight - predLeft + 1 > goldRight - goldLeft + 1) {
						longWrongNum++;
					}
				}
				if (predLeft == goldLeft && predRight == goldRight) {
					exactCorr++;
					exactCount++;
				} else {
//					System.out.println(sent.toString());
//					System.out.println("c: " + predLeft + ", " + predRight);
//					System.out.println("g: " + goldLeft + ", " + goldRight);
				}
			} else if (predLeft < 0 && predRight < 0 && goldLeft < 0 && goldRight < 0) {
				overlapCount++;
				exactCount++;
			} 
			if (goldLeft >= 0 && goldRight >= 0) {
				totalExpected++;
				totalDesTokenExpected += goldRight - goldLeft + 1;
			}
			if (predLeft >= 0 && predRight >= 0) {
				totalPredict++;
				totalDesTokenPredict += predRight - predLeft + 1;
			}
			totalCount++;
		}
		metrics[0][0] = overlapCount * 1.0 / totalCount *100;
		metrics[0][1] = overlapCorr * 1.0 / totalPredict * 100;
		metrics[0][2] = overlapCorr * 1.0 / totalExpected * 100;
		metrics[0][3] = 2.0 * overlapCorr / (totalPredict + totalExpected) * 100;
		metrics[1][0] = exactCount * 1.0 / totalCount * 100;
		metrics[1][1] = exactCorr * 1.0 / totalPredict * 100;
		metrics[1][2] = exactCorr * 1.0 / totalExpected * 100;
		metrics[1][3] = 2.0 * exactCorr / (totalPredict + totalExpected) * 100;
		metrics[2][1] = desTokenTp * 1.0 / totalDesTokenPredict * 100;
		metrics[2][2] = desTokenTp * 1.0 / totalDesTokenExpected * 100;
		metrics[2][3] = 2.0 * desTokenTp / (totalDesTokenExpected + totalDesTokenPredict) * 100;
		System.out.println("overlap count: " + overlapCount);
		System.out.println("[Refined SDP Prediction] wrong predicted to shoort: " + shortWrongNum);
		System.out.println("[Refined SDP Prediction] wrong predicted to long: " + longWrongNum);
		System.out.println("[Refined SDP Prediction] separate des: " + separate);
		System.out.println("[Refined SDP Prediction] both has des: " + bothHasDesNum);
		System.out.printf("[Result Token Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", metrics[2][0], metrics[2][1], metrics[2][2], metrics[2][3]);
//		System.out.printf("[Result Overlap Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", metrics[0][0], metrics[0][1], metrics[0][2], metrics[0][3]);
		System.out.printf("[Result Exact Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", metrics[1][0], metrics[1][1], metrics[1][2], metrics[1][3]);
	}
	
	private static int[] getStartEndFromSDPs(int leftBound, int rightBound, int[] sdps) {
		int[] startEnd = new int[2];
		startEnd[0] = -1;
		startEnd[1] = -1;
		for (int i = 0; i < sdps.length; i++) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[0] = i; break;}
		}
		for (int i = sdps.length - 1; i >= 0; i--) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[1] = i; break;}
		}
		return startEnd;
	}
	
	private static  int[] getShortestDepPath(Sentence sent, Span arg1Span, Span arg2Span) {
		Graph<Integer, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
		int[] des = new int[sent.length()];
		for (int i = 0; i < sent.length(); i++) {
			g.addVertex(i);
		}
		for (int i = 0; i < sent.length(); i++) {
			int head = sent.get(i).getHeadIndex();
			if (head != -1) {
				g.addEdge(i, head);
			}
		}
		DijkstraShortestPath<Integer, DefaultEdge> dg = new DijkstraShortestPath<>(g);
		GraphPath<Integer, DefaultEdge>  results = dg.getPath(arg1Span.headIdx, arg2Span.headIdx);
		List<Integer> list =  results.getVertexList();
		StringBuilder sb = new StringBuilder();
		for (int v : list) {
			if (v >= arg1Span.start && v <= arg1Span.end) continue;
			if (v >= arg2Span.start && v <= arg2Span.end) continue;
			if (v == arg1Span.headIdx || v == arg2Span.headIdx) continue;
			des[v] = 1;
			sb.append(sent.get(v).getForm() + " ");
		}
		return des;
	}
	
	public static int getLen(int[] a) {
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}
}
