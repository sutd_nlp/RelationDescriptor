package org.statnlp.example.descriptor.eval;

import java.util.Arrays;

public class Annotation {

	public int id; //basically sentence id;
	public String[] sent;
	public int[][] anns;
	public boolean allAgree = false;
	public boolean twoSame = false;
	public int twoSameIdx = -1;
	public int[] allUnion;
	
	public Annotation(int id, String[] sent, int[][] anns) {
		this.id = id;
		this.sent = sent;
		this.anns = anns;
		allAgree = checkAllAgree(anns);
		twoSame = checkTwoSame(anns);
		this.createUnion();
	}
	
	public int getId() {
		return this.id;
	}
	
	private boolean checkAllAgree(int[][] ann) {
		return Arrays.equals(ann[0], ann[1]) && Arrays.equals(ann[1], ann[2]);
	}
	
	private boolean checkTwoSame(int[][] ann) {
		if (Arrays.equals(ann[0], ann[1])) twoSameIdx = 0;
		else if (Arrays.equals(ann[1], ann[2])) twoSameIdx = 1;
		else if (Arrays.equals(ann[0], ann[2])) twoSameIdx = 0;
		return twoSameIdx >= 0;
	}
	
	private void createUnion(){
		allUnion = new int[anns[0].length];
		for (int i = 0; i < allUnion.length; i++) {
			allUnion[i] = anns[0][i] + anns[1][i] + anns[2][i];
			if (allUnion[i] > 1) {
				allUnion[i] = 1;
			}
		}
	}
	
	public String sent2Str() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sent.length; i++) {
			sb.append(i == 0 ? sent[i] : " " + sent[i]);
		}
		return sb.toString();
	}
	
	public String ann2Str() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < anns.length; i++) {
			for (int j = 0; j < anns[i].length; j++) {
				sb.append(j == 0 ? anns[i][j] + "" : " " + anns[i][j]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
