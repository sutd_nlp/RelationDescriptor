package org.statnlp.example.descriptor.eval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.statnlp.commons.io.RAWF;

import cern.colt.Arrays;

public class EvalMain {

	
	public static String annFile = "results/final_annotation.txt";
	public static String candidateFile = "results/lds_semeval_res.txt";
	//FULL_NN_GRU_500_0.5_batch_25_fe_true.res_83.9.txt
	//results/NN_GRU_500_0.5_batch_25_fe_true_tg_true_desEmb.res.txt
	//me_semeval_res.txt
	//NN_GRU_500_0.5_batch_25_fe_true_tg_true_desEmb_load_true.res
	
	public static String aDevFile = "results/allAgreeDev.txt";
	public static String aTestFile = "results/allAgreeTest.txt";
	public static String aDevIds = "results/allAgreeDevIds.txt";
	public static String aTestIds = "results/allAgreeTestIds.txt";
	public static String bDevFile = "results/twoAgreeDev.txt";
	public static String bTestFile = "results/twoAgreeTest.txt";
	public static String bDevIds = "results/twoAgreeDevIds.txt";
	public static String bTestIds = "results/twoAgreeTestIds.txt";
	public static String cDevFile = "results/allUnionDev.txt";
	public static String cTestFile = "results/allUnionTest.txt";
	public static String cDevIds = "results/allUnionDevIds.txt";
	public static String cTestIds = "results/allUnionTestIds.txt";
	
	public static String testFile = "data/semeval/sd_parseTag_test.txt";
	
	public static enum SetSize {
		allAgree,
		twoAgree,
		allUnion
	}
	
	public static SetSize ss;

	public static void splitFiles() {
		Annotation[] anns = EvalReader.readAnnotations(annFile);
		EvalReader.splitAnnotation(anns, aDevFile, aDevIds, aTestFile, aTestIds, bDevFile, bDevIds, bTestFile, bTestIds, cDevFile, cDevIds, cTestFile, cTestIds);
	}
	
	public static void main(String[] args) {
		for (SetSize ss : SetSize.values()) {
			Annotation[] anns = EvalReader.readAnnotations("results/" + ss.name() + "Test.txt");
			Candidate[] cans = EvalReader.readRDCandidate("results/" + ss.name() + "_res.txt");
			//check length
			for (int i = 0; i < cans.length; i++) {
				if (anns[i].sent.length != cans[i].sent.length) {
					System.err.println(i + ": " + Arrays.toString(anns[i].sent));
					System.err.println(i + ": " + Arrays.toString(cans[i].sent));
					throw new RuntimeException("sent length not equal");
				}
			}
			System.out.println(ss);
			DesEval.evaluateTokenAndExactMatchDescriptor(anns, cans, ss, null, null);
		}
		/** statistics ***/
		Annotation[] anns = EvalReader.readAnnotations(annFile);
		printAllAgreeDistribution(anns);
		String[] labels = readLabelFie(testFile);
		measureAgreement(anns, labels);
	}
	
	public static void printAllAgreeDistribution(Annotation[] anns) {
		Map<Integer, Integer> len2Num = new HashMap<>();
		for (Annotation ann : anns) {
			if (!ann.twoSame) continue;
			int[] ans = ann.anns[ann.twoSameIdx];
			int len = 0;
			for (int x : ans) len += x;
			if (len2Num.containsKey(len)) {
				int num = len2Num.get(len);
				len2Num.put(len, num + 1);
			} else {
				len2Num.put(len, 1);
			}
		}
		for(int len : len2Num.keySet()) {
			System.out.println(len + ", " + len2Num.get(len));
		}
	}
	
	private static String[] readLabelFie(String file) {
		List<String> list = new ArrayList<>();
		BufferedReader br;
		try {
			br = RAWF.reader(file);
			String line = null;
			while ((line = br.readLine()) != null) {
				//words
				br.readLine(); br.readLine(); br.readLine();
				br.readLine(); br.readLine(); br.readLine();
				line = br.readLine();
				list.add(line.replace("-rev", ""));
				br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list.toArray(new String[list.size()]);
	}
	
	public static void measureAgreement(Annotation[] anns, String[] rels) {
		Map<String, Double> allAgreeMap = new HashMap<String, Double>(); 
		Map<String, Double> twoSameMap = new HashMap<String, Double>(); 
		int allAgree = 0;
		int twoSame  = 0;
		int continuousAllAgree = 0;
		int hasDesAllAgree = 0;
		int continuousTwoSame = 0;
		int hasDesTwoSame = 0;
		for (int i = 0; i < anns.length; i++) {
			Annotation ann = anns[i];
			String rel = rels[i];
			if (ann.allAgree) {
				if (allAgreeMap.containsKey(rel)) {
					double num = allAgreeMap.get(rel);
					allAgreeMap.put(rel, num + 1);
				} else {
					allAgreeMap.put(rel, 1.0);
				}
				allAgree++;
				if (hasDescriptor(ann.anns[0]) )  {
					hasDesAllAgree++;
					if (isContinuous(ann.anns[0])) continuousAllAgree++;
				}
			}
			if (ann.twoSame) {
				if (twoSameMap.containsKey(rel)) {
					double num = twoSameMap.get(rel);
					twoSameMap.put(rel, num + 1);
				} else {
					twoSameMap.put(rel, 1.0);
				}
				twoSame++;
				if (hasDescriptor(ann.anns[ann.twoSameIdx]) )  {
					hasDesTwoSame++;
					if (isContinuous(ann.anns[ann.twoSameIdx])) continuousTwoSame++;
				}
			}
		}
		for (String rel : allAgreeMap.keySet()) {
			System.out.println(rel + ": " + allAgreeMap.get(rel) + ", " + allAgreeMap.get(rel)/anns.length);
		}
		
		System.out.println("all agree: " + allAgree + ", " + allAgree * 1.0 / anns.length );
		System.out.println();
		for (String rel : twoSameMap.keySet()) {
			System.out.println(rel + ": " + twoSameMap.get(rel) + ", " + twoSameMap.get(rel)/anns.length);
		}
		System.out.println("two same: " + twoSame + ", "  + twoSame * 1.0 / anns.length );
		
		System.out.println("continuous: " + continuousAllAgree + ", totalHasDesAllgree: " + hasDesAllAgree + " pers: " + continuousAllAgree * 1.0 / hasDesAllAgree);
		System.out.println("continuous: " + continuousTwoSame + ", totalHasDesTwoSame: " + hasDesTwoSame + " pers: " + continuousTwoSame * 1.0 / hasDesTwoSame);
	}
	
	private static boolean hasDescriptor(int[] ann) {
		for (int i = 0; i < ann.length; i++) {
			if (ann[i] == 1) return true;
		}
		return false;
	}
	
	private static boolean isContinuous(int[] ann) {
		int sum = 0;
		for (int i = 0; i < ann.length; i++) {
			sum += ann[i];
		}
		int left = -1;
		for (int i = 0; i < ann.length; i++) {
			if (ann[i] == 1) {left = i; break;}
		}
		int right = -1;
		for (int i = ann.length - 1; i >= 0; i--) {
			if (ann[i] == 1) {right = i; break;}
		}
		if (right - left + 1 == sum) {
			return true;
		} else return false;
	}
}
