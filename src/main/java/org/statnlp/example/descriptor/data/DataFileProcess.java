package org.statnlp.example.descriptor.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

import org.statnlp.commons.io.RAWF;

public class DataFileProcess {

	public void exportVocabFileFromData(String input, String output) {
		BufferedReader br;
		try {
			br = RAWF.reader(input);
			PrintWriter pw = RAWF.writer(output);
			String line = null;
			Set<String> vocab = new HashSet<>();
			while ((line = br.readLine()) != null) {
				String words = line;
				br.readLine(); //pos
				br.readLine();// heads
				br.readLine(); //depLabel
				br.readLine();//entity info
				br.readLine(); //relationlabel;
				br.readLine(); //emptyLine.
				String[] vals = words.split(" ");
				for (int i = 0; i < vals.length; i++) {
					vocab.add(vals[i]);
				}
			}
			for (String word : vocab) {
				pw.println(word);
			}
			br.close();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		DataFileProcess dfp = new DataFileProcess();
		String input = "data/semeval/sd_parseTag_test.txt";
		String output = "data/semeval/sd_parseTag_test_vocab.txt";
		dfp.exportVocabFileFromData(input, output);
	}

}
