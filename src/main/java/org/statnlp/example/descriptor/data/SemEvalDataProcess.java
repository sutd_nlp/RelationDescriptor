package org.statnlp.example.descriptor.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.statnlp.commons.io.RAWF;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.LabeledScoredTreeNode;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.trees.GrammaticalStructure.Extras;

public class SemEvalDataProcess {
	
	List<SemEvalObj> objs;
	private static final String TEMP = "data/semeval/temp.txt";
	private MaxentTagger tagger;
	private LexicalizedParser parser;
	private DependencyParser dependencyParser;
	
	private static final String MODEL_LEXICAL = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
	private static final String SD_MODEL = "edu/stanford/nlp/models/parser/nndep/english_SD.gz";
	private static final String UD_MODEL = "edu/stanford/nlp/models/parser/nndep/english_UD.gz";
	private boolean sdDependency = true;
	private boolean useParser = false;
	
	public SemEvalDataProcess(boolean useParser, boolean sdDep) {
		this.useParser = useParser;
		this.sdDependency = sdDep;
	}
	
	class SemEvalObj {
		int id;
		String rawSent;
		String rawRelation;
		List<CoreLabel> tokenizedSent;
		
		public SemEvalObj (int id, String rawSent, String rawRealtion) {
			this.id = id;
			this.rawSent = rawSent;
			this.rawRelation = rawRealtion;
		}
	}
	
	private void readRawFile(String inputPath) {
		try {
			BufferedReader br = RAWF.reader(inputPath);
			String line = null;
			this.objs = new ArrayList<>(8000);
			while ((line = br.readLine()) != null) {
				String[] data = line.split("\\t");
				int id = Integer.valueOf(data[0]);
				if (!data[1].startsWith("\"") || !data[1].endsWith("\"")) {
					throw new RuntimeException("not startwith?");
				}
				String rawSent = data[1].substring(1, data[1].length()-1);
				String rawRelation = br.readLine();
				SemEvalObj object = new SemEvalObj(id, rawSent, rawRelation);
				objs.add(object);
				br.readLine(); //comment
				br.readLine();
			}
			System.out.println("#obj sents: " + this.objs.size());
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	private void writeToDoc(String outputPath) {
		PrintWriter pw;
		try {
			pw = RAWF.writer(outputPath);
			for (SemEvalObj obj : this.objs) {
				pw.println(obj.rawSent);
			}
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void tokenizeDoc(String inputPath) {
		DocumentPreprocessor dp = new DocumentPreprocessor(inputPath);
		dp.setSentenceDelimiter("\n");
		int id = 0;
		for (List<HasWord> sentence : dp) {
			List<CoreLabel> coreLabelSent = new ArrayList<>(sentence.size());
			for (HasWord w : sentence) {
				CoreLabel word = new CoreLabel();
				word.setWord(w.word());
				coreLabelSent.add(word);
			}
			this.objs.get(id).tokenizedSent = coreLabelSent;
	        id++;
	    }
		System.out.println("#sents: " + id);
	}
	
	private void formatData(String outputPath) {
		try {
			PrintWriter pw = RAWF.writer(outputPath);
			//sentence
			//e1Start,e1End e2Start,e2End relation||relation_rev
			for (SemEvalObj obj : this.objs) {
				StringBuilder sentence = new StringBuilder();
				boolean[] flags = new boolean[4];
				Arrays.fill(flags, false);
				int e1Start = -1, e1End = -1, e2Start = -1, e2End = -1;
				List<CoreLabel> cleanedSent = new ArrayList<CoreLabel>(obj.tokenizedSent.size() - 4);
				for (int i = 0; i < obj.tokenizedSent.size(); i++) {
					String w = obj.tokenizedSent.get(i).word();
					if (w.equals("<e1>")) {
						flags[0] = true;
						e1Start = cleanedSent.size();
						continue;
					} else if (w.equals("</e1>")) {
						e1End = cleanedSent.size() - 1;
						flags[1] = true;
						continue;
					} else if (w.equals("<e2>")) {
						e2Start = cleanedSent.size();
						flags[2] = true;
						continue;
					} else if (w.equals("</e2>")) {
						e2End = cleanedSent.size() - 1;
						flags[3] = true;
						continue;
					}
					CoreLabel word = new CoreLabel();
					word.setWord(w);
					cleanedSent.add(word);
					if (sentence.toString().equals("")) {
						sentence.append(w);
					} else { 
						sentence.append(" " + w);
					}
				}
				for (boolean f : flags) {
					if (!f) {
						System.err.println(obj.rawSent);
						throw new RuntimeException ("invalid sentence");
					}
				}
				if (this.useParser) {
					cleanedSent = ((LabeledScoredTreeNode)this.parser.parse(cleanedSent)).taggedLabeledYield();
				} else {
					this.tagger.tagCoreLabels(cleanedSent);
				}
				StringBuilder tags = new StringBuilder();
				for (int p = 0; p < cleanedSent.size(); p++) {
					if (p == 0) {
						tags.append(cleanedSent.get(p).tag());
					} else {
						tags.append(" " + cleanedSent.get(p).tag());
					}
					
				}
				GrammaticalStructure structure = dependencyParser.predict(cleanedSent);
				List<TypedDependency> deps =  structure.typedDependencies(Extras.NONE);
				int[] heads = new int[cleanedSent.size()];
				String[] depLabels = new String[cleanedSent.size()];
				for (int i = 0; i < deps.size(); i++) {
					TypedDependency typedDep = deps.get(i);
					heads[typedDep.dep().index() - 1] = typedDep.gov().index() - 1;
					depLabels[typedDep.dep().index() - 1] = typedDep.reln().getShortName();
				}
				StringBuilder headSeq = new StringBuilder();
				for (int p = 0; p < cleanedSent.size(); p++) {
					if (p == 0) {
						headSeq.append(heads[p] + "");
					} else {
						headSeq.append(" " + heads[p]);
					}
				}
				StringBuilder depLabelSeq = new StringBuilder();
				for (int p = 0; p < cleanedSent.size(); p++) {
					if (p == 0) {
						depLabelSeq.append(depLabels[p] + "");
					} else {
						depLabelSeq.append(" " + depLabels[p]);
					}
				}
				pw.println(sentence.toString());
				pw.println(tags.toString());
				pw.println(headSeq.toString());
				pw.println(depLabelSeq.toString());
				pw.println(e1Start + "," + e1End + " " + e2Start + "," + e2End);
				if (e1Start < 0 || e1End < 0 || e2Start < 0 || e2End < 0) {
					throw new RuntimeException("index is smaller than 0");
				}
				if (e1Start > e1End || e2Start > e2End) {
					throw new RuntimeException("start larger than end");
				}
				String relation = null;
				if (obj.rawRelation.endsWith("(e1,e2)")) {
					relation = obj.rawRelation.replace("(e1,e2)", "");
				} else {
					relation = obj.rawRelation.replace("(e2,e1)", "-rev");
				}
				pw.println(relation);
				pw.println();
			}
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void loadTagger () {
		tagger = new MaxentTagger(MaxentTagger.DEFAULT_JAR_PATH);
//		for (SemEvalObj obj : this.objs) {
//			List<CoreLabel> sentence = obj.tokenizedSent;
//			tagger.tagCoreLabels(sentence);
//		}
	}
	
	private void loadParser () {
		parser = LexicalizedParser.loadModel(MODEL_LEXICAL);
	}
	
	private void loadDepParser() {
		if (this.sdDependency) {
			System.out.println("Using Stanford Dependency..");
			dependencyParser = DependencyParser.loadFromModelFile(SD_MODEL);
		}
		else {
			System.out.println("Using Universal Dependency..");
			dependencyParser = DependencyParser.loadFromModelFile(UD_MODEL);
		}
	}
	
	public void preprocess (String inputPath, String outputPath) {
		this.readRawFile(inputPath);
		this.writeToDoc(TEMP);
		this.tokenizeDoc(TEMP);
		if (this.useParser) {
			this.loadParser();
		} else {
			this.loadTagger();
		}
		this.loadDepParser();
		this.formatData(outputPath);
	}
	
	public void test(){
		this.loadTagger();
		System.out.println(tagger.tagString("Reduce heat, cover and cook 15-20 minutes or until tender"));
		
		String MODEL_LEXICAL = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
		parser = LexicalizedParser.loadModel(MODEL_LEXICAL);
		Tree tree = parser.parse("Reduce heat, cover and cook 15-20 minutes or until tender");
		tree = (LabeledScoredTreeNode)tree;
		System.out.println(tree.taggedYield());
		
		String STANDARD_ENGLISH = "edu/stanford/nlp/models/parser/nndep/english_SD.gz";
		DependencyParser dependencyParser = DependencyParser.loadFromModelFile(STANDARD_ENGLISH);
		GrammaticalStructure structure = dependencyParser.predict(tree.taggedYield());
		System.out.println(structure.typedDependencies(Extras.NONE));
	}
	
	public static void main(String[] args) {
		boolean useParser = true;
		boolean useSD = false;
		SemEvalDataProcess sedp = new SemEvalDataProcess(useParser, useSD);
//		sedp.preprocess("data/semeval/TRAIN_FILE.txt", "data/semeval/ud_parseTag_train.txt");
		sedp.preprocess("data/semeval/TEST_FILE_FULL.txt", "data/semeval/ud_parseTag_test.txt");
//		sedp.test();
	}
}
