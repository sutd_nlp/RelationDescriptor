package org.statnlp.example.descriptor.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.io.RAWF;

public class HypernymProcess {

	/**
	 * Convert sd_parse_file to the format read by supersense tagger
	 * @param file
	 * @param output
	 */
	public void sdParseFile2RowFile(String file, String output) {
		try {
			BufferedReader br = RAWF.reader(file);
			PrintWriter pw = RAWF.writer(output);
			String line = null;
			//int index = 1;
			while ((line = br.readLine()) != null) {
				String words = line;
				br.readLine(); //pos
				br.readLine();// heads
				br.readLine(); //depLabel
				br.readLine();//entity info
				br.readLine(); //relationlabel;
				br.readLine(); //emptyLine.
				String[] vals = words.split(" ");
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < vals.length; i++) {
					builder.append(i==0? vals[i] : " " + vals[i]);
				}
				pw.println(builder.toString());
				//index++;
			}
			br.close();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private List<String> readHypernymLines (String hypernymFile) {
		List<String> list = new ArrayList<>();
		try {
			BufferedReader br = RAWF.reader(hypernymFile);
			String line = null;
			while((line = br.readLine()) != null) {
				String[] vals = line.split(" ");
				assert(vals.length%5 == 0);
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < vals.length; i++) {
					if ((i + 1) % 5  == 0) {
						sb.append(i==4 ? vals[i-1] : " " + vals[i-1]);
//						String hypernym = vals[i-1];
//						if (!hypernym.equals("0")) {
//							assert(hypernym.split("\\.").length == 2);
//							System.out.println(hypernym.split("\\.")[1]);
//						}
					}
				}
				list.add(sb.toString());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	private List<String> readNERLines (String hypernymFile) {
		List<String> list = new ArrayList<>();
		try {
			BufferedReader br = RAWF.reader(hypernymFile);
			String line = null;
			while((line = br.readLine()) != null) {
				String[] vals = line.split(" ");
				assert(vals.length%5 == 0);
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < vals.length; i++) {
					if ((i + 1) % 5  == 0) {
						sb.append(i==4 ? vals[i] : " " + vals[i]);
//						String hypernym = vals[i-1];
//						if (!hypernym.equals("0")) {
//							assert(hypernym.split("\\.").length == 2);
//							System.out.println(hypernym.split("\\.")[1]);
//						}
					}
				}
				list.add(sb.toString());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public void includeHypernymAndNERIntoDataFile(String sdParseFile, String hypernymFile, String output) {
		try {
			List<String> hyperStrs = this.readHypernymLines(hypernymFile);
			List<String> nerStrs = this.readNERLines(hypernymFile);
			BufferedReader dataReader = RAWF.reader(sdParseFile);
			PrintWriter pw = RAWF.writer(output);
			String line = null;
			int index = 0;
			while((line = dataReader.readLine()) != null) {
				pw.println(line); //word
				line = dataReader.readLine(); //pos
				pw.println(line); //pos
				line = dataReader.readLine();// heads
				pw.println(line); //heads
				line = dataReader.readLine(); //depLabel
				pw.println(line); //depLabel
				pw.println(hyperStrs.get(index)); //wordnet
				pw.println(nerStrs.get(index)); //ners
				line = dataReader.readLine();//entity info
				pw.println(line); //entity
				line = dataReader.readLine(); //relationlabel;
				pw.println(line); //relationlabel
				
				line = dataReader.readLine(); //emptyLine.
				pw.println(line);
				index++;
			}
			pw.close();
			dataReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		String sdParseFile = "data/semeval/sd_parseTag_nosst_train.txt";
		String hypernymFile = "data/semeval/train_sst.tags";
		String outputFile = "data/semeval/sd_parseTag_sst_train.txt";
//		String sstFile = "data/semeval/train_sst.txt";
		HypernymProcess p = new HypernymProcess();
		p.includeHypernymAndNERIntoDataFile(sdParseFile, hypernymFile, outputFile);
		sdParseFile = "data/semeval/sd_parseTag_nosst_test.txt";
		hypernymFile = "data/semeval/test_sst.tags";
		outputFile = "data/semeval/sd_parseTag_sst_test.txt";
		p.includeHypernymAndNERIntoDataFile(sdParseFile, hypernymFile, outputFile);
//		p.sdParseFile2RowFile(sdParseFile, sstFile);
//		sdParseFile = "data/semeval/sd_parseTag_test.txt";
//		sstFile = "data/semeval/test_sst.txt";
//		p.sdParseFile2RowFile(sdParseFile, sstFile);
	}
}
