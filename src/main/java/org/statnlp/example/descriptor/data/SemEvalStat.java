package org.statnlp.example.descriptor.data;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.descriptor.Config;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationType;

public class SemEvalStat {

	public static void countRelation(Instance[] trainData) {
		int[] totalNums = new int[RelationType.RELS.size()];
		for (Instance inst : trainData) {
			RelInstance res = (RelInstance)inst;
			RelationType gold = res.getOutput().type;
			String goldForm = gold.form;
			int corrGoldId = gold.id;
			if (goldForm.endsWith(Config.REV_SUFF)) {
				corrGoldId = RelationType.get(goldForm.replace(Config.REV_SUFF, "")).id;
			}
			totalNums[corrGoldId]++;
		}
		int sum = 0;
		for (int r = 0; r < RelationType.RELS.size(); r++) {
			if (RelationType.get(r).form.equals(Config.NR) || RelationType.get(r).form.endsWith(Config.REV_SUFF)) continue;
			sum += totalNums[r]; 
		}
		for (int r = 0; r < RelationType.RELS.size(); r++) {
			if (RelationType.get(r).form.equals(Config.NR) || RelationType.get(r).form.endsWith(Config.REV_SUFF)) continue;
			System.out.printf("%s:\t%d\t%.2f%%\n", RelationType.get(r).form, totalNums[r], totalNums[r] * 1.0 / sum * 100);
		}
		System.out.println();
	}
	
}
