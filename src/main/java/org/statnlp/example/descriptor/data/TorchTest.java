package org.statnlp.example.descriptor.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.statnlp.commons.io.RAWF;

public class TorchTest {
	
	public static void noSSTFileToPureTxtAnn(String file, String output, int limit) {
		try {
			BufferedReader br = RAWF.reader(file);
			PrintWriter pw = RAWF.writer(output + "1.txt");
			PrintWriter pwAnn = RAWF.writer(output + "1.ann");
			String line = null;
			int currentOffset = -1;
			int tidx = 1;
			int num = 0;
			int fileIdx = 1;
			while ((line = br.readLine()) != null) {
				String words = line;
				br.readLine(); //pos
				br.readLine();// heads
				br.readLine(); //depLabel
				String entity = br.readLine();//entity info
				br.readLine();//relation label
				br.readLine(); //emptyline;
				words = words.trim();
				pw.println(words);
				String[] entIdxs = entity.split(" ");
				String[] ent1Idxs = entIdxs[0].split(",");
				String[] ent2Idxs = entIdxs[1].split(",");
				int e1Start = Integer.valueOf(ent1Idxs[0]);
				int e1End = Integer.valueOf(ent1Idxs[1]);
				int e2Start = Integer.valueOf(ent2Idxs[0]);
				int e2End = Integer.valueOf(ent2Idxs[1]);
				int e1StartOffset = 0;
				int e1EndOffset = 0;
				int e2StartOffset = 0;
				int e2EndOffset = 0;
				String[] vals = words.split(" ");
				
				StringBuilder e1Builder = new StringBuilder();
				StringBuilder e2Builder = new StringBuilder();
				for (int i = e1Start; i <= e1End; i++) {
					e1Builder.append(i == e1Start ? vals[i] : " " + vals[i]);
				}
				for (int i = e2Start; i <= e2End; i++) {
					e2Builder.append(i == e2Start ? vals[i] : " " + vals[i]);
				}
//				System.out.println("words length: " + words.length());
				for (int i = 0; i < vals.length; i++) {
//					System.out.println( "offset info: " + i + " ," + currentOffset);
					if (i == e1Start) {
						e1StartOffset = i == 0 ? currentOffset + 1 : currentOffset + 2;
						currentOffset = i == 0 ? currentOffset + vals[i].length() : currentOffset + vals[i].length() + 1;
					}
					if (i == e1End) {
						if (e1Start == e1End) e1EndOffset = e1StartOffset + vals[i].length() - 1;
						else {
							e1EndOffset = currentOffset + vals[i].length() + 1; 
							currentOffset += vals[i].length() + 1;
						}
					}
					if (i == e2Start) {
						e2StartOffset = currentOffset + 2;
						currentOffset += vals[i].length() + 1;
					}
					if (i == e2End) {
						if (e2Start == e2End) e2EndOffset = e2StartOffset + vals[i].length() - 1;
						else {
							e2EndOffset = currentOffset + vals[i].length() + 1;
							currentOffset += vals[i].length() + 1;
						}
					}
					if (i != e1Start && i != e1End && i != e2Start && i!= e2End) {
						currentOffset = i == 0 ? currentOffset + vals[i].length() : currentOffset + vals[i].length() + 1;
					}
				}
				pwAnn.printf("T%d\tEntity %d %d\t%s\n", tidx, e1StartOffset, e1EndOffset + 1, e1Builder.toString() );
				tidx++;
				pwAnn.printf("T%d\tEntity %d %d\t%s\n", tidx,e2StartOffset,e2EndOffset + 1,e2Builder.toString() );
				tidx++;
				num++;
				currentOffset += 2; //because of new line.
				if (num == limit) {
					pw.close();
					pwAnn.close();
					fileIdx++;
					pw = RAWF.writer(output + fileIdx + ".txt" );
					pwAnn = RAWF.writer(output + fileIdx + ".ann");
					num = 0;
					tidx = 1;
					currentOffset = -1;
//					break;
				}
			}
			pw.close();
			pwAnn.close();
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	/**
	 * nosst -> augment position indicator
	 * @param file
	 * @param output
	 */
	public static void sdParseFileWithPI(String file, String output) {
		try {
			BufferedReader br = RAWF.reader(file);
			PrintWriter pw = RAWF.writer(output);
			String line = null;
			while ((line = br.readLine()) != null) {
				String words = line;
				String pos = br.readLine(); //pos
				String heads = br.readLine();// heads
				String depLabel = br.readLine(); //depLabel
				String entity = br.readLine();//entity info
				String[] entIdxs = entity.split(" ");
				String[] ent1Idxs = entIdxs[0].split(",");
				String[] ent2Idxs = entIdxs[1].split(",");
				int e1Start = Integer.valueOf(ent1Idxs[0]);
				int e1End = Integer.valueOf(ent1Idxs[1]);
				int e2Start = Integer.valueOf(ent2Idxs[0]);
				int e2End = Integer.valueOf(ent2Idxs[1]);
				String relationLabel = br.readLine(); //relationlabel;
				br.readLine(); //emptyLine.
				String[] vals = words.split(" ");
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < vals.length; i++) {
					if (e1Start == i) {
						builder.append(i == 0 ? "<e1>" : " <e1>");
						builder.append(" " + vals[i]);
					}
					if (e1End == i) {
						if (e1Start != e1End)
							builder.append(" " + vals[i]);
						builder.append(" </e1>");
					}
					if (i == e2Start) {
						builder.append(" <e2>");
						builder.append(" " + vals[i]);
					}
					if (i == e2End) {
						if (e2Start != e2End)
							builder.append(" " + vals[i]);
						builder.append(" </e2>");
					}
					if (i != e1Start && i != e1End && i != e2Start && i != e2End) {
						builder.append(i == 0 ? vals[i] : " " + vals[i]);
					}
				}
				pw.println(builder.toString());
				pw.println(pos);
				pw.println(heads);
				pw.println(depLabel);
				pw.println(entity);
				pw.println(relationLabel);
				pw.println();
			}
			br.close();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
//		sdParseFileWithPI("data/semeval/sd_parseTag_nosst_train.txt", "data/semeval/sd_parseTag_pi_train.txt");
//		sdParseFileWithPI("data/semeval/sd_parseTag_nosst_test.txt", "data/semeval/sd_parseTag_pi_test.txt");
		noSSTFileToPureTxtAnn("data/semeval/sd_parseTag_nosst_test.txt", "data/smallAnnotate/sd_parse_small_test_ann", 100);
//		noSSTFileToPureTxtAnn("data/semeval/testtmp.txt", "data/semeval/testtmp_ann.txt", "data/semeval/testtmp_ann.ann", 38);
	}
}
