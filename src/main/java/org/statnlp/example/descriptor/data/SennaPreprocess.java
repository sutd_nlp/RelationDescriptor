package org.statnlp.example.descriptor.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.statnlp.commons.io.RAWF;

/**
 * Put the word into the senna embeddings txt
 * @author Allan (allanmcgrady@gmail.com)
 *
 */
public class SennaPreprocess {
	
	public static void readAndWriteSennaEmbed(String wordFile, String embeddingFile, String outputEmbedFile) {
		try {
			BufferedReader br = RAWF.reader(wordFile);
			BufferedReader brEmbed = RAWF.reader(embeddingFile);
			PrintWriter pw = RAWF.writer(outputEmbedFile);
			String wLine = null;
			String embedLine = null;
			while ((wLine = br.readLine())!= null) {
				embedLine = brEmbed.readLine();
				pw.println(wLine + " " + embedLine);
			}
			pw.close();
			brEmbed.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		String wordFile = "/Users/allanjie/allan/data/senna/hash/words.lst";
		String embeddingFile = "/Users/allanjie/allan/data/senna/embeddings/embeddings.txt";
		String outputEmbeddingFile = "/Users/allanjie/allan/data/senna_embeddings.txt";
		readAndWriteSennaEmbed(wordFile, embeddingFile, outputEmbeddingFile);
	}

}
