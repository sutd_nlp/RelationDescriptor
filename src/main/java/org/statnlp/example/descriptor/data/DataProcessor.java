package org.statnlp.example.descriptor.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.statnlp.commons.types.Instance;

public class DataProcessor {

	public DataProcessor() {
		
	}
	
	/**
	 * Shuffle the instances given random seed 
	 * @param insts
	 * @param rand
	 * @return
	 */
	public Instance[] shuffle(Instance[] insts, Random rand) {
		List<Instance> list = new ArrayList<>(insts.length);
		for (int i = 0; i < insts.length; i++) {
			list.add(insts[i]);
		}
		Collections.shuffle(list, rand);
		System.out.println("[Info] shuffle insts:"+list.size());
		return list.toArray(new Instance[list.size()]);
	}
	
	/**
	 * Split the instances into train/test splits
	 * @param allInsts
	 * @param rand
	 * @param trainPortion
	 * @return
	 */
	public Instance[][] splitInstIntoTrainTest(Instance[] allInsts, Random rand, double trainPortion) {
		Instance[][] splits = new Instance[2][];
		if (trainPortion >= 1) {
			throw new RuntimeException("without test data?");
		}
		int trainNum = (int)(allInsts.length * trainPortion);
		splits[0] = new Instance[trainNum];
		splits[1] = new Instance[allInsts.length - trainNum];
		for (int i = 0; i < trainNum; i++) {
			splits[0][i] = allInsts[i];
			allInsts[i].setLabeled();
		}
		for (int i = trainNum; i < allInsts.length; i++) {
			splits[1][i - trainNum] = allInsts[i];
			allInsts[i].setUnlabeled();
		}
		System.out.println("#number of trainInsts: " + splits[0].length);
		System.out.println("#number of testInsts: " + splits[1].length);
		return splits;
	}
	
	public Instance[][] splitIntoKFolds(Instance[] allInsts, int k) {
		Instance[][] splits = new Instance[k][];
		int num = allInsts.length / k; 
		for (int f = 0; f < k; f++) {
			int currNum = f != k - 1 ? num : allInsts.length - (k - 1) * num;
			splits[f] = new Instance[currNum];
			for (int i = 0; i < currNum; i++) {
				splits[f][i] = allInsts[f * num + i];
			}
//			System.out.printf("[Info] Fold %d: %d\n", (f+1), splits[f].length);
		}
		return splits;
	}
	
	public Instance[][] combAlongTheFirstDim(Instance[][] one, Instance[][] two) {
		System.out.println("Combining..");
		Instance[][] insts = new Instance[one.length][];
		for (int i = 0; i < insts.length; i++) {
			insts[i] = new Instance[one[i].length + two[i].length];
			for (int t = 0; t < one[i].length; t++) {
				insts[i][t] = one[i][t];
				insts[i][t].setUnlabeled();
			}
			for (int t = 0; t < two[i].length; t++) {
				insts[i][one[i].length + t] = two[i][t];
				insts[i][one[i].length + t].setUnlabeled();
			}
//			System.out.printf("[Info] Fold %d: %d\n", (i+1), insts[i].length);
		}
		return insts;
	}
	
}
