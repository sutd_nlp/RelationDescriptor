package org.statnlp.example.descriptor.split;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationDescriptorEvaluator;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.example.descriptor.eval.Annotation;
import org.statnlp.example.descriptor.eval.Candidate;
import org.statnlp.example.descriptor.eval.DesEval;
import org.statnlp.example.descriptor.eval.EvalMain.SetSize;
import org.statnlp.example.descriptor.eval.EvalReader;
import org.statnlp.example.descriptor.semeval.SemEvalDataReader;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;
import org.statnlp.hypergraph.StringIndex;

import cern.colt.Arrays;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class SPlitMain {

	public static double l2val = 0.0001;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static String trainFile = "data/semeval/sd_parseTag_train.txt";
	public static String testFile = "data/semeval/sd_parseTag_test.txt";
	public static int trainNum = -1;
	public static int testNum = -1;
	public static String resultFile = "results/lds_semeval_res.txt";
	public static String annFile = "results/final_annotation.txt";
	public static boolean saveModel = false;
	public static boolean readModel = false;
	public static String modelFile = "models/lds_semeval_model.m";
	public static OptimizerFactory optimizer = OptimizerFactory.getLBFGSFactory();
	public static boolean readPreLabels = true;
	public static String preLabelsFile = "data/semeval/prevlabels.txt";
	
	public static int latentNeighbor = 1;
	public static boolean useSpanPenalty = true;
	public static double penalty = 30;
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		setArgs(args);
		
		/***
		 * Parameter settings and model configuration
		 */
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.OPTIMIZE_NEURAL = true;
		NetworkConfig.PRINT_BATCH_OBJECTIVE = false;
		NetworkConfig.USE_FEATURE_VALUE = true;
		if (readPreLabels) {
			readPreLabels(preLabelsFile);
		}
		SemEvalDataReader reader = new SemEvalDataReader();

		/**debug info**/
		/***/
		
		Instance[] trainData = reader.read(trainFile, true, trainNum, true, null, false);
		trainData = splitInsts(trainData, latentNeighbor);
		reader.putInMaxLen(trainData);
		int contNum = 0;
		int desNum = 0;
		for (Instance ins: trainData) {
			RelInstance inst = (RelInstance)ins;
			CandidatePair cp = inst.getInput();
			if (cp.hasDes) {
				desNum++;
				if (cp.isContinuous) {
					contNum++;
				}
			} 
		}
		System.out.println("cont num: " + contNum + " des NUm: " + desNum);
		Instance[] testData = reader.read(testFile, false, testNum, true, null, false);
		reader.putInMaxLen(testData);
		
		System.out.println(RelationType.RELS.toString());
		System.out.println("#labels: " + RelationType.RELS.size());
		RelationDescriptorEvaluator evaluator = new RelationDescriptorEvaluator(true);
		NetworkModel model = null;
		if (readModel) {
			System.out.println("[Info] Reading Model....");
			ObjectInputStream in = RAWF.objectReader(modelFile);
			model = (NetworkModel)in.readObject();
		} else {
            GlobalNetworkParam gnp = new GlobalNetworkParam(optimizer);
            gnp.setStoreFeatureReps();
            SplitFeatureManager tfm = new SplitFeatureManager(gnp);
            SplitCompiler tnc = new SplitCompiler();
			model = DiscriminativeNetworkModel.create(tfm, tnc);
			model.train(trainData, numIteration);
			gnp.getStringIndex().buildReverseIndex();
		}
		if (saveModel) {
			ObjectOutputStream out = RAWF.objectWriter(modelFile);
			out.writeObject(model);
			out.close();
		}
		
		/***Tuning the Feature Weights***/
		if (readModel && useSpanPenalty) {
			GlobalNetworkParam gnp = model.getFeatureManager().getParam_G();
			StringIndex strIdx = gnp.getStringIndex();
			strIdx.buildReverseIndex();
			gnp.setStoreFeatureReps();
			for (int i = 0; i < gnp.size(); i++) {
				int[] fs = gnp.getFeatureRep(i);
				String type = strIdx.get(fs[0]);
				if (type.equals("spanlen")) {
					System.out.println(gnp.getWeight(i));
					gnp.overRideWeight(i, penalty);
					System.out.println(gnp.getWeight(i));
					break;
				}
			}
		}
		/*************/
		
		/**
		 * Testing Phase
		 */
		Instance[] results = model.test(testData);
		evaluator.evaluateRelation(results);
		
		//print the results
		PrintWriter pw = RAWF.writer(resultFile);
		for (Instance res : results) {
			RelInstance inst = (RelInstance)res;
			Sentence sent = inst.getInput().sent;
			List<Span> spans = inst.getInput().spans;
			Span arg1Span = spans.get(inst.getInput().leftSpanIdx);
			Span arg2Span = spans.get(inst.getInput().rightSpanIdx);
			RelationDescriptor gold = inst.getOutput();
			RelationDescriptor pred = inst.getPrediction();
			String goldForm = gold.getType().form;
			String predForm = pred.getType().form;
			pw.println(sent.toString());
			pw.println(arg1Span.start+","+arg1Span.end + " " + arg2Span.start+","+arg2Span.end + " "
					+ gold.getLeft() + "," + gold.getRight() + " " + pred.getLeft() + "," + pred.getRight());
			pw.println(goldForm + "," + predForm);
			pw.println();
		}
		pw.close();
		Annotation[] anns = EvalReader.readAnnotations(annFile);
		Candidate[] cans = EvalReader.readRDCandidate(resultFile);
		//check length
		for (int i = 0; i < cans.length; i++) {
			if (anns[i].sent.length != cans[i].sent.length) {
				System.err.println(i + ": " + Arrays.toString(anns[i].sent));
				System.err.println(i + ": " + Arrays.toString(cans[i].sent));
				throw new RuntimeException("sent length not equal");
			}
		}
		for (SetSize ss : SetSize.values()) {
			System.out.println(ss);
			DesEval.evaluateTokenAndExactMatchDescriptor(anns, cans, ss, null, null);
		}
	}
	
	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Latent Linear-chain CRF Model for Relation Extraction");
		parser.addArgument("-t", "--thread").type(Integer.class).setDefault(8).help("number of threads");
		parser.addArgument("--l2").type(Double.class).setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--iter").type(Integer.class).setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--trainFile").setDefault(trainFile).help("The path of the traininigFile file");
		parser.addArgument("--testFile").setDefault(testFile).help("The path of the testFile file");
		parser.addArgument("--trainNum").type(Integer.class).setDefault(trainNum).help("The number of training instances");
		parser.addArgument("--testNum").type(Integer.class).setDefault(testNum).help("The number of testing instances");
		parser.addArgument("--saveModel").type(Boolean.class).setDefault(saveModel).help("whether to save the model");
		parser.addArgument("--readModel").type(Boolean.class).setDefault(readModel).help("whether to read the model");
		parser.addArgument("--modelFile").setDefault(modelFile).help("specify the model file");
		parser.addArgument("--resFile").setDefault(resultFile).help("specify the result file");
		parser.addArgument("--os").setDefault(NetworkConfig.OS).help("linux, osx");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = ns.getInt("thread");
        l2val = ns.getDouble("l2");
        numIteration = ns.getInt("iter");
        trainFile = ns.getString("trainFile");
        testFile = ns.getString("testFile");
        trainNum = ns.getInt("trainNum");
        testNum = ns.getInt("testNum");
        saveModel = ns.getBoolean("saveModel");
        readModel = ns.getBoolean("readModel") ;
        modelFile = ns.getString("modelFile");
        resultFile = ns.getString("resFile");
        NetworkConfig.OS = ns.getString("os");
        if (saveModel && readModel) {
        	throw new RuntimeException("save model and read model can't be both true");
        }
        System.err.println(ns.getAttrs().toString());
	}
	
	/**
	 * Reading the label list
	 * @param file
	 */
	private static void readPreLabels(String file) {
		BufferedReader br;
		try {
			br = RAWF.reader(file);
			String line = null; 
			while ((line = br.readLine()) != null) {
				RelationType.get(line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static RelInstance[] splitInsts(Instance[] insts, int ln) {
		List<Instance> list = new ArrayList<>();
		for (Instance ins : insts) {
			RelInstance inst = (RelInstance)ins;
			RelationDescriptor rd = inst.getOutput();
			CandidatePair input = inst.getInput();
			Sentence sent = input.sent;
			int leftIdx = input.leftSpanIdx;
			int rightIdx = input.rightSpanIdx;
			Span leftSpan = input.spans.get(leftIdx);
			Span rightSpan = input.spans.get(rightIdx); 
			int[] sdps = getShortestDepPath(sent, leftSpan, rightSpan);
			int[] startEnds = getStartEndFromSDPs(leftSpan.end, rightSpan.start, sdps);
			if (startEnds[0] == -1) {
				rd.setLeft(-1);
				rd.setRight(-1);
				inst.setLabeled();
				list.add(inst);
				continue;
			} else {
				int rdpStart = startEnds[0];
				int rdpEnd = startEnds[1];
				for (int sdiff = 0; sdiff <= ln; sdiff++) {
					for (int ediff = 0; ediff <= ln; ediff++) {
						if (rdpStart - sdiff <= rdpEnd + ediff &&
								rdpStart - sdiff > leftSpan.end && 
								rdpEnd + ediff < rightSpan.start) {
							RelInstance new_Inst = inst.duplicate();
							new_Inst.setLabeled();
							new_Inst.getOutput().setLeft(rdpStart - sdiff);
							new_Inst.getOutput().setRight(rdpEnd + ediff);
							list.add(new_Inst);
						}
					}
				}
			}
		}
		return list.toArray(new RelInstance[list.size()]);
	}
	
	private static int[] getStartEndFromSDPs(int leftBound, int rightBound, int[] sdps) {
		int[] startEnd = new int[2];
		startEnd[0] = -1;
		startEnd[1] = -1;
		for (int i = 0; i < sdps.length; i++) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[0] = i; break;}
		}
		for (int i = sdps.length - 1; i >= 0; i--) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[1] = i; break;}
		}
		return startEnd;
	}
	
	private static int[] getShortestDepPath(Sentence sent, Span arg1Span, Span arg2Span) {
		Graph<Integer, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
		int[] des = new int[sent.length()];
		for (int i = 0; i < sent.length(); i++) {
			g.addVertex(i);
		}
		for (int i = 0; i < sent.length(); i++) {
			int head = sent.get(i).getHeadIndex();
			if (head != -1) {
				g.addEdge(i, head);
			}
		}
		DijkstraShortestPath<Integer, DefaultEdge> dg = new DijkstraShortestPath<>(g);
		GraphPath<Integer, DefaultEdge>  results = dg.getPath(arg1Span.headIdx, arg2Span.headIdx);
		List<Integer> list =  results.getVertexList();
		StringBuilder sb = new StringBuilder();
		for (int v : list) {
			if (v >= arg1Span.start && v <= arg1Span.end) continue;
			if (v >= arg2Span.start && v <= arg2Span.end) continue;
			if (v == arg1Span.headIdx || v == arg2Span.headIdx) continue;
			des[v] = 1;
			sb.append(sent.get(v).getForm() + " ");
		}
		return des;
	}

} 
