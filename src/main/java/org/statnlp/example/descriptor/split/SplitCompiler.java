package org.statnlp.example.descriptor.split;

import java.util.Arrays;

import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class SplitCompiler extends NetworkCompiler {
	
	private static final long serialVersionUID = 3567628608697514488L;

	public enum NodeType {leaf, des, rel, root};//relaiton is node, always fix
	
	private boolean DEBUG = true;
	
	//private boolean inBetween = false; //for now, always inbetween
	//private int leftRightBoundary = -1; //w.r.t the two arguments
	
	static {
		NetworkIDMapper.setCapacity(new int[]{4, 200, 200, RelationType.RELS.size()});
	}
	
	public SplitCompiler() {
	}

	private long toNode_leaf() {
		return toNode(NodeType.leaf, 0, 0, 0); 
	}
	
	private long toNode_Span(int leftIndex, int rightIndex){
		return toNode(NodeType.des, leftIndex, rightIndex, 0);
	}
	
	private long toNode_Rel(int label) {
		return toNode(NodeType.rel, 0, 0, label);
	}
	
	private long toNode_root() {
		return toNode(NodeType.root, 0, 0, 0);
	}
	
	private long toNode(NodeType nodeType, int leftIndex, int rightIndex, int labelId) {
		return NetworkIDMapper.toHybridNodeID(new int[]{nodeType.ordinal(), leftIndex, rightIndex, labelId});
	}
	
	@Override
	public Network compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance rInst = (RelInstance)inst;
		CandidatePair input = rInst.getInput();
		Sentence sent = input.sent;
		RelationDescriptor descriptor = rInst.getOutput();
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(descriptor.getType().id);
		builder.addNode(node);
		if (descriptor.getLeft() < 0) {
			builder.addEdge(node, new long[]{leaf});
		} else {
			long span = this.toNode_Span(descriptor.getLeft(), descriptor.getRight());
			builder.addNode(span);
			builder.addEdge(node, new long[]{span});
			builder.addEdge(span, new long[]{leaf});
		}
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network)) {
				System.err.println(sent.toString());
				System.err.println("not contains");
				throw new RuntimeException("network is not contained");
			}
				
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance rInst = (RelInstance)inst;
		CandidatePair input = rInst.getInput();
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		RelationDescriptor descriptor = rInst.getOutput();
		Span leftSpan = input.spans.get(leftIdx);
		Span rightSpan = input.spans.get(rightIdx); 
//		int[] sdps = this.getShortestDepPath(sent, leftSpan, rightSpan);
//		int[] startEnds = this.getStartEndFromSDPs(leftSpan.end, rightSpan.start, sdps);
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(descriptor.getType().id);
		builder.addNode(node);
		for (int dl = leftSpan.end + 1; dl < rightSpan.start; dl++) {
			for (int dr = dl; dr < rightSpan.start; dr++) {
				long span = this.toNode_Span(dl, dr);
				builder.addNode(span);
				builder.addEdge(node, new long[]{span});
				builder.addEdge(span, new long[]{leaf});
			}
		}
		builder.addEdge(node, new long[]{leaf});
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		return network;
	}

	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root();
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
		int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
		int labelId = arr[3];
		RelationType predType = RelationType.get(labelId);
		nodeIdx = baseNetwork.getMaxPath(labeledNodeIdx)[0];
		arr = baseNetwork.getNodeArray(nodeIdx);
		int left = arr[1];
		int right = arr[2];
		if (arr[0] == NodeType.leaf.ordinal()) {
			left = -1;
			right = -1;
		}
		RelationDescriptor prediction = new RelationDescriptor(predType, left, right);
		inst.setPrediction(prediction);
		return inst;
	}
	
	
}
