package org.statnlp.example.descriptor.normal;

import java.util.Arrays;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class LRNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -3523483926592577270L;

	public enum NodeType {LEAF, SPAN, NODE, ROOT};
	private boolean DEBUG = false;
	
	private boolean latentDescriptor;
	
	public LRNetworkCompiler(boolean useLatentSpan) {
		//nodeType, leftIndex, rightIndex, relation type.
		NetworkIDMapper.setCapacity(new int[]{4, 200, 200, RelationType.RELS.size()});
		this.latentDescriptor = useLatentSpan;
	}

	private long toNode_leaf() {
		return toNode(NodeType.LEAF, 0, 0, 0); 
	}
	
	private long toNode_Span(int leftIndex, int rightIndex){
		return toNode(NodeType.SPAN, leftIndex, rightIndex, 0);
	}
	
	private long toNode_Rel(int label) {
		return toNode(NodeType.NODE, 0, 0, label);
	}
	
	private long toNode_root() {
		return toNode(NodeType.ROOT, 0, 0, 0);
	}
	
	private long toNode(NodeType nodeType, int leftIndex, int rightIndex, int labelId) {
		return NetworkIDMapper.toHybridNodeID(new int[]{nodeType.ordinal(), leftIndex, rightIndex, labelId});
	}

	@Override
	public BaseNetwork compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance lgInst = (RelInstance)inst;
		RelationDescriptor descriptor = lgInst.getOutput();
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(lgInst.getOutput().getType().id);
		builder.addNode(node);
		if (this.latentDescriptor) {
			for (int pos = 0; pos < lgInst.size(); pos++) {
				for (int right = pos; right < lgInst.size(); right++) {
					long spanNode = this.toNode_Span(pos, right);
					builder.addNode(spanNode);
					builder.addEdge(spanNode, new long[]{leaf});
					builder.addEdge(node, new long[]{spanNode});
				}
			}
		} else {
			long spanNode = this.toNode_Span(descriptor.getLeft(), descriptor.getRight());
			builder.addNode(spanNode);
			builder.addEdge(spanNode, new long[]{leaf});
			builder.addEdge(node, new long[]{spanNode});
		}
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network))
				System.err.println("not contains");
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long root = toNode_root();
		builder.addNode(root);
		for (int l = 0; l < RelationType.RELS.size(); l++) {
			long node = this.toNode_Rel(l);
			builder.addNode(node);
			for (int pos = 0; pos < inst.size(); pos++) {
				for (int right = pos; right < inst.size(); right++) {
					long spanNode = this.toNode_Span(pos, right);
					builder.addNode(spanNode);
					builder.addEdge(spanNode, new long[]{leaf});
					builder.addEdge(node, new long[]{spanNode});
				}
			}
			builder.addEdge(root, new long[]{node});
		}
		return builder.build(networkId, inst, param, this);
	}
	
	@SuppressWarnings("unused")
	private void buildGenericNetwork() {
		NetworkBuilder<BaseNetwork> networkBuilder = NetworkBuilder.builder(BaseNetwork.class);
		long leaf = toNode_leaf();
		long[] leaves = new long[]{leaf};
		networkBuilder.addNode(leaf);
		long root = toNode_root();
		networkBuilder.addNode(root);
		for (int l = 0; l < RelationType.RELS.size(); l++) {
			long node = this.toNode_Rel(l);
			networkBuilder.addNode(node);
			networkBuilder.addEdge(node, leaves);
			networkBuilder.addEdge(root, new long[]{node});
		}
		BaseNetwork network = networkBuilder.buildRudimentaryNetwork();
	}
	
	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root();
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
		int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
		int labelId = arr[3];
		RelationType predType = RelationType.get(labelId);
		nodeIdx = baseNetwork.getMaxPath(labeledNodeIdx)[0];
		arr = baseNetwork.getNodeArray(nodeIdx);
		RelationDescriptor prediction = new RelationDescriptor(predType, arr[1], arr[2]);
		inst.setPrediction(prediction);
		return inst;
	}


}
