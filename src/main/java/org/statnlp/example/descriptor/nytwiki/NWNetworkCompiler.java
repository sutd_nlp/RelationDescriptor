package org.statnlp.example.descriptor.nytwiki;

import java.util.Arrays;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class NWNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -3523483926592577270L;

	public enum NodeType {LEAF, SPAN, NODE, ROOT};
	private boolean DEBUG = true;
	
	private boolean inBetween = false;
	private int leftRightBoundary = -1; //w.r.t the two arguments
	
	static {
		NetworkIDMapper.setCapacity(new int[]{4, 200, 200, RelationType.RELS.size()});
	}
	
	/**
	 * NWNetworkCompiler
	 * @param useLatentSpan: use latent span or not. if false, all other arguments are useless. 
	 * @param inBetween: latent scope should between the boundary.
	 * @param leftRightBoundary: allowance on up a few left or right with respect to the arguments
	 * @param fixRelation: true means using the gold relation.
	 */
	public NWNetworkCompiler(boolean inBetween, int leftRightBoundary) {
		//nodeType, leftIndex, rightIndex, relation type.
		this.inBetween = inBetween;
		this.leftRightBoundary = leftRightBoundary;
	}

	private long toNode_leaf() {
		return toNode(NodeType.LEAF, 0, 0, 0); 
	}
	
	private long toNode_Span(int leftIndex, int rightIndex){
		return toNode(NodeType.SPAN, leftIndex, rightIndex, 0);
	}
	
	private long toNode_Rel(int label) {
		return toNode(NodeType.NODE, 0, 0, label);
	}
	
	private long toNode_root() {
		return toNode(NodeType.ROOT, 0, 0, 0);
	}
	
	private long toNode(NodeType nodeType, int leftIndex, int rightIndex, int labelId) {
		return NetworkIDMapper.toHybridNodeID(new int[]{nodeType.ordinal(), leftIndex, rightIndex, labelId});
	}

	@Override
	public BaseNetwork compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance lgInst = (RelInstance)inst;
		RelationDescriptor descriptor = lgInst.getOutput();
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(lgInst.getOutput().getType().id);
		builder.addNode(node);
		if (descriptor.getLeft() < 0 && descriptor.getRight() < 0) {
			builder.addEdge(node, new long[]{leaf});
		} else {
			long spanNode = this.toNode_Span(descriptor.getLeft(), descriptor.getRight());
			builder.addNode(spanNode);
			builder.addEdge(spanNode, new long[]{leaf});
			builder.addEdge(node, new long[]{spanNode});
		}
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			Instance unInst = inst.duplicate();
			unInst.setInstanceId(-inst.getInstanceId());
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, unInst, param);
			if(!unlabeled.contains(network)) {
				System.out.println(lgInst.getInput().sent.toString());
				System.err.println("not contains");
				throw new RuntimeException("not contained");
			}
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long root = toNode_root();
		builder.addNode(root);
		RelInstance lgInst = (RelInstance)inst;
		CandidatePair input = lgInst.getInput();
		int leftIdx = input.spans.get(input.leftSpanIdx).start;
		int rightIdx = input.spans.get(input.rightSpanIdx).start;
		
//		int[] sdps = this.getShortestDepPath(input.sent, input.spans.get(input.leftSpanIdx), input.spans.get(input.rightSpanIdx));
//		int[] startEnds = this.getStartEndFromSDPs(leftIdx, rightIdx, sdps);
		long node = toNode_Rel(lgInst.getOutput().getType().id);
		builder.addNode(node);
		int enumDLStart = 0;
		int enumDLEnd = lgInst.size();
		int enumDREnd = lgInst.size();
		if (this.leftRightBoundary > 0) {
			enumDLStart = Math.max(enumDLStart, leftIdx - this.leftRightBoundary);
			enumDLEnd = Math.min(enumDLEnd, rightIdx + this.leftRightBoundary);
			enumDREnd = Math.min(enumDREnd, rightIdx + this.leftRightBoundary);
		}
		if (this.inBetween) {
			enumDLStart = leftIdx + 1; 
			enumDLEnd = rightIdx ;
			enumDREnd = rightIdx;
		}
		for (int dl = enumDLStart; dl < enumDLEnd; dl++) {
			for (int dr = dl; dr < enumDREnd; dr++) {
				//make sure the two arguments are not overlapped with the two arguments
				if (leftIdx >= dl && leftIdx <= dr) continue;
				if (rightIdx >= dl && rightIdx <= dr) continue;
				long spanNode = this.toNode_Span(dl, dr);
				builder.addNode(spanNode);
				builder.addEdge(spanNode, new long[]{leaf});
				builder.addEdge(node, new long[]{spanNode});
			}
		}
		if (this.inBetween && inst.getInstanceId() < 0) {
			RelationDescriptor des = lgInst.getOutput();
			if (des.getLeft() >= 0 && !this.inBetween(leftIdx, rightIdx, des.getLeft(), des.getRight())) {
				//add back the labeled network
				long spanNode = this.toNode_Span(des.getLeft(), des.getRight());
				builder.addNode(spanNode);
				builder.addEdge(spanNode, new long[]{leaf});
				builder.addEdge(node, new long[]{spanNode});
			}
		}
		builder.addEdge(node, new long[]{leaf});
		builder.addEdge(root, new long[]{node});
		return builder.build(networkId, inst, param, this);
	}
	
	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root();
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
		int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
		int labelId = arr[3];
		RelationType predType = RelationType.get(labelId);
		nodeIdx = baseNetwork.getMaxPath(labeledNodeIdx)[0];
		arr = baseNetwork.getNodeArray(nodeIdx);
		NodeType childNodeType = NodeType.values()[arr[0]];
		int left = -1;
		int right = -1;
		if (childNodeType != NodeType.LEAF) {
			left = arr[1];
			right = arr[2];
		}
		RelationDescriptor prediction = new RelationDescriptor(predType, left, right);
		inst.setPrediction(prediction);
		return inst;
	}
	
	@SuppressWarnings("unused")
	private int[] getStartEndFromSDPs(int leftBound, int rightBound, int[] sdps) {
		int[] startEnd = new int[2];
		startEnd[0] = -1;
		startEnd[1] = -1;
		for (int i = 0; i < sdps.length; i++) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[0] = i; break;}
		}
		for (int i = sdps.length - 1; i >= 0; i--) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[1] = i; break;}
		}
		return startEnd;
	}
	
	private boolean inBetween (int arg1Idx, int arg2Idx, int dl, int dr) {
		if (dl > arg1Idx  && dl < arg2Idx && dr > arg1Idx && dr < arg2Idx) return true;
		return false;
	}
	
	@SuppressWarnings("unused")
	private  int[] getShortestDepPath(Sentence sent, Span arg1Span, Span arg2Span) {
		Graph<Integer, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
		int[] des = new int[sent.length()];
		for (int i = 0; i < sent.length(); i++) {
			g.addVertex(i);
		}
		for (int i = 0; i < sent.length(); i++) {
			int head = sent.get(i).getHeadIndex();
			if (head != -1) {
				g.addEdge(i, head);
			}
		}
		DijkstraShortestPath<Integer, DefaultEdge> dg = new DijkstraShortestPath<>(g);
		GraphPath<Integer, DefaultEdge>  results = dg.getPath(arg1Span.headIdx, arg2Span.headIdx);
		List<Integer> list =  results.getVertexList();
		StringBuilder sb = new StringBuilder();
		for (int v : list) {
			if (v >= arg1Span.start && v <= arg1Span.end) continue;
			if (v >= arg2Span.start && v <= arg2Span.end) continue;
			if (v == arg1Span.headIdx || v == arg2Span.headIdx) continue;
			des[v] = 1;
			sb.append(sent.get(v).getForm() + " ");
		}
		return des;
	}


}
