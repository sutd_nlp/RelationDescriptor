package org.statnlp.example.descriptor.nytwiki.dep;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.descriptor.Span;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructure.Extras;
import edu.stanford.nlp.trees.LabeledScoredTreeNode;
import edu.stanford.nlp.trees.TypedDependency;

public class NWDataPreprocess {
	
	private static final String MODEL_LEXICAL = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
	private static final String SD_MODEL = "edu/stanford/nlp/models/parser/nndep/english_SD.gz";
	
	private LexicalizedParser parser;
	private DependencyParser dependencyParser;
	
	class NWObj {
		Sentence sent;
		List<CoreLabel> labeledSent;
		int[] heads = null;
		String[] depLabels = null;
		
		public NWObj (Sentence sent, List<CoreLabel> labeledSent) {
			this.sent = sent;
			this.labeledSent = labeledSent;
		}
		
		public void setHeadAndLabe(int[] hs, String[] labels) {this.heads = hs; this.depLabels = labels;}
	}
	
	
	/**
	 * Read the NYT and WikiData
	 * @param file
	 * @return
	 */
	private List<NWObj> read(String file) {
		List<NWObj> insts = new ArrayList<>();
		String line = null;
		int maxSentSize = 0;
		try {
			BufferedReader br = RAWF.reader(file);
			List<WordToken> wts = new ArrayList<WordToken>();
			List<Span> spans = new ArrayList<Span>();
			while((line = br.readLine()) != null) {
				if (line.startsWith("NYT-") || line.startsWith("Wikipedia-"))  continue; //sentence ID number
				if (line.equals("")) {
					Sentence sent = new Sentence(wts.toArray(new WordToken[wts.size()]));
					maxSentSize = Math.max(maxSentSize, sent.length());
					List<CoreLabel> labeledSent = new ArrayList<>(sent.length());
					for (int i = 0; i < sent.length(); i++) {
						CoreLabel word = new CoreLabel();
						word.setWord(sent.get(i).getForm());
						labeledSent.add(word);
					}
					insts.add(new NWObj(sent, labeledSent));
					wts.clear();
					spans.clear();
				} else {
					String[] vals = line.split("\\t");
					String word = vals[1];
					String posTag = vals[2];
					String phraseTag = vals[3];
					WordToken wt = new WordToken(word, posTag);
					wt.setPhraseTag(phraseTag);
					wt.setEntity(vals[4]);
					wts.add(wt);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("[Info] #Total Sentences: " + insts.size());
		return insts;
	}
	
	private void loadParser () {
		parser = LexicalizedParser.loadModel(MODEL_LEXICAL);
	} 
	
	private void loadDepParser() {
		System.out.println("Using Stanford Dependency..");
		dependencyParser = DependencyParser.loadFromModelFile(SD_MODEL);
	}
	
	private void printToFile(List<NWObj> objs, String output) {
		try {
			PrintWriter pw = RAWF.writer(output);
			for (NWObj obj : objs) {
				if (obj.sent.length() != obj.labeledSent.size()) {
					System.out.println(obj.sent.toString());
					System.out.println(obj.labeledSent.toString());
					throw new RuntimeException("lengths are not equal?");
				}
				StringBuilder tags = new StringBuilder();
				for (int p = 0; p < obj.labeledSent.size(); p++) {
					if (p == 0) {
						tags.append(obj.labeledSent.get(p).tag());
					} else {
						tags.append(" " + obj.labeledSent.get(p).tag());
					}
				}
				StringBuilder headSeq = new StringBuilder();
				for (int p = 0; p < obj.labeledSent.size(); p++) {
					if (p == 0) {
						headSeq.append(obj.heads[p] + "");
					} else {
						headSeq.append(" " + obj.heads[p]);
					}
				}
				StringBuilder depLabelSeq = new StringBuilder();
				for (int p = 0; p < obj.labeledSent.size(); p++) {
					if (p == 0) {
						depLabelSeq.append(obj.depLabels[p] + "");
					} else {
						depLabelSeq.append(" " + obj.depLabels[p]);
					}
				}
				StringBuilder ptSeq = new StringBuilder();
				for (int p = 0; p < obj.labeledSent.size(); p++) {
					if (p == 0) {
						ptSeq.append(obj.sent.get(p).getPhraseTag() + "");
					} else {
						ptSeq.append(" " + obj.sent.get(p).getPhraseTag());
					}
				}
				StringBuilder relLabelSeq = new StringBuilder();
				for (int p = 0; p < obj.labeledSent.size(); p++) {
					if (p == 0) {
						relLabelSeq.append(obj.sent.get(p).getEntity() + "");
					} else {
						relLabelSeq.append(" " + obj.sent.get(p).getEntity());
					}
				}
				pw.println(obj.sent.toString());
				pw.println(tags.toString());
				pw.println(headSeq.toString());
				pw.println(depLabelSeq.toString());
				pw.println(ptSeq.toString());
				pw.println(relLabelSeq.toString());
				pw.println();
			}
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void preprocess(String inputPath, String outputPath) {
		System.out.println("[Input] : " + inputPath);
		System.out.println("[Output] : " + outputPath);
		List<NWObj> objs = this.read(inputPath);
		for (NWObj obj : objs) {
			obj.labeledSent = ((LabeledScoredTreeNode)this.parser.parse(obj.labeledSent)).taggedLabeledYield();
			GrammaticalStructure structure = dependencyParser.predict(obj.labeledSent);
			List<TypedDependency> deps =  structure.typedDependencies(Extras.NONE);
			int[] heads = new int[obj.labeledSent.size()];
			String[] depLabels = new String[obj.labeledSent.size()];
			for (int i = 0; i < deps.size(); i++) {
				TypedDependency typedDep = deps.get(i);
				heads[typedDep.dep().index() - 1] = typedDep.gov().index() - 1;
				depLabels[typedDep.dep().index() - 1] = typedDep.reln().getShortName();
			}
			obj.setHeadAndLabe(heads, depLabels);
		}
		this.printToFile(objs, outputPath);
		System.out.println("[Finsih]....");
	}
	
	public void run(String inputPath, String outputPath) {
		this.loadParser();
		this.loadDepParser();
		this.preprocess(inputPath, outputPath);
	}
	
	public static void main(String[] args) {
		String inputFile = "data/ijcnlp/nyt.txt";
		String outputFile = "data/ijcnlp/nyt_dep.txt";
		NWDataPreprocess per = new NWDataPreprocess();
		per.run(inputFile, outputFile);
		inputFile = "data/ijcnlp/wikipedia.txt";
		outputFile = "data/ijcnlp/wikipedia_dep.txt";
		per.run(inputFile, outputFile);
	}
	
}
