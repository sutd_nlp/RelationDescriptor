package org.statnlp.example.descriptor.nytwiki.dep;

import java.util.Arrays;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class DepCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -8919316854047754910L;
public enum NodeType {leaf, des, rel, root};//relaiton is node, always fix
	
	private boolean DEBUG = true;
	
	private int latentNeighbor = 0;
	private boolean useDependency = false;
	//private boolean inBetween = false; //for now, always inbetween
	//private int leftRightBoundary = -1; //w.r.t the two arguments
	
	static {
		NetworkIDMapper.setCapacity(new int[]{4, 200, 200, RelationType.RELS.size()});
	}
	
	public DepCompiler(int latentNeighbor, boolean useDep) {
		this.latentNeighbor = latentNeighbor;
		this.useDependency = useDep;
	}

	private long toNode_leaf() {
		return toNode(NodeType.leaf, 0, 0, 0); 
	}
	
	private long toNode_Span(int leftIndex, int rightIndex){
		return toNode(NodeType.des, leftIndex, rightIndex, 0);
	}
	
	private long toNode_Rel(int label) {
		return toNode(NodeType.rel, 0, 0, label);
	}
	
	private long toNode_root() {
		return toNode(NodeType.root, 0, 0, 0);
	}
	
	private long toNode(NodeType nodeType, int leftIndex, int rightIndex, int labelId) {
		return NetworkIDMapper.toHybridNodeID(new int[]{nodeType.ordinal(), leftIndex, rightIndex, labelId});
	}
	
	@Override
	public Network compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance rInst = (RelInstance)inst;
		CandidatePair input = rInst.getInput();
		Sentence sent = input.sent;
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		RelationDescriptor descriptor = rInst.getOutput();
		Span leftSpan = input.spans.get(leftIdx);
		Span rightSpan = input.spans.get(rightIdx); 
		int leftSpanIdx = leftSpan.start;
		int rightSpanIdx = rightSpan.start;
		int[] sdps = this.getShortestDepPath(sent, leftSpan, rightSpan);
		int[] startEnds = this.getStartEndFromSDPs(leftSpan.end, rightSpan.start, sdps);
		int rdpStart = startEnds[0];
		int rdpEnd = startEnds[1];
		if (!useDependency) {
			rdpStart = descriptor.getLeft();
			rdpEnd = descriptor.getRight();
		}
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(descriptor.getType().id);
		builder.addNode(node);
		if (rdpStart == -1) {
			builder.addEdge(node, new long[]{leaf});
		} else {
			if (useDependency) {
				for (int sdiff = 0; sdiff <= this.latentNeighbor; sdiff++) {
					for (int ediff = 0; ediff <= this.latentNeighbor; ediff++) {
						if (rdpStart - sdiff <= rdpEnd + ediff &&
								rdpStart - sdiff > leftSpan.end && 
								rdpEnd + ediff < rightSpan.start) {
							long span = this.toNode_Span(rdpStart - sdiff, rdpEnd + ediff);
							builder.addNode(span);
							builder.addEdge(node, new long[]{span});
							builder.addEdge(span, new long[]{leaf});
						}
					}
				}
			} else {
				for (int sdiff = 0; sdiff <= this.latentNeighbor; sdiff++) {
					for (int ediff = 0; ediff <= this.latentNeighbor; ediff++) {
						int newLeft = rdpStart - sdiff;
						int newRight = rdpEnd + ediff;
						if (newLeft < 0 || newRight < 0) continue;
						if (newLeft > newRight) continue; 
						if (leftSpanIdx >= newLeft && leftSpanIdx <= newRight) continue;
						if (rightSpanIdx >= newLeft && rightSpanIdx <= newRight) continue;
						long span = this.toNode_Span(newLeft, newRight);
						builder.addNode(span);
						builder.addEdge(node, new long[]{span});
						builder.addEdge(span, new long[]{leaf});
						
					}
				}
			}
			
		}
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network)) {
				System.err.println(sent.toString());
				System.err.println("not contains");
				throw new RuntimeException("network is not contained");
			}
				
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance rInst = (RelInstance)inst;
		CandidatePair input = rInst.getInput();
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		RelationDescriptor descriptor = rInst.getOutput();
		Span leftSpan = input.spans.get(leftIdx);
		Span rightSpan = input.spans.get(rightIdx);
		int leftSpanIdx = leftSpan.start;
		int rightSpanIdx = rightSpan.start;
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(descriptor.getType().id);
		builder.addNode(node);
		if (this.useDependency) {
			for (int dl = leftSpan.end + 1; dl < rightSpan.start; dl++) {
				for (int dr = dl; dr < rightSpan.start; dr++) {
					long span = this.toNode_Span(dl, dr);
					builder.addNode(span);
					builder.addEdge(node, new long[]{span});
					builder.addEdge(span, new long[]{leaf});
				}
			}
		} else {
			int dlStart = 0; 
			int dlEnd = inst.size();
			for (int dl = dlStart; dl < dlEnd; dl++) {
				for (int dr = dl; dr < dlEnd; dr++) {
					if (leftSpanIdx >= dl && leftSpanIdx <= dr) continue;
					if (rightSpanIdx >= dl && rightSpanIdx <= dr) continue;
					long span = this.toNode_Span(dl, dr);
					builder.addNode(span);
					builder.addEdge(node, new long[]{span});
					builder.addEdge(span, new long[]{leaf});
				}
			}
		}
		
		builder.addEdge(node, new long[]{leaf});
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		return network;
	}

	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root();
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
		int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
		int labelId = arr[3];
		RelationType predType = RelationType.get(labelId);
		nodeIdx = baseNetwork.getMaxPath(labeledNodeIdx)[0];
		arr = baseNetwork.getNodeArray(nodeIdx);
		int left = arr[1];
		int right = arr[2];
		if (arr[0] == NodeType.leaf.ordinal()) {
			left = -1;
			right = -1;
		}
		RelationDescriptor prediction = new RelationDescriptor(predType, left, right);
		inst.setPrediction(prediction);
		return inst;
	}
	
	private int[] getStartEndFromSDPs(int leftBound, int rightBound, int[] sdps) {
		int[] startEnd = new int[2];
		startEnd[0] = -1;
		startEnd[1] = -1;
		for (int i = 0; i < sdps.length; i++) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[0] = i; break;}
		}
		for (int i = sdps.length - 1; i >= 0; i--) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[1] = i; break;}
		}
		return startEnd;
	}
	
	private  int[] getShortestDepPath(Sentence sent, Span arg1Span, Span arg2Span) {
		Graph<Integer, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
		int[] des = new int[sent.length()];
		for (int i = 0; i < sent.length(); i++) {
			g.addVertex(i);
		}
		for (int i = 0; i < sent.length(); i++) {
			int head = sent.get(i).getHeadIndex();
			if (head != -1) {
				g.addEdge(i, head);
			}
		}
		DijkstraShortestPath<Integer, DefaultEdge> dg = new DijkstraShortestPath<>(g);
		GraphPath<Integer, DefaultEdge>  results = dg.getPath(arg1Span.headIdx, arg2Span.headIdx);
		List<Integer> list =  results.getVertexList();
		StringBuilder sb = new StringBuilder();
		for (int v : list) {
			if (v >= arg1Span.start && v <= arg1Span.end) continue;
			if (v >= arg2Span.start && v <= arg2Span.end) continue;
			if (v == arg1Span.headIdx || v == arg2Span.headIdx) continue;
			des[v] = 1;
			sb.append(sent.get(v).getForm() + " ");
		}
		return des;
	}

}
