package org.statnlp.example.descriptor.nytwiki;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Random;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelMetric;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationDescriptorEvaluator;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.data.DataProcessor;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class NWMain {
	
	public static double l2val = 0.01;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static String dataFile = "data/ijcnlp/nyt_dep.txt";
	public static int allNum = -1;
	public static String nytResultFile = "results/nyt_res.txt";
	public static String wikiResultFile = "results/wiki_res.txt";
	public static boolean saveModel = false;
	public static boolean readModel = false;
	public static String modelFile = "models/nyt_model.m";
	
	public static boolean contextualFeature = false;
	public static boolean pathFeature = false;
	public static boolean boundaryFeature = false;
	public static long randomSeed = 1234;
	public static String relType = "General";
	
	public static boolean inBetween = false;
	public static int leftRightBoundary = 0;
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		setArgs(args);
		
		/***
		 * Parameter settings and model configuration
		 */
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		DataReader reader = new DataReader();
		
		/**debug info**/
//		saveModel = false;
//		readModel = false;
//		data = "wikipedia";
//		debugMain(reader);
//		System.exit(0);
		/***/
		
		Random rand = new Random(randomSeed);
		DataProcessor preprocessor = new DataProcessor();
		int numFolds = 10;
		
		Instance[] insts = reader.readPreprocessed(dataFile, true, allNum, relType);
		insts = preprocessor.shuffle(insts, rand);
		Instance[][] splits = preprocessor.splitIntoKFolds(insts, numFolds);
		
		System.out.println("#Relations: " + RelationType.RELS.size());
		System.out.println("Relations: " + RelationType.RELS.toString());
		RelationType.lock();
		int numAllInsts = insts.length;
		System.out.println("#all combined insts: " + numAllInsts);
		
		double[][] sumMetric = new double[2][4];
		double[][] sumRelMetric = new double[2][3];
		for (int k = 0; k < numFolds; k++) {
			Instance[] testData = splits[numFolds - k - 1];
			for(Instance inst : testData) {
				inst.setUnlabeled();
			}
			Instance[] trainData = new Instance[numAllInsts - testData.length];
			int idx = 0;
			for (int t = 0; t < numFolds; t++) {
				if (t == numFolds - k - 1) continue;
				for (int i = 0; i < splits[t].length; i++) {
					splits[t][i].setLabeled();
					splits[t][i].setPrediction(null);
					trainData[idx++] = splits[t][i];
				}
			}
			NetworkModel model = null;
			if (readModel) {
				System.out.println("[Info] Reading Model....");
				ObjectInputStream in = RAWF.objectReader(modelFile + "."+k);
				model = (NetworkModel)in.readObject();
			} else {
				GlobalNetworkParam gnp = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
				NWFeatureManager tfm = new NWFeatureManager(gnp, contextualFeature, pathFeature, boundaryFeature);
				NWNetworkCompiler tnc = new NWNetworkCompiler(inBetween, leftRightBoundary);
				model = DiscriminativeNetworkModel.create(tfm, tnc);
				model.train(trainData, numIteration);
			}
			if (saveModel) {
				ObjectOutputStream out = RAWF.objectWriter(modelFile + "."+k);
				out.writeObject(model);
				out.close();
			}
			
			Instance[] results = null;
			RelationDescriptorEvaluator evaluator = new RelationDescriptorEvaluator();
			RelMetric relMet = null;
			double[][] relMetrics = null;
			double[][] metrics;
			/**
			 * Testing Phase Test the full dataset.
			 */
			
			/***Test the NYT Dataset***/
			results = model.test(testData);
			System.out.println("[NYT Results]");
			relMet = evaluator.evaluateRelation(results);
			relMetrics = relMet.getAll();
			for (int r = 0; r < sumRelMetric.length; r++) {
				for (int c = 0; c < sumRelMetric[r].length; c++) {
					sumRelMetric[r][c] += relMetrics[r][c];
				}
			}
			metrics = evaluator.evaluateOverlapAndExactMatchDescriptor(results);
			for (int r = 0; r < metrics.length; r++) {
				for (int c = 0; c < metrics[r].length; c++)
					sumMetric[r][c] += metrics[r][c];
			}
			
			PrintWriter pw = RAWF.writer(nytResultFile+"."+k);
			for (Instance res : results) {
				RelInstance inst = (RelInstance)res;
				Sentence sent = inst.getInput().sent;
				RelationDescriptor gold = inst.getOutput();
				RelationDescriptor pred = inst.getPrediction();
				for (int i = 0; i < inst.size(); i++) {
					String goldLabel = i >= gold.getLeft() && i <= gold.getRight() ? 
							i == gold.getLeft() ? "B-R" : "I-R" : "O";
					String predLabel = i >= pred.getLeft() && i <= pred.getRight() ? 
							i == pred.getLeft() ? "B-R" : "I-R" : "O";
					pw.println(sent.get(i).getForm() + "\t" + goldLabel + "\t" + predLabel);
				}
				pw.println();
			}
			pw.close();
		}
		/****Total for NYT****/
		for (int r = 0; r < sumRelMetric.length; r++) {
			for (int c = 0; c < sumRelMetric[r].length; c++)
				sumRelMetric[r][c] /= 10;
		}
		for (int r = 0; r < sumMetric.length; r++) {
			for (int c = 0; c < sumMetric[r].length; c++)
				sumMetric[r][c] /= 10;
		}
		System.out.printf("[NYT Result micro] Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", sumRelMetric[0][0], sumRelMetric[0][1], sumRelMetric[0][2]);
		System.out.printf("[NYT Result macro] Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", sumRelMetric[1][0], sumRelMetric[1][1], sumRelMetric[1][2]);
		System.out.printf("[NYT Result Overlap Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", sumMetric[0][0], sumMetric[0][1], sumMetric[0][2], sumMetric[0][3]);
		System.out.printf("[NYT Result Exact Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", sumMetric[1][0], sumMetric[1][1], sumMetric[1][2], sumMetric[1][3]);
		
	}
	
	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Logistic Regression Model for Relation Extraction");
		parser.addArgument("-t", "--thread").setDefault(8).help("number of threads");
		parser.addArgument("--l2").setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--iter").setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--data").setDefault(dataFile).help("The path of the data file");
		parser.addArgument("--allNum").setDefault(allNum).help("The number of all instances");
		parser.addArgument("--saveModel").setDefault(saveModel).help("whether to save the model");
		parser.addArgument("--readModel").setDefault(readModel).help("whether to read the model");
		parser.addArgument("--modelFile").setDefault(modelFile).help("specify the model file");
		parser.addArgument("--nytResFile").setDefault(nytResultFile).help("specify the nyt result file");
		parser.addArgument("--wikiResFile").setDefault(wikiResultFile).help("specify the wiki result file");
		parser.addArgument("-cf", "--contextual").setDefault(contextualFeature).help("whether to use contextual features");
		parser.addArgument("-pf", "--path").setDefault(pathFeature).help("whether to use path features");
		parser.addArgument("-bf", "--boundary").setDefault(boundaryFeature).help("whether to use boundary features");
		parser.addArgument("--randomSeed").setDefault(randomSeed).help("the random seed");
		parser.addArgument("-ib", "--inbetween").type(Boolean.class).setDefault(inBetween).help("whether the descripto is in between");
		parser.addArgument("-lrb","--leftrightbound").type(Integer.class).setDefault(leftRightBoundary).help("left right boundary");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = Integer.valueOf(ns.getString("thread"));
        l2val = Double.valueOf(ns.getString("l2"));
        numIteration = Integer.valueOf(ns.getString("iter"));
        dataFile = ns.getString("data");
        allNum = Integer.valueOf(ns.getString("allNum"));
        saveModel = Boolean.valueOf(ns.getString("saveModel"));
        readModel = Boolean.valueOf(ns.getString("readModel")) ;
        modelFile = ns.getString("modelFile");
        nytResultFile = ns.getString("nytResFile");
        wikiResultFile = ns.getString("wikiResFile");
        contextualFeature = Boolean.valueOf(ns.getString("contextual")) ;
        pathFeature = Boolean.valueOf(ns.getString("path")) ;
        boundaryFeature = Boolean.valueOf(ns.getString("boundary")) ;
        randomSeed = Long.valueOf(ns.getString("randomSeed"));
        inBetween = ns.getBoolean("inbetween");
        leftRightBoundary = ns.getInt("leftrightbound");
        System.err.println(ns.getAttrs().toString());
	}
}
