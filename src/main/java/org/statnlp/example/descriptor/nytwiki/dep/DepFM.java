package org.statnlp.example.descriptor.nytwiki.dep;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;

public class DepFM extends FeatureManager {

	private static final long serialVersionUID = -5975586214902327900L;
	
	private enum FeaType {
		word,
		tag,
		phrase,
		bigram_word,
		bigram_tag,
		bigram_phrase,
		fo_word,
		fo_tag,
		fo_phrase,
		fo_bigram_word,
		fo_bigram_tag,
		fo_bigram_phrase,
		transition,
		contextual,
		path,
		phrase_boundary,spanlen, hasdes
		}
	
	public DepFM(GlobalNetworkParam param_g) {
		super(param_g);
		// TODO Auto-generated constructor stub
	}


	@Override
	protected FeatureArray extract_helper(Network network, int parent_k, int[] children_k, int children_k_index) {
		int[] paArr = network.getNodeArray(parent_k);
		int nodeType = paArr[0];
		if (nodeType != DepCompiler.NodeType.rel.ordinal())
			return FeatureArray.EMPTY;
		RelInstance inst = (RelInstance)network.getInstance();
		CandidatePair input = inst.getInput();
//		List<Span> spans = input.spans;
		Sentence sent = input.sent;
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		int relId = paArr[3];
//		String rel = relId + "";
		String relForm = RelationType.get(relId).form;
		int arg1Idx = leftIdx;
		int arg2Idx = rightIdx;
		Span arg1Span = input.spans.get(arg1Idx);
		Span arg2Span = input.spans.get(arg2Idx);
		FeatureArray startFa = this.createFeatureArray(network, new int[0]);
		FeatureArray currFa = startFa;
		/***Create the feature array for the relation descriptor span***/
		String relNoRev = relForm;//.replaceAll(Config.REV_SUFF, "");
		this.addDescriptorFeats(currFa, network, parent_k, children_k, children_k_index, sent, relNoRev, arg1Span.start, arg1Span.end, arg2Span.start, arg2Span.end);
		return startFa;
	}
	
	private FeatureArray addDescriptorFeats (FeatureArray currFa, Network network, int parent_k, int[] children_k, int children_k_index,
			Sentence sent, String relNoRev, int arg1Left, int arg1Right, int arg2Left, int arg2Right) {
		List<Integer> fs = new ArrayList<>();
		int[] child = network.getNodeArray(children_k[0]);
		DepCompiler.NodeType childNodeType = DepCompiler.NodeType.values()[child[0]];
		int dl = child[1];
		int dr = child[2];
		if (childNodeType == DepCompiler.NodeType.leaf) {
			dl = -100;
			dr = -100;
		}
		for (int i = 0; i < sent.length(); i++) {
			//use zero order first;
			String currLabel = i == dl ? "B-" + relNoRev : (i > dl && i <= dr) ? 
				"I-" + relNoRev : "O";
			int prevIdx = i - 1;
			String prevLabel = prevIdx == dl ? "B-" + relNoRev : (prevIdx > dl && prevIdx <= dr) ?
					"I-" + relNoRev : "O";
			if (prevIdx < 0) prevLabel = "O";
			String output = currLabel;
			for (int j = i - 2; j <= i + 2; j++) {
				int add = j - sent.length();
				int relPos = j - i;
				String word = j < 0 ? "START" + j : j >= sent.length() ? "END" + add :  sent.get(j).getForm();
				String tag  = j < 0 ? "START_TAG" + j : j >= sent.length() ? "END_TAG" + add :  sent.get(j).getTag();
				String phrase  = j < 0 ? "START_Phrase" + j : j >= sent.length() ? "END_Phrase" + add : sent.get(j).getPhraseTag();
				
				fs.add(this._param_g.toFeature(network, FeaType.word.name() + relPos, output, word));
				fs.add(this._param_g.toFeature(network, FeaType.tag.name() + relPos, output, tag));
				fs.add(this._param_g.toFeature(network, FeaType.phrase.name() + relPos, output, phrase));
				
				fs.add(this._param_g.toFeature(network, FeaType.fo_word.name() + relPos, output, word + " " + prevLabel));
				fs.add(this._param_g.toFeature(network, FeaType.fo_tag.name() + relPos, output, tag + " " + prevLabel));
				fs.add(this._param_g.toFeature(network, FeaType.fo_phrase.name() + relPos, output, phrase + " " + prevLabel));
			}
			for (int j = i - 1; j <= i + 2; j++) {
				int k = j - 1;
				int relPos = j - i;
				int addJ = j - sent.length();
				int addK = k - sent.length();
				String prevWord = k < 0 ? "START" + k : k >= sent.length() ? "END" + addK :  sent.get(k).getForm();
				String prevTag = k < 0 ? "START_TAG" + k : k >= sent.length() ? "END_TAG" + addK :  sent.get(k).getTag();
				String prevPhrase = k < 0 ? "START_Phrase" + k : k >= sent.length() ? "END_Phrase" + addK : sent.get(k).getPhraseTag();
				String word = j < 0 ? "START" + j : j >= sent.length() ? "END" + addJ:  sent.get(j).getForm();
				String tag  = j < 0 ? "START_TAG" + j : j >= sent.length() ? "END_TAG" + addJ:  sent.get(j).getTag();
				String phrase  = j < 0 ? "START_Phrase" + j : j >= sent.length() ? "END_Phrase" + addJ : sent.get(j).getPhraseTag();
				
				fs.add(this._param_g.toFeature(network, FeaType.bigram_word.name() + relPos, output, prevWord + " " + word));
				fs.add(this._param_g.toFeature(network, FeaType.bigram_tag.name() + relPos, output, prevTag + " " + tag));
				fs.add(this._param_g.toFeature(network, FeaType.bigram_phrase.name() + relPos, output, prevPhrase + " " +phrase));
				fs.add(this._param_g.toFeature(network, FeaType.fo_bigram_word.name() + relPos, output, prevWord + " " + word + " " + prevLabel));
				fs.add(this._param_g.toFeature(network, FeaType.fo_bigram_tag.name() + relPos, output, prevTag + " " + tag + " " + prevLabel));
				fs.add(this._param_g.toFeature(network, FeaType.fo_bigram_phrase.name() + relPos, output, prevPhrase + " " +phrase + " " + prevLabel));
			}
			fs.add(this._param_g.toFeature(network, FeaType.transition.name(), currLabel, prevLabel));
		}
		if (dl > 0) {
			int lb = dl - 1;
			int rb = dr + 1;
			String lbw = lb < 0 ? "START" + lb : sent.get(lb).getForm();
			String lbt = lb < 0 ? "START_TAG" + lb : sent.get(lb).getTag();
			int addK = rb - sent.length();
			String rbw = rb >= sent.length() ? "END" + addK : sent.get(rb).getForm();
			String rbt = rb >= sent.length() ? "END_TAG" + addK : sent.get(rb).getTag();
			fs.add(this._param_g.toFeature(network, FeaType.contextual.name() + "-lbw", relNoRev, lbw));
			fs.add(this._param_g.toFeature(network, FeaType.contextual.name() + "-lbt", relNoRev, lbt));
			fs.add(this._param_g.toFeature(network, FeaType.contextual.name() + "-rbw", relNoRev, rbw));
			fs.add(this._param_g.toFeature(network, FeaType.contextual.name() + "-rbt", relNoRev, rbt));
			StringBuilder arg1Path = new StringBuilder("");
			StringBuilder arg1TagPath = new StringBuilder("");
			boolean arg1OnLeft = isOnLeft(arg1Left, dl, dr);
			int pathStart = arg1OnLeft ? arg1Left + 1 : dr + 1;
			int pathEnd = arg1OnLeft ? dl : arg1Left;
			for (int i = pathStart; i < pathEnd; i++) {
				if (i == pathStart) {
					arg1Path.append(sent.get(i).getForm());
					arg1TagPath.append(sent.get(i).getTag());
				}
				else  {
					arg1Path.append(" " + sent.get(i).getForm());
					arg1TagPath.append(" " + sent.get(i).getTag());
				}
			}
			fs.add(this._param_g.toFeature(network,  FeaType.path.name() + "-a1w", relNoRev, arg1Path.toString()));
			fs.add(this._param_g.toFeature(network,  FeaType.path.name() + "-a1t", relNoRev, arg1TagPath.toString()));
			
			StringBuilder arg2Path = new StringBuilder("");
			StringBuilder arg2TagPath = new StringBuilder("");
			boolean arg2OnLeft = isOnLeft(arg2Left, dl, dr);
			pathStart = arg2OnLeft ? arg2Left + 1 : dr + 1;
			pathEnd = arg2OnLeft ? dl : arg2Left;
			for (int i = pathStart; i < pathEnd; i++) {
				if (i == pathStart) {
					arg2Path.append(sent.get(i).getForm());
					arg2TagPath.append(sent.get(i).getTag());
				}
				else  {
					arg2Path.append(" " + sent.get(i).getForm());
					arg2TagPath.append(" " + sent.get(i).getTag());
				}
			}
			fs.add(this._param_g.toFeature(network,  FeaType.path.name() + "-a2w", relNoRev, arg2Path.toString()));
			fs.add(this._param_g.toFeature(network,  FeaType.path.name() + "-a2t", relNoRev, arg2TagPath.toString()));
			int minStart = Math.min(arg1Left, arg2Left);
			minStart = Math.min(minStart, dl);
			int maxEnd = Math.max(arg1Left, arg2Left);
			maxEnd = Math.max(maxEnd, dr);
			StringBuilder comb = new StringBuilder("");
			StringBuilder combTag = new StringBuilder("");
			for (int i = minStart; i <= maxEnd; i++) {
				if (i == dl) {
					comb.append(relNoRev + " ");
					continue;
				} else if (i > dl && i <= dr) {continue;}
				comb.append(sent.get(i).getForm() + " ");
				combTag.append(sent.get(i).getTag()  + " ")  ;
			}
			fs.add(this._param_g.toFeature(network, FeaType.path.name() + "-w", relNoRev, comb.toString()));
			fs.add(this._param_g.toFeature(network, FeaType.path.name() + "-t", relNoRev, combTag.toString()));
			boolean violateBoundary = this.violateBoundary(sent, dl, dr);
			fs.add(this._param_g.toFeature(network, FeaType.phrase_boundary.name(), relNoRev, String.valueOf(violateBoundary)));
		}
		List<Integer> spanFs = new ArrayList<>();
		List<Double> vals = new ArrayList<>();
		double spanLen = dl < 0? 0 : dr - dl + 1;
		spanFs.add(this._param_g.toFeature(network, FeaType.spanlen.name(), "",  ""));
		vals.add(spanLen + 1);
		if (dl >= 0) {
			fs.add(this._param_g.toFeature(network, FeaType.hasdes.name(), "",  ""));
		}
		currFa = currFa.addNext(this.createFeatureArray(network, fs));
		currFa = currFa.addNext(this.createFeatureArray(network, spanFs, vals));
		return currFa;
	}

	private boolean isOnLeft(int idx, int relLeft, int relRight) {
		if (idx < relLeft) {
			return true; 
		} else if ( idx > relRight) {
			return false;
		} else {
			System.out.println(idx + ", " + relLeft + ", " + relRight);
			throw new RuntimeException("index is in between the two arguments?");
		}
	}
	
	private boolean violateBoundary (Sentence sent, int relLeft, int relRight) {
		if (!sent.get(relLeft).getPhraseTag().startsWith("B-")) return true;
		String rpt = sent.get(relRight).getPhraseTag();
		if (relRight == sent.length() - 1) return rpt.equals("O");
		String next_rpt = sent.get(relRight + 1).getPhraseTag();
		if (rpt.startsWith("B-") || rpt.startsWith("I-")) {
			return next_rpt.startsWith("I-");
		} else { //rpt = "O"
			return true;
		}
	}

}
