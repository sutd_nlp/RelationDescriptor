package org.statnlp.example.descriptor.nytwiki;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.DependencyTree;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.Config;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;

public class DataReader {
	
	public DataReader() {
		
	}
	
	/**
	 * TODO: check whether arg1 is always on the left.
	 * @param file
	 * @param isTrainingInsts
	 * @param number
	 * @return
	 */
	@Deprecated
	public RelInstance[] read(String file, boolean isTrainingInsts, int number, String preRelLabel, String arg1Ent, String arg2Ent) {
		List<RelInstance> insts = new ArrayList<>();
		String line = null;
		int maxDescriptorLen = 0;
		int maxSentSize = 0;
		int numLeft = 0;
		int numRight = 0;
		int numBetween = 0;
		try {
			BufferedReader br = RAWF.reader(file);
			List<WordToken> wts = new ArrayList<WordToken>();
			List<Span> spans = new ArrayList<Span>();
			int arg1Idx = -1;
			int arg2Idx = -1;
			int instId = 1;
			int rdLeft = -1;
			int rdRight = -1;
			String relLabel = null;
			String prevRelLabel = "O";
			while((line = br.readLine()) != null) {
				if (line.startsWith("NYT-") || line.startsWith("Wikipedia-"))  continue; //sentence ID number
				if (line.equals("")) {
					Sentence sent = new Sentence(wts.toArray(new WordToken[wts.size()]));
					int leftSpanIdx = arg1Idx < arg2Idx ? arg1Idx : arg2Idx;
					int rightSpanIdx = arg1Idx < arg2Idx ? arg2Idx : arg1Idx;
					sent.get(leftSpanIdx).setEntity(arg1Ent);
					sent.get(rightSpanIdx).setEntity(arg2Ent);
					CandidatePair cp = new CandidatePair(sent, spans, leftSpanIdx, rightSpanIdx);
					maxSentSize = Math.max(maxSentSize, sent.length());
					RelationType relType = null;
					if (rdLeft < 0) {
						if (arg1Idx > arg2Idx) {
							//remove the argument positioning bias
							relType = RelationType.get(preRelLabel + Config.REV_SUFF);
							sent.get(arg1Idx).setForm("arg2");
							sent.get(arg2Idx).setForm("arg1");
						}else relType = RelationType.get(preRelLabel);
					} else {
						if (arg1Idx > arg2Idx) {
							//remove the argument positioning bias
							relType = RelationType.get(preRelLabel + Config.REV_SUFF);
							sent.get(arg1Idx).setForm("arg2");
							sent.get(arg2Idx).setForm("arg1");
						} else relType = RelationType.get(preRelLabel);
					}
					if (rdLeft >= 0) {
						int leftMost = Math.min(arg1Idx, arg2Idx);
						int rightMost = Math.max(arg1Idx, arg2Idx);
						if (rdRight < leftMost) {
							numLeft++;
						} else if (rdLeft > rightMost) {
							numRight++;
						} else {
							numBetween++;
						}
					}
//					if (rdLeft < 0 || rdRight < 0) {
//						System.out.println(sent.toString());
//						System.out.println("Descriptor span: " + rdLeft + ", " + rdRight);
//						throw new RuntimeException("descriptor length have negative boundary?");
//					}
					RelationDescriptor output = new RelationDescriptor(relType, rdLeft, rdRight);
					maxDescriptorLen = Math.max(rdRight - rdLeft + 1, maxDescriptorLen);
					RelInstance inst = new RelInstance(instId, 1.0, cp, output);
					insts.add(inst);
					if (insts.size() == number) {
						break;
					}
					wts.clear();
					spans.clear();
					arg1Idx = -1;
					arg2Idx = -1;
					rdLeft = -1;
					rdRight = -1;
					prevRelLabel = "O";
					instId++;
				} else {
					String[] vals = line.split("\\t");
					int idx = Integer.valueOf(vals[0]);
					String word = vals[1];
					String posTag = vals[2];
					String phraseTag = vals[3];
					relLabel = vals[4];
					WordToken wt = new WordToken(word, posTag);
					wt.setPhraseTag(phraseTag);
					if (word.equals("arg1")) {
						arg1Idx = idx;
					} else if (word.equals("arg2")) {
						arg2Idx = idx;
					}
					if (relLabel.equals("B-R")) {
						rdLeft = idx;
					} 
					if (relLabel.equals("O") && !prevRelLabel.equals("O")) {
						rdRight = idx - 1; //means previous is endding.
					}
					wts.add(wt);
					prevRelLabel = relLabel;
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("[Info] #Total Instances: " + insts.size());
		System.out.println("[Info] Maximum sentence length: " + maxSentSize);
		System.out.println("[Info] Maximum descriptor length: " + maxDescriptorLen);
		System.out.println("[Info] num des. left: " + numLeft);
		System.out.println("[Info] num des. right: " + numRight);
		System.out.println("[Info] num des. bete: " + numBetween);
		return insts.toArray(new RelInstance[insts.size()]);
	}
	
	public RelInstance[] readPreprocessed(String file, boolean isTrainingInsts, int number, String preRelLabel) {
		List<RelInstance> insts = new ArrayList<RelInstance>();
		BufferedReader br;
		Set<String> sentSet = new HashSet<>();
		try {
			br = RAWF.reader(file);
			String line = null;
			int instId = 1;
			int maxSentSize = -1;
			while ((line = br.readLine()) != null) {
				String[] vals = line.split(" ");
				String tagSeq = br.readLine();
				String[] tagVals = tagSeq.split(" ");
				String headIdxs = br.readLine();
				String[] headIdxArr = headIdxs.split(" ");
				String headLabelSeq = br.readLine();
				String[] headLabels = headLabelSeq.split(" ");
				String phraseTagSeq = br.readLine();
				String[] phraseLabels = phraseTagSeq.split(" ");
				String labelSeq = br.readLine();
				String[] labels = labelSeq.split(" ");
				String relation = null;
				WordToken[] wts = new WordToken[vals.length];
				int arg1Idx = -1;
				int arg2Idx = -1;
				for (int p = 0; p < wts.length; p++) {
					if (vals[p].equals("arg1")) {
						arg1Idx = p;
					}
					if (vals[p].equals("arg2")) {
						arg2Idx = p;
					}
					wts[p] = new WordToken(vals[p], tagVals[p], Integer.valueOf(headIdxArr[p]), null, headLabels[p]);
					wts[p].setPhraseTag(phraseLabels[p]);
				}
				if (arg1Idx > arg2Idx) {
					relation = preRelLabel + Config.REV_SUFF;
					wts[arg1Idx].setForm("arg2");
					wts[arg2Idx].setForm("arg1");
					int tmp = arg1Idx;
					arg1Idx = arg2Idx;
					arg2Idx = tmp;
				} else {
					relation = preRelLabel;
				}
				Sentence sent = new Sentence(wts);
				sent.depTree = new DependencyTree(sent);
				//arg1 and arg2 and swapped already
				Span leftSpan = new Span(arg1Idx, arg1Idx, "entity");
				leftSpan.headIdx = arg1Idx;
				Span rightSpan = new Span(arg2Idx, arg2Idx, "entity");
				rightSpan.headIdx = arg2Idx;
				List<Span> spans = new ArrayList<>(2);
				spans.add(leftSpan);
				spans.add(rightSpan);
				CandidatePair cp = new CandidatePair(sent, spans, 0, 1);
				sentSet.add(sent.toString());
				int left = -1;
				int right = -1;
				for (int i = 0; i < labels.length; i++) {
					if (labels[i].startsWith(Config.B_PREF)) {left = i; break;}
				}
				for (int i = labels.length - 1; i >= 0; i--) {
					if (labels[i].startsWith(Config.I_PREF) || labels[i].startsWith(Config.B_PREF)) {right = i; break;}
				}
				RelationDescriptor descriptor = new RelationDescriptor(RelationType.get(relation), left, right);
				RelInstance inst = new RelInstance(instId, 1.0, cp, descriptor);
				maxSentSize = Math.max(maxSentSize, inst.size());
				if (isTrainingInsts) {
					inst.setLabeled();
				} else {
					inst.setUnlabeled();
				}
				insts.add(inst);
				if (insts.size() == number) {
					break;
				}
				instId++;
				br.readLine(); //empty line;
			}
			br.close();
			System.out.println("[Info] number of unique sentences: " + sentSet.size());
			sentSet.clear();
			sentSet = null;
			System.out.println("[Info] maximum size of sentence: " + maxSentSize);
			System.out.println("[Info] number of instances read: " + insts.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return insts.toArray(new RelInstance[insts.size()]);
	}
}
