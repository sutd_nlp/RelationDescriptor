package org.statnlp.example.descriptor.dep;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.Span;
import org.statnlp.example.descriptor.eval.Annotation;
import org.statnlp.example.descriptor.eval.Candidate;
import org.statnlp.example.descriptor.eval.DesEval;
import org.statnlp.example.descriptor.eval.EvalMain.SetSize;
import org.statnlp.example.descriptor.eval.EvalReader;
import org.statnlp.example.descriptor.semeval.SemEvalDataReader;

public class SDPEval {

	
	public static String testFile = "data/semeval/sd_parseTag_test.txt";
	public static int testNum = -1;
	public static String resultFile = "results/me_semeval_res.txt";
	public static String annFile = "results/final_annotation.txt";
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		String specificRel = null;
		List<String> rels = new ArrayList<>();
		if (args.length > 0) {
			specificRel = args[0];
		}
		SemEvalDataReader reader = new SemEvalDataReader();
		Instance[] testData = reader.read(testFile, false, testNum);
		System.out.println(specificRel);
		for (SetSize ss : SetSize.values()) {
			Set<String> ids = new HashSet<>();
			String currIdFile = "results/" + ss.name() + "TestIds.txt";
			String currAnnFile =  "results/" + ss.name() + "Test.txt";
			Stream<String> stream = Files.lines(Paths.get(currIdFile));
			ids = stream.collect(Collectors.toSet());
			stream.close();
			int[][] sdps = new int[ids.size()][];
			int s = 0;
			for (int i = 0; i < testData.length; i++) {
				if (!ids.contains(i + "")) continue;
				RelInstance inst = (RelInstance)testData[i];
				rels.add(inst.getOutput().getType().form);
				CandidatePair input = inst.getInput();
				List<Span> spans = inst.getInput().spans;
				Span arg2Span = spans.get(inst.getInput().rightSpanIdx);
				Span arg1Span = spans.get(inst.getInput().leftSpanIdx);
				sdps[s] = getShortestDepPath(input.sent, arg1Span, arg2Span);
				sdps[s] = refinement(sdps[s], arg1Span, arg2Span);  //remove this become non-refined mode
//				if (arg2Span.end - arg1Span.start > 7 && getLeft(sdps[s]) - arg1Span.start > 1
//						&&  arg2Span.start - getRight(sdps[s]) > 1 && getSum(sdps[s]) > 3) {
//					System.out.println(inst.getInput().sent.toString());
//				}
				s++;
			}
			Annotation[] anns = EvalReader.readAnnotations(currAnnFile);
			Candidate[] cans = new Candidate[ids.size()];
			if (anns.length != cans.length) throw new RuntimeException("length not match");
			//check length
			for (int t = 0; t < cans.length; t++) {
				cans[t] = new Candidate(null, sdps[t]);
			}
			System.out.println(ss);
			DesEval.evaluateTokenAndExactMatchDescriptor(anns, cans, ss, rels, specificRel);
		}
		
//		int[][] sdps = new int[testData.length][];
//		for (int s = 0; s < sdps.length; s++) {
//			RelInstance inst = (RelInstance)testData[s];
//			CandidatePair input = inst.getInput();
//			List<Span> spans = inst.getInput().spans;
//			Span arg2Span = spans.get(inst.getInput().rightSpanIdx);
//			Span arg1Span = spans.get(inst.getInput().leftSpanIdx);
//			sdps[s] = getShortestDepPath(input.sent, arg1Span, arg2Span);
//			sdps[s] = refinement(sdps[s], arg1Span, arg2Span);
//		}
//		Annotation[] anns = EvalReader.readAnnotations(annFile);
//		Candidate[] cans = new Candidate[testData.length];
//		for (int t = 0; t < testData.length; t++) {
//			cans[t] = new Candidate(null, sdps[t]);
//		}
//		//check length
//		for (int i = 0; i < sdps.length; i++) {
//			if (anns[i].sent.length != testData[i].size()) {
//				System.err.println(i + ": " + Arrays.toString(anns[i].sent));
//				throw new RuntimeException("sent length not equal");
//			}
//		}
//		for (SetSize ss : SetSize.values()) {
//			System.out.println(ss);
//			DesEval.evaluateTokenAndExactMatchDescriptor(anns, cans, ss);
//		}
//		measureBetweeness(anns, testData);
	}
	
	private static int[] refinement(int[] sdp, Span arg1Span, Span arg2Span) {
		for (int i = 0; i < sdp.length; i++) {
			if (i < arg1Span.start) sdp[i] = 0;
			if (i > arg2Span.end) sdp[i] = 0;
		}
		int sdpLeft = -1; 
		int sdpRight = -1;
		for (int i = 0; i < sdp.length; i++) {
			if (sdp[i] == 1) { sdpLeft = i; break;}
		}
		for (int i = sdp.length - 1; i >=0; i--) {
			if (sdp[i] == 1) {sdpRight = i; break;}
		}
		if (sdpLeft >= 0 && sdpRight >= 0) {
			for (int i = sdpLeft; i <= sdpRight; i++) {
				if (sdp[i] == 0) sdp[i] = 1;
			}
		}
		return sdp;
	}
	
	public static void measureBetweeness(Annotation[] anns, Instance[] insts) {
		int betweenAllAgree = 0;
		int hasDesAllAgree = 0;
		int betweenTwoSame = 0;
		int hasDesTwoSame = 0;
		int contAndBetAllAgree = 0;
		int contAndBetTwoSame = 0;
		for (int a = 0; a < anns.length; a++) {
			RelInstance inst = (RelInstance)insts[a];
			List<Span> spans = inst.getInput().spans;
			Span arg1Span = spans.get(inst.getInput().leftSpanIdx);
			Span arg2Span = spans.get(inst.getInput().rightSpanIdx);
			Annotation ann = anns[a];
			if (ann.allAgree) {
				if (hasDescriptor(ann.anns[0])) {
					hasDesAllAgree++; 
					if (isBetween(ann.anns[0], arg1Span.end, arg2Span.start)) betweenAllAgree++;
					if (isBetween(ann.anns[0], arg1Span.end, arg2Span.start) && isContinuous(ann.anns[0])) contAndBetAllAgree++;
				}
			}
			if (ann.twoSame) {
				if (hasDescriptor(ann.anns[ann.twoSameIdx])) {
					hasDesTwoSame++;
					if (isBetween(ann.anns[ann.twoSameIdx], arg1Span.end, arg2Span.start)) betweenTwoSame++;
					if (isBetween(ann.anns[0], arg1Span.end, arg2Span.start) && isContinuous(ann.anns[ann.twoSameIdx])) contAndBetTwoSame++;
				}
			}
		}
		System.out.println("between: " + betweenAllAgree + ", totalHasDesAllgree: " + hasDesAllAgree + " pers: " + betweenAllAgree * 1.0 / hasDesAllAgree);
		System.out.println("between: " + betweenTwoSame + ", totalHasDesTwoSame: " + hasDesTwoSame + " pers: " + betweenTwoSame * 1.0 / hasDesTwoSame);
		System.out.println("contbet	: " + contAndBetAllAgree + ", totalHasDesAllgree: " + hasDesAllAgree + " pers: " + contAndBetAllAgree * 1.0 / hasDesAllAgree);
		System.out.println("contbet: " + contAndBetTwoSame + ", totalHasDesTwoSame: " + hasDesTwoSame + " pers: " + contAndBetTwoSame * 1.0 / hasDesTwoSame);
	}
	
	private static boolean isBetween(int[] ans, int e1End, int e2Start) {
//		System.out.println(Arrays.toString(ans));
//		System.out.println(e1End + ", " + e2Start);
		for (int i = 0; i < ans.length; i++) {
			if (ans[i] == 1) {
				if (i < e1End ||i > e2Start) return false;
			}
		}
//		System.out.println("retuning true");
		return true;
	}
	
	private static boolean hasDescriptor(int[] ann) {
		for (int i = 0; i < ann.length; i++) {
			if (ann[i] == 1) return true;
		}
		return false;
	}
	
	public static int[] getShortestDepPath(Sentence sent, Span arg1Span, Span arg2Span) {
		Graph<Integer, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
		int[] des = new int[sent.length()];
		for (int i = 0; i < sent.length(); i++) {
			g.addVertex(i);
		}
		for (int i = 0; i < sent.length(); i++) {
			int head = sent.get(i).getHeadIndex();
			if (head != -1) {
				g.addEdge(i, head);
			}
		}
		DijkstraShortestPath<Integer, DefaultEdge> dg = new DijkstraShortestPath<>(g);
		GraphPath<Integer, DefaultEdge>  results = dg.getPath(arg1Span.headIdx, arg2Span.headIdx);
		List<Integer> list =  results.getVertexList();
		StringBuilder sb = new StringBuilder();
		for (int v : list) {
			if (v >= arg1Span.start && v <= arg1Span.end) continue;
			if (v >= arg2Span.start && v <= arg2Span.end) continue;
			if (v == arg1Span.headIdx || v == arg2Span.headIdx) continue;
			des[v] = 1;
			sb.append(sent.get(v).getForm() + " ");
		}
//		System.out.println(sb.toString());
		return des;
	}
	
	private static int getSum(int[] a) {
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}
	private static int getLeft(int[] a) {
		int left = -1;
		for (int i = 0; i < a.length; i++) {
			if (a[i] == 1) {left = i; break;}
		}
		return left;
	}
	
	private static int getRight(int[] a) {
		int right = -1;
		for (int i = a.length - 1; i>=0; i--) {
			if (a[i] == 1) {right = i; break;}
		}
		return right;
	}
	
	private static boolean isContinuous(int[] ann) {
		int sum = 0;
		for (int i = 0; i < ann.length; i++) {
			sum += ann[i];
		}
		int left = -1;
		for (int i = 0; i < ann.length; i++) {
			if (ann[i] == 1) {left = i; break;}
		}
		int right = -1;
		for (int i = ann.length - 1; i >= 0; i--) {
			if (ann[i] == 1) {right = i; break;}
		}
		if (right - left + 1 == sum) {
			return true;
		} else return false;
	}
	
}
