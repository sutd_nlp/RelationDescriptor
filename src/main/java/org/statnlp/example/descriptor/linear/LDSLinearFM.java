package org.statnlp.example.descriptor.linear;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;

public class LDSLinearFM extends FeatureManager {

	private static final long serialVersionUID = 3401885156226214176L;

	private enum FeaType {
		word,
		tag,
		bigram_word,
		bigram_tag,
		fo_word,
		fo_tag,
		fo_bigram_word,
		fo_bigram_tag,
		transition,spanlen
		}
	
	private List<String> labels;
	
	public LDSLinearFM(GlobalNetworkParam param_g, List<String> labels) {
		super(param_g);
		this.labels = labels;
	}

	@Override
	protected FeatureArray extract_helper(Network network, int parent_k, int[] children_k, int children_k_index) {
		int[] paArr = network.getNodeArray(parent_k);
		int nodeType = paArr[4];
		if (nodeType != LDSLinearCompiler.NodeType.node.ordinal())
			return FeatureArray.EMPTY;
		RelInstance inst = (RelInstance)network.getInstance();
		CandidatePair input = inst.getInput();
		Sentence sent = input.sent;
		FeatureArray startFa = this.createFeatureArray(network, new int[0]);
		FeatureArray currFa = startFa;
		String currLabel = this.labels.get(paArr[1]);
		if (currLabel.startsWith(LinearConfig.O_LABEL)) currLabel = LinearConfig.O_LABEL;
		this.addDescriptorFeats(currFa, network, parent_k, children_k, sent, currLabel);
		return startFa;
	}

	private FeatureArray addDescriptorFeats (FeatureArray currFa, Network network, int parent_k, int[] children_k,
			Sentence sent, String currLabel) {
		List<Integer> fs = new ArrayList<>();
		int[] paArr = network.getNodeArray(parent_k);
		int[] child = network.getNodeArray(children_k[0]);
		//use zero order first;
		String prevLabel = child[0] == 0 ? LinearConfig.O_LABEL : this.labels.get(child[1]);
		if (prevLabel.equals(LinearConfig.OE_LABEL)) prevLabel = LinearConfig.O_LABEL;
		String output = currLabel;
		int i = paArr[0];
		for (int j = i - 2; j <= i + 2; j++) {
			int add = j - sent.length();
			int relPos = j - i;
			String word = j < 0 ? "START" + j : j >= sent.length() ? "END" + add :  sent.get(j).getForm();
			String tag  = j < 0 ? "START_TAG" + j : j >= sent.length() ? "END_TAG" + add :  sent.get(j).getTag();
			
			fs.add(this._param_g.toFeature(network, FeaType.word.name() + relPos, output, word));
			fs.add(this._param_g.toFeature(network, FeaType.tag.name() + relPos, output, tag));
			
			fs.add(this._param_g.toFeature(network, FeaType.fo_word.name() + relPos, output, word + " " + prevLabel));
			fs.add(this._param_g.toFeature(network, FeaType.fo_tag.name() + relPos, output, tag + " " + prevLabel));
		}
		for (int j = i - 1; j <= i + 2; j++) {
			int k = j - 1;
			int relPos = j - i;
			int addJ = j - sent.length();
			int addK = k - sent.length();
			String prevWord = k < 0 ? "START" + k : k >= sent.length() ? "END" + addK :  sent.get(k).getForm();
			String prevTag = k < 0 ? "START_TAG" + k : k >= sent.length() ? "END_TAG" + addK :  sent.get(k).getTag();
			String word = j < 0 ? "START" + j : j >= sent.length() ? "END" + addJ:  sent.get(j).getForm();
			String tag  = j < 0 ? "START_TAG" + j : j >= sent.length() ? "END_TAG" + addJ:  sent.get(j).getTag();
			
			fs.add(this._param_g.toFeature(network, FeaType.bigram_word.name() + relPos, output, prevWord + " " + word));
			fs.add(this._param_g.toFeature(network, FeaType.bigram_tag.name() + relPos, output, prevTag + " " + tag));
			 
			fs.add(this._param_g.toFeature(network, FeaType.fo_bigram_word.name() + relPos, output, prevWord + " " + word + " " + prevLabel));
			fs.add(this._param_g.toFeature(network, FeaType.fo_bigram_tag.name() + relPos, output, prevTag + " " + tag + " " + prevLabel));
		}
		fs.add(this._param_g.toFeature(network, FeaType.transition.name(), currLabel, prevLabel));
		currFa = currFa.addNext(this.createFeatureArray(network, fs));
		if (currLabel.startsWith(LinearConfig.B_PREFIX) || currLabel.startsWith(LinearConfig.I_PREFIX) ||
				(currLabel.equals(LinearConfig.O_LABEL) && paArr[0] == sent.length() - 1)  ) {
			List<Integer> spanFs = new ArrayList<>();
			spanFs.add(this._param_g.toFeature(network, FeaType.spanlen.name(), "",  ""));
			if (currLabel.startsWith(LinearConfig.B_PREFIX)) spanFs.add(this._param_g.toFeature(network, FeaType.spanlen.name(), "",  ""));
			currFa = currFa.addNext(this.createFeatureArray(network, spanFs));
		}
//		if (paArr[0] == sent.length() - 1) {
//			List<Integer> spanFs = new ArrayList<>();
//			List<Double> vals = new ArrayList<>();
//			int spanLen = paArr[2] == sent.length()? 1 : paArr[3] - paArr[2] + 1;
//			spanFs.add(this._param_g.toFeature(network, FeaType.spanlen.name(), "",  ""));
//			vals.add(1.0 / (spanLen + 1));
//			currFa = currFa.addNext(this.createFeatureArray(network, spanFs, vals));
//		}
		return currFa;
	}
}
