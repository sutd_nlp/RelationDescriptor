package org.statnlp.example.descriptor.linear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.example.descriptor.linear.LinearConfig.Baseline;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class LDSLinearCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -6539162975330444420L;

	public enum NodeType {start, node, end};//relaiton is node, always fix

	private boolean DEBUG = true;
	
	private int latentNeighbor = 0;
	private List<String> labels;
	private Map<String, Integer> label2Id;
	private Baseline baseline = Baseline.notused;
	private static Random rand = new Random(1234);

	static {
		//position, labelId, nodeType
		NetworkIDMapper.setCapacity(new int[]{100, RelationType.RELS.size() * 2 + 2, 100, 100, 3});
	}
	
	public LDSLinearCompiler(int latentNeighbor, List<String> labels, Baseline baseline) {
		this.latentNeighbor = latentNeighbor;
		this.labels = labels;
		this.label2Id = new HashMap<>();
		this.baseline = baseline;
		for (int id = 0; id < this.labels.size(); id++) {
			this.label2Id.put(this.labels.get(id), id);
		}
		System.out.println("[Info] num: " + this.label2Id.size());
		System.out.println("[Info] label set: " + this.label2Id.toString());
		System.out.println("[Info] using baseline: " + this.baseline);
	}
	
	private long toNode_leaf() {
		return toNode(0, 0, 0, 0, NodeType.start); 
	}
	
	private long toNode(int position, int label, int dl, int dr) {
		return toNode(position, label, dl, dr, NodeType.node);
	}
	
	private long toNode_root(int size) {
		return toNode(size, 0, 0, 0, NodeType.end);
	}
	
	private long toNode(int position, int labelId, int dl, int dr, NodeType nodeType) {
		return NetworkIDMapper.toHybridNodeID(new int[]{position, labelId, 0, 0, nodeType.ordinal()});
	}
	
	
	
	@Override
	public Network compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance rInst = (RelInstance)inst;
		CandidatePair input = rInst.getInput();
		Sentence sent = input.sent;
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		RelationDescriptor descriptor = rInst.getOutput();
		String relForm = descriptor.type.form;
		Span leftSpan = input.spans.get(leftIdx);
		Span rightSpan = input.spans.get(rightIdx); 
		int[] sdps = this.getShortestDepPath(sent, leftSpan, rightSpan);
		int[] startEnds = this.getStartEndFromSDPs(leftSpan.end, rightSpan.start, sdps);
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long[] children = new long[]{leaf};
		if (startEnds[0] == -1) {
			for (int i = 0; i < sent.length(); i++) {
				long curr = this.toNode(i, this.label2Id.get(LinearConfig.O_LABEL), sent.length(), sent.length());
				builder.addNode(curr);
				builder.addEdge(curr, children);
				children = new long[]{curr};
			}
			long root = toNode_root(sent.length());
			builder.addNode(root);
			builder.addEdge(root, children);
		} else {
			int s_start = this.latentNeighbor >= 0 ? 0 : this.latentNeighbor;
			int s_end = this.latentNeighbor >= 0 ? this.latentNeighbor : 0;
			int e_start = this.latentNeighbor >= 0 ? 0 : this.latentNeighbor;
			int e_end = this.latentNeighbor >= 0 ? this.latentNeighbor : 0;
			for (int sdiff = s_start; sdiff <= s_end; sdiff++) {
				for (int ediff = e_start; ediff <= e_end; ediff++) {
					if (startEnds[0] - sdiff <= startEnds[1] + ediff &&
							startEnds[0] - sdiff > leftSpan.end && 
							startEnds[1] + ediff < rightSpan.start) {
						int dl = startEnds[0] - sdiff;
						int dr = startEnds[1] + ediff;
						children = new long[]{leaf};
						for (int i = 0; i < sent.length(); i++) {
							long curr = -1;
							if (i < dl) {
								curr = this.toNode(i, this.label2Id.get(LinearConfig.O_LABEL), dl, dr);
							} else if (i > dr) {
								curr = this.toNode(i, this.label2Id.get(LinearConfig.OE_LABEL), dl, dr);
							} else {
								if (i == dl) {
									curr = this.toNode(i, this.label2Id.get(LinearConfig.B_PREFIX + relForm), dl, dr);
								} else {
									curr = this.toNode(i, this.label2Id.get(LinearConfig.I_PREFIX + relForm), dl, dr);
								}
							}
							builder.addNode(curr);
							builder.addEdge(curr, children);
							children = new long[]{curr};
						}
						long root = toNode_root(sent.length());
						builder.addNode(root);
						builder.addEdge(root, children);
					}
				}
			}
		}
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network)) {
				System.err.println(sent.toString());
				System.err.println("not contains");
				throw new RuntimeException("network is not contained");
			}
				
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance rInst = (RelInstance)inst;
		CandidatePair input = rInst.getInput();
		Sentence sent = input.sent;
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		RelationDescriptor descriptor = rInst.getOutput();
		String relForm = descriptor.type.form;
		Span leftSpan = input.spans.get(leftIdx);
		Span rightSpan = input.spans.get(rightIdx); 
//		int[] sdps = this.getShortestDepPath(sent, leftSpan, rightSpan);
//		int[] startEnds = this.getStartEndFromSDPs(leftSpan.end, rightSpan.start, sdps);
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long[] children = new long[]{leaf};
		for (int dl = leftSpan.end + 1; dl < rightSpan.start; dl++) {
			for (int dr = dl; dr < rightSpan.start; dr++) {
				children = new long[]{leaf};
				for (int pos = 0; pos < sent.length(); pos++) {
					long curr = -1;
					if (pos < dl) {
						curr = this.toNode(pos, this.label2Id.get(LinearConfig.O_LABEL), dl, dr);
					} else if (pos > dr) {
						curr = this.toNode(pos, this.label2Id.get(LinearConfig.OE_LABEL), dl, dr);
					} else {
						if (pos == dl) {
							curr = this.toNode(pos, this.label2Id.get(LinearConfig.B_PREFIX + relForm), dl, dr);
						} else {
							curr = this.toNode(pos, this.label2Id.get(LinearConfig.I_PREFIX + relForm), dl, dr);
						}
					}
					builder.addNode(curr);
					builder.addEdge(curr, children);
					children = new long[]{curr};
				}
				long root = toNode_root(sent.length());
				builder.addNode(root);
				builder.addEdge(root, children);
			}
		}
		children = new long[]{leaf};
		for (int pos = 0; pos < sent.length(); pos++) {
			long curr = this.toNode(pos, this.label2Id.get(LinearConfig.O_LABEL), sent.length(), sent.length());
			builder.addNode(curr);
			builder.addEdge(curr, children);
			children = new long[]{curr};
		}
		long root = toNode_root(sent.length());
		builder.addNode(root);
		builder.addEdge(root, children);
		BaseNetwork network = builder.build(networkId, inst, param, this);
		return network;
	}

	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root(inst.size());
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		int left = -1;
		int right = -1;
		for (int i = 0; i < inst.size(); i++) {
			int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
			int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
			String currLabel = this.labels.get(arr[1]);
			if (right == -1 && currLabel.startsWith(LinearConfig.I_PREFIX)) {
				right = arr[0];
			}
			if (currLabel.startsWith(LinearConfig.B_PREFIX)) {
				left = arr[0];
				if (right == -1) right = arr[0];
			}
			nodeIdx = labeledNodeIdx;
		}
		if (this.baseline == Baseline.span_extend) {
			CandidatePair input = inst.getInput();
			Sentence sent = input.sent;
			int leftIdx = input.leftSpanIdx;
			int rightIdx = input.rightSpanIdx;
			Span leftSpan = input.spans.get(leftIdx);
			Span rightSpan = input.spans.get(rightIdx); 
			int[] sdps = this.getShortestDepPath(sent, leftSpan, rightSpan);
			int[] startEnds = this.getStartEndFromSDPs(leftSpan.end, rightSpan.start, sdps);
			left = startEnds[0] > 0 ? startEnds[0] - 1 :startEnds[0] ==0? 0 : -1 ;
//			right = startEnds[0] < 0 ? -1 : startEnds[1] + 1 < sent.length() ? startEnds[1] + 1 : sent.length() - 1;
			right = startEnds[0] < 0 ? -1 : startEnds[1];
		} else if (this.baseline == Baseline.span_rand){
			//remember to use one thread only for randomness
			CandidatePair input = inst.getInput();
			Sentence sent = input.sent;
			int leftIdx = input.leftSpanIdx;
			int rightIdx = input.rightSpanIdx;
			Span leftSpan = input.spans.get(leftIdx);
			Span rightSpan = input.spans.get(rightIdx); 
			int[] sdps = this.getShortestDepPath(sent, leftSpan, rightSpan);
			int[] startEnds = this.getStartEndFromSDPs(leftSpan.end, rightSpan.start, sdps);
			List<int[]> candidates = new ArrayList<>();
			if (startEnds[0] >= 0) {
				for (int sdiff = 0; sdiff <= 1; sdiff++) {
					for (int ediff = 0; ediff <= 1; ediff++) {
						if (startEnds[0] - sdiff <= startEnds[1] + ediff &&
								startEnds[0] - sdiff > leftSpan.end && 
								startEnds[1] + ediff < rightSpan.start) {
							int dl = startEnds[0] - sdiff;
							int dr = startEnds[1] + ediff;
							candidates.add(new int[]{dl, dr});
						}
					}
				}
				if (candidates.size() == 0) throw new RuntimeException("no candidatees?");
				int idx = rand.nextInt(candidates.size());
				left = candidates.get(idx)[0];
				right = candidates.get(idx)[1];
			} else {
				left = -1;
				right = -1;
			}
		}
		RelationDescriptor prediction = new RelationDescriptor(inst.getOutput().type, left, right);
		inst.setPrediction(prediction);
		return inst;
	}
	
	
	
	private int[] getStartEndFromSDPs(int leftBound, int rightBound, int[] sdps) {
		int[] startEnd = new int[2];
		startEnd[0] = -1;
		startEnd[1] = -1;
		for (int i = 0; i < sdps.length; i++) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[0] = i; break;}
		}
		for (int i = sdps.length - 1; i >= 0; i--) {
			if (i > leftBound && i < rightBound && sdps[i] == 1) { startEnd[1] = i; break;}
		}
		return startEnd;
	}
	
	private  int[] getShortestDepPath(Sentence sent, Span arg1Span, Span arg2Span) {
		Graph<Integer, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
		int[] des = new int[sent.length()];
		for (int i = 0; i < sent.length(); i++) {
			g.addVertex(i);
		}
		for (int i = 0; i < sent.length(); i++) {
			int head = sent.get(i).getHeadIndex();
			if (head != -1) {
				g.addEdge(i, head);
			}
		}
		DijkstraShortestPath<Integer, DefaultEdge> dg = new DijkstraShortestPath<>(g);
		GraphPath<Integer, DefaultEdge>  results = dg.getPath(arg1Span.headIdx, arg2Span.headIdx);
		List<Integer> list =  results.getVertexList();
		StringBuilder sb = new StringBuilder();
		for (int v : list) {
			if (v >= arg1Span.start && v <= arg1Span.end) continue;
			if (v >= arg2Span.start && v <= arg2Span.end) continue;
			if (v == arg1Span.headIdx || v == arg2Span.headIdx) continue;
			des[v] = 1;
			sb.append(sent.get(v).getForm() + " ");
		}
		return des;
	}

}
