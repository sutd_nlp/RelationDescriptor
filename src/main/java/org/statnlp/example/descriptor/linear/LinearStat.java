package org.statnlp.example.descriptor.linear;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.Span;

public class LinearStat {

	public static void checkSameRelation(Instance[] data) {
		//check the entity-origin.
		Map<String, List<Sentence>> map = new HashMap<>();
		for (Instance ins : data) {
			RelInstance inst = (RelInstance)ins;
			CandidatePair input = inst.getInput();
			Sentence sent = input.sent;
			int leftIdx = input.leftSpanIdx;
			int rightIdx = input.rightSpanIdx;
			RelationDescriptor descriptor = inst.getOutput();
			String relForm = descriptor.type.form;
			Span leftSpan = input.spans.get(leftIdx);
			Span rightSpan = input.spans.get(rightIdx); 
			StringBuilder sb1 = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();
			for (int i=leftSpan.start; i<=leftSpan.end;i++) sb1.append(i==leftSpan.start? sent.get(i).getForm() : " " + sent.get(i).getForm());
			for (int i=rightSpan.start; i<=rightSpan.end;i++) sb2.append(i==rightSpan.start? sent.get(i).getForm() : " " + sent.get(i).getForm());
			if (map.containsKey(sb1.toString() + " " + sb2.toString())) {
				map.get(sb1.toString() + " " + sb2.toString()).add(sent);
			} else {
				List<Sentence> sents = new ArrayList<>();
				sents.add(sent);
				map.put(sb1.toString() + " " + sb2.toString(), sents);
			}
		}
		for(String key : map.keySet()) {
			if (map.get(key).size() > 1) {
				System.out.println(key);
				System.out.println(map.get(key));
			}
		}
	}
}
