package org.statnlp.example.descriptor.linear;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationDescriptorEvaluator;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;
import org.statnlp.example.descriptor.eval.Annotation;
import org.statnlp.example.descriptor.eval.Candidate;
import org.statnlp.example.descriptor.eval.DesEval;
import org.statnlp.example.descriptor.eval.EvalMain.SetSize;
import org.statnlp.example.descriptor.linear.LinearConfig.Baseline;
import org.statnlp.example.descriptor.eval.EvalReader;
import org.statnlp.example.descriptor.semeval.SemEvalDataReader;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;
import org.statnlp.hypergraph.StringIndex;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class LDLinearMain {

	
	public static double l2val = 0.01;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static String trainFile = "data/semeval/sd_parseTag_train.txt";
	//we will split out to dev set and test set later.
	public static String testFile = "data/semeval/sd_parseTag_test.txt";
	public static int trainNum = -1;
	public static int testNum = -1;
	public static String resultFile = "results/lds_semeval_res.txt";
	public static String annFile = "results/final_annotation.txt";
	public static boolean saveModel = false;
	public static boolean readModel = false;
	public static String modelFile = "models/lds_semeval_model.m";
	public static OptimizerFactory optimizer = OptimizerFactory.getLBFGSFactory();
	public static boolean readPreLabels = true;
	public static String preLabelsFile = "data/semeval/prevlabels.txt";
	public static String specifiRel = null;
	public static int latentNeighbor = 0;
	public static boolean useSpanPenalty = false;
	public static double penalty = 30;
	public static boolean penaltyNegative = false;
	public static boolean useDev = false;
	public static boolean useGenericRelation = false;
	public static Baseline baseline = Baseline.notused;
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		setArgs(args);
		
		/***
		 * Parameter settings and model configuration
		 */
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.OPTIMIZE_NEURAL = true;
		NetworkConfig.PRINT_BATCH_OBJECTIVE = false;
		NetworkConfig.USE_FEATURE_VALUE = false;
		if (readPreLabels && !useGenericRelation) {
			readPreLabels(preLabelsFile);
		} else {
			RelationType.get("Generic");
//			RelationType.get("Generic" + LinearConfig.REV_SUFFIX);
		}
		SemEvalDataReader reader = new SemEvalDataReader();
		if (penaltyNegative) {
			penalty *= -1;
		}
		/**debug info**/
//		penalty *= -1;
		/***/
		
		Instance[] trainData = reader.read(trainFile, true, trainNum, true, specifiRel, useGenericRelation);
		reader.putInMaxLen(trainData);
//		LinearStat.checkSameRelation(trainData);
		Instance[] testData = reader.read(testFile, false, testNum, true, null, useGenericRelation);
		reader.putInMaxLen(testData);
//		LinearStat.checkSameRelation(testData);
		
		System.out.println(RelationType.RELS.toString());
		System.out.println("#labels: " + RelationType.RELS.size());
		RelationType.lock();
		
		List<String> labels = new ArrayList<String>();
		labels.add(LinearConfig.O_LABEL);
		labels.add(LinearConfig.OE_LABEL);
		for(String label : RelationType.RELS.keySet()) {
			labels.add(LinearConfig.B_PREFIX+label);
			labels.add(LinearConfig.I_PREFIX+label);
		}
		System.out.println("labels list: " + labels.toString());
		
		RelationDescriptorEvaluator evaluator = new RelationDescriptorEvaluator(true);
		NetworkModel model = null;
		if (readModel) {
			System.out.println("[Info] Reading Model....");
			ObjectInputStream in = RAWF.objectReader(modelFile);
			model = (NetworkModel)in.readObject();
		} else {
            GlobalNetworkParam gnp = new GlobalNetworkParam(optimizer);
            gnp.setStoreFeatureReps();
            LDSLinearFM tfm = new LDSLinearFM(gnp, labels);
            LDSLinearCompiler tnc = new LDSLinearCompiler(latentNeighbor, labels, baseline);
			model = DiscriminativeNetworkModel.create(tfm, tnc);
			model.train(trainData, numIteration);
			gnp.getStringIndex().buildReverseIndex();
		}
		if (saveModel) {
			ObjectOutputStream out = RAWF.objectWriter(modelFile);
			out.writeObject(model);
			out.close();
		}
		
		/***Tuning the Feature Weights***/
		if (readModel && useSpanPenalty) {
			GlobalNetworkParam gnp = model.getFeatureManager().getParam_G();
			StringIndex strIdx = gnp.getStringIndex();
			strIdx.buildReverseIndex();
			gnp.setStoreFeatureReps();
			for (int i = 0; i < gnp.size(); i++) {
				int[] fs = gnp.getFeatureRep(i);
				String type = strIdx.get(fs[0]);
				if (type.equals("spanlen")) {
					System.out.println(gnp.getWeight(i));
					gnp.overRideWeight(i, penalty);
					System.out.println(gnp.getWeight(i));
					break;
				}
			}
		}
		/*************/
		
		/**
		 * Testing Phase
		 */
		Instance[] results = model.test(testData);
		evaluator.evaluateRelation(results);
		
		//read the id list.
		Instance[] evalTest = null;
		if (useGenericRelation && specifiRel != null) {
			RelationType.locked = false;
			evalTest = reader.read(testFile, false, testNum, true, null, false);
		}
		System.out.println(specifiRel);
		for (SetSize ss : SetSize.values()) {
			Set<String> ids = new HashSet<>();
			String currIdFile = useDev ? "results/" + ss.name() + "DevIds.txt" : "results/" + ss.name() + "TestIds.txt";
			String currResultFile = useDev ? "results/ldlinear_" + ss.name() + "_dev_rel_" + "res.txt" : "results/ldlinear_" + ss.name() + "_test_" + "res.txt";
			String currAnnFile =  useDev ?  "results/" + ss.name() + "Dev.txt" : "results/" + ss.name() + "Test.txt";
			Stream<String> stream = Files.lines(Paths.get(currIdFile));
			ids = stream.collect(Collectors.toSet());
			stream.close();
			Annotation[] anns = EvalReader.readAnnotations(currAnnFile);
			PrintWriter pw = RAWF.writer(currResultFile);
			List<Instance> depResForEval = new ArrayList<>();
			List<String> rels = new ArrayList<>();
//			int t = 0;
//			Map<Integer, List<Integer>> dist2Len = new HashMap<>();
			for (int i = 0; i < results.length; i++) {
				if (!ids.contains(i + "")) continue;
				Instance res = results[i];
				depResForEval.add(res);
				RelInstance inst = (RelInstance)res;
				Sentence sent = inst.getInput().sent;
				List<Span> spans = inst.getInput().spans;
				Span arg1Span = spans.get(inst.getInput().leftSpanIdx);
				Span arg2Span = spans.get(inst.getInput().rightSpanIdx);
				RelationDescriptor gold = inst.getOutput();
				RelationDescriptor pred = inst.getPrediction();
				String goldForm = gold.getType().form;
				String predForm = pred.getType().form;
//				if (arg2Span.end - arg1Span.start > 7 && pred.getLeft() - arg1Span.start > 2
//						&&  arg2Span.start - pred.getRight() > 2) {
//					System.out.println(sent.toString());
//				}
				if (useGenericRelation && specifiRel != null) {
					RelInstance t = (RelInstance)evalTest[i];
					rels.add(t.getOutput().getType().form);
				} else {
					rels.add(goldForm);
				}
				pw.println(sent.toString());
				pw.println(arg1Span.start+","+arg1Span.end + " " + arg2Span.start+","+arg2Span.end + " "
						+ gold.getLeft() + "," + gold.getRight() + " " + pred.getLeft() + "," + pred.getRight());
				pw.println(goldForm + "," + predForm);
				pw.println();
//				if (ss == SetSize.twoAgree) {
//					if (dist2Len.containsKey(arg2Span.start - arg1Span.end)) {
//						dist2Len.get(arg2Span.start - arg1Span.end).add(DesEval.getLen(anns[t].anns[anns[t].twoSameIdx]));
//					} else {
//						List<Integer> lens = new ArrayList<>();
//						//allagree
//						lens.add(DesEval.getLen(anns[t].anns[anns[t].twoSameIdx]));
//						dist2Len.put(arg2Span.start - arg1Span.end, lens);
//					}
//				}
//				t++;
			}
			pw.close();
			/**Get the distance 2 length distribution**/
//			PrintWriter pwDist = RAWF.writer("results/two-agree-distribution.txt");
//			for(Integer dist : dist2Len.keySet()) {
//				for (Integer len : dist2Len.get(dist)) {
//					pwDist.println(dist + "\t" + len);
//				}
//			}
//			pwDist.close();
//			System.out.println(dist2Len.toString());
			/***Get the distance to length distribution***/
			Candidate[] cans = EvalReader.readRDCandidate(currResultFile);
			//check length
			if (anns.length != cans.length) throw new RuntimeException("num sents not matched");
			for (int i = 0; i < cans.length; i++) {
				if (anns[i].sent.length != cans[i].sent.length) {
					System.err.println(i + ": " + Arrays.toString(anns[i].sent));
					System.err.println(i + ": " + Arrays.toString(cans[i].sent));
					throw new RuntimeException("sent length not equal");
				}
			}
			System.out.println(ss);
			DesEval.evaluateTokenAndExactMatchDescriptor(anns, cans, ss, rels, specifiRel);
//			System.out.println("[depEvaluation]: " + ss);
//			DesEval.evalutateDep(depResForEval.toArray(new Instance[depResForEval.size()]), anns, ss);
//			System.out.println("[End of depEvaluation]: ");
		}
		
	}
	
	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Latent Linear-chain CRF Model for Relation Extraction");
		parser.addArgument("-t", "--thread").type(Integer.class).setDefault(8).help("number of threads");
		parser.addArgument("--l2").type(Double.class).setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--iter").type(Integer.class).setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--trainFile").setDefault(trainFile).help("The path of the traininigFile file");
		parser.addArgument("--testFile").setDefault(testFile).help("The path of the testFile file");
		parser.addArgument("--trainNum").type(Integer.class).setDefault(trainNum).help("The number of training instances");
		parser.addArgument("--testNum").type(Integer.class).setDefault(testNum).help("The number of testing instances");
		parser.addArgument("--saveModel").type(Boolean.class).setDefault(saveModel).help("whether to save the model");
		parser.addArgument("--readModel").type(Boolean.class).setDefault(readModel).help("whether to read the model");
		parser.addArgument("--modelFile").setDefault(modelFile).help("specify the model file");
		parser.addArgument("--resFile").setDefault(resultFile).help("specify the result file");
		parser.addArgument("--os").setDefault(NetworkConfig.OS).help("linux, osx");
		parser.addArgument("--useDev").type(Boolean.class).setDefault(useDev).help("valiation");
		parser.addArgument("--latentNeighbor").type(Integer.class).setDefault(latentNeighbor).help("the latent neighbor number");
		parser.addArgument("--usePenalty").type(Boolean.class).setDefault(useSpanPenalty).help("use the penaly ");
		parser.addArgument("--penalty").type(Double.class).setDefault(penalty).help("value of the penaly ");
		parser.addArgument("--srel").type(String.class).setDefault(specifiRel).help("use a specific relation");
		parser.addArgument("-pn","--penaltyNegative").type(Boolean.class).setDefault(penaltyNegative).help("negative sign for penalty");
		parser.addArgument("--useGeneric").type(Boolean.class).setDefault(useGenericRelation).help("use generic relation or not");
		parser.addArgument("--baseline").type(String.class).setDefault(baseline.toString()).help("span based baseline");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = ns.getInt("thread");
        l2val = ns.getDouble("l2");
        numIteration = ns.getInt("iter");
        trainFile = ns.getString("trainFile");
        testFile = ns.getString("testFile");
        trainNum = ns.getInt("trainNum");
        testNum = ns.getInt("testNum");
        saveModel = ns.getBoolean("saveModel");
        readModel = ns.getBoolean("readModel") ;
        modelFile = ns.getString("modelFile");
        resultFile = ns.getString("resFile");
        useDev = ns.getBoolean("useDev");
        latentNeighbor = ns.getInt("latentNeighbor");
        useSpanPenalty = ns.getBoolean("usePenalty");
        penalty = ns.getDouble("penalty");
        specifiRel = ns.getString("srel");
        NetworkConfig.OS = ns.getString("os");
        penaltyNegative = ns.getBoolean("penaltyNegative");
        useGenericRelation = ns.getBoolean("useGeneric");
        baseline = Baseline.valueOf(ns.getString("baseline")); 
        if (saveModel && readModel) {
        	throw new RuntimeException("save model and read model can't be both true");
        }
        System.err.println(ns.getAttrs().toString());
	}
	
	/**
	 * Reading the label list
	 * @param file
	 */
	private static void readPreLabels(String file) {
		BufferedReader br;
		try {
			br = RAWF.reader(file);
			String line = null; 
			while ((line = br.readLine()) != null) {
				RelationType.get(line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
