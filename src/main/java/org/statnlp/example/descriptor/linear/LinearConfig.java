package org.statnlp.example.descriptor.linear;

public class LinearConfig {

	public static final String O_LABEL = "O";
	public static final String OE_LABEL = "OE";
	
	public static final String B_PREFIX = "B-";
	public static final String I_PREFIX = "I-";
	
	public static final String REV_SUFFIX = "-rev";
	
	public static enum Baseline {
		notused,
		span_extend,
		span_rand
	};
}
