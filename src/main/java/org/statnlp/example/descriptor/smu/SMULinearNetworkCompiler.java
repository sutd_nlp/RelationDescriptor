package org.statnlp.example.descriptor.smu;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class SMULinearNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -3523483926592577270L;

	public enum NodeType {LEAF, NODE, ROOT};
	private boolean DEBUG = false;
	private List<String> labels;
	private Map<String, Integer> label2Id;
	
	static {
		NetworkIDMapper.setCapacity(new int[]{100, 5, 3, 1000000});
	}
	
	/**
	 * Constructing the label map
	 * @param labels
	 */
	public SMULinearNetworkCompiler(List<String> labels) {
		this.labels = labels;
		this.label2Id = new HashMap<>();
		for (int id = 0; id < this.labels.size(); id++) {
			this.label2Id.put(this.labels.get(id), id);
		}
		System.out.println("[Info] label set: " + this.label2Id.toString());
	}

	private long toNode_leaf() {
		return toNode(0, 0, NodeType.LEAF, 0); 
	}
	
	private long toNode_Rel(int pos, int labelId, int chainId) {
		return toNode(pos, labelId, NodeType.NODE, chainId);
	}
	
	private long toNode_root(int size) {
		return toNode(size, 0, NodeType.ROOT, 0);
	}
	
	private long toNode(int pos, int labelId,  NodeType nodeType, int chainId) {
		return NetworkIDMapper.toHybridNodeID(new int[]{pos, labelId, nodeType.ordinal(), chainId});
	}

	@Override
	public BaseNetwork compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance lgInst = (RelInstance)inst;
		CandidatePair input = lgInst.getInput();
		RelationDescriptor descriptor = lgInst.getOutput();
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		String relType = descriptor.getType().form;
		int dl = descriptor.getLeft();
		int dr = descriptor.getRight();
		long child = leaf;
		if (descriptor.getLeft() < 0 && descriptor.getRight() < 0) {
			dl = inst.size();
			dr = inst.size();
		} 
		int chainId = dl * 100 + dr;
		Sentence sent = input.sent;
		for (int p = 0; p < sent.length(); p++) {
			int labelId = -1;
			if (p == dl) {
				labelId = this.label2Id.get("B-"+relType);
			} else if (p > dl && p <= dr) {
				labelId = this.label2Id.get("I-"+relType);
			} else {
				labelId = this.label2Id.get("O");
			}
			long node = this.toNode_Rel(p, labelId, chainId);
			builder.addNode(node);
			builder.addEdge(node, new long[]{child});
			child = node;
		}
		long root = toNode_root(sent.length());
		builder.addNode(root);
		builder.addEdge(root, new long[]{child});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network)) {
				System.out.println(lgInst.getInput().sent.toString());
				System.err.println("not contains");
				throw new RuntimeException("not contained");
			}
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long root = this.toNode_root(inst.size());
		builder.addNode(root);
		RelInstance lgInst = (RelInstance)inst;
		CandidatePair input = lgInst.getInput();
		RelationDescriptor output = lgInst.getOutput();
		String relType = output.getType().form;
		Sentence sent = input.sent;
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		long child = leaf;
		for (int left = 0; left < inst.size(); left++) {
			int maxRight = inst.size();
			for (int right = left; right < maxRight; right++) {
				//make sure the two arguments are not overlapped with the two arguments
				if (leftIdx >= left && leftIdx <= right) continue;
				if (rightIdx >= left && rightIdx <= right) continue;
				int chainId = left * 100 + right;
				child = leaf;
				for (int p = 0; p < sent.length(); p++) {
					int labelId = -1;
					if (p == left) {
						labelId = this.label2Id.get("B-"+relType);
					} else if (p > left && p <= right) {
						labelId = this.label2Id.get("I-"+relType);
					} else {
						labelId = this.label2Id.get("O");
					}
					long node = this.toNode_Rel(p, labelId, chainId);
					builder.addNode(node);
					builder.addEdge(node, new long[]{child});
					child = node;
				}
				builder.addEdge(root, new long[]{child});
			}
		}
		child = leaf;
		int chainId = inst.size() * 100 + inst.size();
		for (int p = 0; p < sent.length(); p++) {
			long node = this.toNode_Rel(p, this.label2Id.get("O"), chainId);
			builder.addNode(node);
			builder.addEdge(node, new long[]{child});
			child = node;
		}
		builder.addEdge(root, new long[]{child});
		return builder.build(networkId, inst, param, this);
	}
	
	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root(inst.size());
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		
		int rdLeft = -1;
		int rdRight = -1;
		for (int i = 0; i < inst.size(); i++) {
			int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
			int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
			int pos = arr[0];
			int labelId = arr[1];
			String label = this.labels.get(labelId);
			if (label.startsWith("I-") && rdRight < 0) {
				rdRight = pos;
			}
			if (label.startsWith("B-")) {
				rdLeft = pos;
			}
			nodeIdx = labeledNodeIdx;
		}
		if (rdLeft >= 0 && rdRight <0) {
			rdRight = rdLeft;
		}
		RelationDescriptor prediction = new RelationDescriptor(inst.getOutput().getType(), rdLeft, rdRight);
		inst.setPrediction(prediction);
		return inst;
	}


}
