package org.statnlp.example.descriptor.smu;

import java.util.Arrays;

import org.statnlp.commons.types.Instance;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkIDMapper;

public class SMUNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -3523483926592577270L;

	public enum NodeType {LEAF, SPAN, NODE, ROOT};
	private boolean DEBUG = true;
	
	private boolean latentDescriptor;
	
	static {
		NetworkIDMapper.setCapacity(new int[]{4, 200, 200, RelationType.RELS.size()});
	}
	
	public SMUNetworkCompiler(boolean useLatentSpan) {
		//nodeType, leftIndex, rightIndex, relation type.
		this.latentDescriptor = useLatentSpan;
	}

	private long toNode_leaf() {
		return toNode(NodeType.LEAF, 0, 0, 0); 
	}
	
	private long toNode_Span(int leftIndex, int rightIndex){
		return toNode(NodeType.SPAN, leftIndex, rightIndex, 0);
	}
	
	private long toNode_noSpan(int sentLen){
		return toNode(NodeType.SPAN, sentLen, sentLen, 0);
	}
	
	private long toNode_Rel(int label) {
		return toNode(NodeType.NODE, 0, 0, label);
	}
	
	private long toNode_root() {
		return toNode(NodeType.ROOT, 0, 0, 0);
	}
	
	private long toNode(NodeType nodeType, int leftIndex, int rightIndex, int labelId) {
		return NetworkIDMapper.toHybridNodeID(new int[]{nodeType.ordinal(), leftIndex, rightIndex, labelId});
	}

	@Override
	public BaseNetwork compileLabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		RelInstance lgInst = (RelInstance)inst;
		CandidatePair input = lgInst.getInput();
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		RelationDescriptor descriptor = lgInst.getOutput();
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long node = toNode_Rel(lgInst.getOutput().getType().id);
		builder.addNode(node);
		if (this.latentDescriptor) {
			for (int left = 0; left < lgInst.size(); left++) {
				for (int right = left; right < lgInst.size(); right++) {
					//make sure the two arguments are not overlapped with the two arguments
					if (leftIdx >= left && leftIdx <= right) continue;
					if (rightIdx >= left && rightIdx <= right) continue;
					long spanNode = this.toNode_Span(left, right);
					builder.addNode(spanNode);
					builder.addEdge(spanNode, new long[]{leaf});
					builder.addEdge(node, new long[]{spanNode});
				}
			}
			long noSpanNode = this.toNode_noSpan(lgInst.size());
			builder.addNode(noSpanNode);
			builder.addEdge(noSpanNode, new long[]{leaf});
			builder.addEdge(node, new long[]{noSpanNode});
		} else {
			long spanNode = -1;
			if (descriptor.getLeft() < 0 && descriptor.getRight() < 0) {
				spanNode = this.toNode_noSpan(lgInst.size());
			} else {
				spanNode = this.toNode_Span(descriptor.getLeft(), descriptor.getRight());
			}
			builder.addNode(spanNode);
			builder.addEdge(spanNode, new long[]{leaf});
			builder.addEdge(node, new long[]{spanNode});
		}
		long root = toNode_root();
		builder.addNode(root);
		builder.addEdge(root, new long[]{node});
		BaseNetwork network = builder.build(networkId, inst, param, this);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if(!unlabeled.contains(network)) {
				System.out.println(lgInst.getInput().sent.toString());
				System.err.println("not contains");
				throw new RuntimeException("not contained");
			}
		}
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance inst, LocalNetworkParam param) {
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder(BaseNetwork.class);
		long leaf = toNode_leaf();
		builder.addNode(leaf);
		long root = toNode_root();
		builder.addNode(root);
		RelInstance lgInst = (RelInstance)inst;
		CandidatePair input = lgInst.getInput();
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		long node = toNode_Rel(lgInst.getOutput().getType().id);
		builder.addNode(node);
		for (int left = 0; left < inst.size(); left++) {
			int maxRight = inst.size();
			for (int right = left; right < maxRight; right++) {
				//make sure the two arguments are not overlapped with the two arguments
				if (leftIdx >= left && leftIdx <= right) continue;
				if (rightIdx >= left && rightIdx <= right) continue;
				long spanNode = this.toNode_Span(left, right);
				builder.addNode(spanNode);
				builder.addEdge(spanNode, new long[]{leaf});
				builder.addEdge(node, new long[]{spanNode});
			}
		}
		long noSpanNode = this.toNode_noSpan(lgInst.size());
		builder.addNode(noSpanNode);
		builder.addEdge(noSpanNode, new long[]{leaf});
		builder.addEdge(node, new long[]{noSpanNode});
		builder.addEdge(root, new long[]{node});
		return builder.build(networkId, inst, param, this);
	}
	
	@Override
	public Instance decompile(Network network) {
		BaseNetwork baseNetwork = (BaseNetwork)network;
		RelInstance inst = (RelInstance)network.getInstance();
		long node = this.toNode_root();
		int nodeIdx = Arrays.binarySearch(baseNetwork.getAllNodes(), node);
		int labeledNodeIdx = baseNetwork.getMaxPath(nodeIdx)[0];
		int[] arr = baseNetwork.getNodeArray(labeledNodeIdx);
		int labelId = arr[3];
		RelationType predType = RelationType.get(labelId);
		nodeIdx = baseNetwork.getMaxPath(labeledNodeIdx)[0];
		arr = baseNetwork.getNodeArray(nodeIdx);
		int left = arr[1];
		int right = arr[2];
		if (arr[1] == inst.size() && arr[2] == inst.size()) {
			left = -1;
			right = -1;
		}
		RelationDescriptor prediction = new RelationDescriptor(predType, left, right);
		inst.setPrediction(prediction);
		return inst;
	}


}
