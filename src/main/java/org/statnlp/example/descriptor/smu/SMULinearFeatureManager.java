package org.statnlp.example.descriptor.smu;

import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.Config;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.smu.SMULinearNetworkCompiler.NodeType;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;

public class SMULinearFeatureManager extends FeatureManager {

	private static final long serialVersionUID = -7169027921164193697L;

	private enum FeaType {hm1, hm2, hm12,
		word,
		tag,
		phrase,
		bigram_word,
		bigram_tag,
		bigram_phrase,
		fo_word,
		fo_tag,
		fo_phrase,
		fo_bigram_word,
		fo_bigram_tag,
		fo_bigram_phrase,
		transition,
		contextual,
		path}
	
	private boolean contextual = true;
	private boolean path = true;
	private boolean first_order = true;
	
	private List<String> labels;
	
	public SMULinearFeatureManager(GlobalNetworkParam param_g, boolean contextual,
			boolean path, List<String> labels) {
		super(param_g);
		this.contextual = contextual;
		this.path = path;
		this.labels = labels;
	}

	@Override
	protected FeatureArray extract_helper(Network network, int parent_k, int[] children_k, int children_k_index) {
		int[] paArr = network.getNodeArray(parent_k);
		int nodeType = paArr[2];
		if (nodeType != NodeType.NODE.ordinal())
			return FeatureArray.EMPTY;
		RelInstance inst = (RelInstance)network.getInstance();
		CandidatePair input = inst.getInput();
		Sentence sent = input.sent;
		int leftIdx = input.leftSpanIdx;
		int rightIdx = input.rightSpanIdx;
		
//		String rel = relId + "";
		
		int arg1Idx = leftIdx;
		int arg2Idx = rightIdx;
		
		
		
		List<Integer> ugf = new ArrayList<>(6);
		List<Integer> bgf = new ArrayList<>(6);
		List<Integer> tran = new ArrayList<>(1);
		List<Integer> context = new ArrayList<>(4);
		List<Integer> path = new ArrayList<>(6);
		
		
		int pos = paArr[0];
		int relId = paArr[1];
		String currLabelwD = this.labels.get(relId);
		String currLabel = currLabelwD.replaceAll(Config.REV_SUFF, "");
		
		int[] child = network.getNodeArray(children_k[0]);
		String prevLabelwD = this.labels.get(child[1]);
		String prevLabel = prevLabelwD.replaceAll(Config.REV_SUFF, "");
		
		String output = currLabel;
		
		if (currLabelwD.endsWith(Config.REV_SUFF)) {
			arg1Idx = rightIdx;
			arg2Idx = leftIdx;
		}
		
		
		for (int j = pos - 2; j <= pos + 2; j++) {
			int add = j - sent.length();
			int relPos = j - pos;
			String word = j < 0 ? "START" + j : j >= sent.length() ? "END" + add :  sent.get(j).getForm();
			String tag  = j < 0 ? "START_TAG" + j : j >= sent.length() ? "END_TAG" + add :  sent.get(j).getTag();
			String phrase  = j < 0 ? "START_Phrase" + j : j >= sent.length() ? "END_Phrase" + add : sent.get(j).getPhraseTag();
			ugf.add(this._param_g.toFeature(network, FeaType.word.name() + relPos, output, word));
			ugf.add(this._param_g.toFeature(network, FeaType.tag.name() + relPos, output, tag));
			ugf.add(this._param_g.toFeature(network, FeaType.phrase.name() + relPos, output, phrase));
//			if (network.getInstance().getInstanceId() > 0) {
//				System.out.println(FeaType.word.name() + relPos + "," +  output +","+ word);
//				System.out.println(FeaType.tag.name() + relPos + "," +  output + "," +  tag);
//				System.out.println(FeaType.phrase.name() + relPos + "," +  output + "," +  phrase);
//			}
			if (first_order) {
				ugf.add(this._param_g.toFeature(network, FeaType.fo_word.name() + relPos, output, word + " " + prevLabel));
				ugf.add(this._param_g.toFeature(network, FeaType.fo_tag.name() + relPos, output, tag + " " + prevLabel));
				ugf.add(this._param_g.toFeature(network, FeaType.fo_phrase.name() + relPos, output, phrase + " " + prevLabel));
			}
		}
		for (int j = pos - 1; j <= pos + 2; j++) {
			int k = j - 1;
			int relPos = j - pos;
			int addJ = j - sent.length();
			int addK = k - sent.length();
			String prevWord = k < 0 ? "START" + k : k >= sent.length() ? "END" + addK :  sent.get(k).getForm();
			String prevTag = k < 0 ? "START_TAG" + k : k >= sent.length() ? "END_TAG" + addK :  sent.get(k).getTag();
			String prevPhrase = k < 0 ? "START_Phrase" + k : k >= sent.length() ? "END_Phrase" + addK : sent.get(k).getPhraseTag();
			String word = j < 0 ? "START" + j : j >= sent.length() ? "END" + addJ:  sent.get(j).getForm();
			String tag  = j < 0 ? "START_TAG" + j : j >= sent.length() ? "END_TAG" + addJ:  sent.get(j).getTag();
			String phrase  = j < 0 ? "START_Phrase" + j : j >= sent.length() ? "END_Phrase" + addJ : sent.get(j).getPhraseTag();
			bgf.add(this._param_g.toFeature(network, FeaType.bigram_word.name() + relPos, output, prevWord + " " + word));
			bgf.add(this._param_g.toFeature(network, FeaType.bigram_tag.name() + relPos, output, prevTag + " " + tag));
			bgf.add(this._param_g.toFeature(network, FeaType.bigram_phrase.name() + relPos, output, prevPhrase + " " +phrase));
			if (first_order) {
				bgf.add(this._param_g.toFeature(network, FeaType.fo_bigram_word.name() + relPos, output, prevWord + " " + word + " " + prevLabel));
				bgf.add(this._param_g.toFeature(network, FeaType.fo_bigram_tag.name() + relPos, output, prevTag + " " + tag + " " + prevLabel));
				bgf.add(this._param_g.toFeature(network, FeaType.fo_bigram_phrase.name() + relPos, output, prevPhrase + " " +phrase + " " + prevLabel));
			}
		}
		tran.add(this._param_g.toFeature(network, FeaType.transition.name(), currLabel, prevLabel));
		
		if (pos == inst.size() - 1 && (this.contextual || this.path)) {
			String[] seq = new String[inst.size()];
			seq[inst.size() - 1] = currLabelwD;
			int node_k = parent_k;
			NodeType currNodeType = NodeType.values()[paArr[2]];
			BaseNetwork net = (BaseNetwork)network;
			int dl = -1;
			int dr = -1;
			int childPos = pos;
			String relation = null;
			while (currNodeType != NodeType.LEAF) {
				if (dr < 0 && seq[childPos].startsWith("I-")) {
					dr = childPos;
				}
				if (seq[childPos].startsWith("B-")) {
					dl = childPos;
					relation = seq[childPos].substring(2).replaceAll(Config.REV_SUFF, "");
				}
				int child_k = net.getChildren(node_k)[0][0];
				int[] childArr = net.getNodeArray(child_k);
				childPos = childArr[0];
				seq[childPos] = this.labels.get(childArr[1]);
				node_k = child_k;
				currNodeType = NodeType.values()[childArr[2]];
			}
			if (dl >= 0 && dr < 0) {
				dr = dl;
			}
			if (seq[0].equals("")) throw new RuntimeException("not reach leaf yet?");
			if (dl >= 0 && dr >= 0) {
				int lb = dl - 1;
				int rb = dr + 1;
				String lbw = lb < 0 ? "START" + lb : sent.get(lb).getForm();
				String lbt = lb < 0 ? "START_TAG" + lb : sent.get(lb).getTag();
				int addK = rb - sent.length();
				String rbw = rb >= sent.length() ? "END" + addK : sent.get(rb).getForm();
				String rbt = rb >= sent.length() ? "END_TAG" + addK : sent.get(rb).getTag();
				if (this.contextual) {
					context.add(this._param_g.toFeature(network, FeaType.contextual.name() + "-lbw", relation, lbw));
					context.add(this._param_g.toFeature(network, FeaType.contextual.name() + "-lbt", relation, lbt));
					context.add(this._param_g.toFeature(network, FeaType.contextual.name() + "-rbw", relation, rbw));
					context.add(this._param_g.toFeature(network, FeaType.contextual.name() + "-rbt", relation, rbt));
				}
				
				StringBuilder arg1Path = new StringBuilder("");
				StringBuilder arg1TagPath = new StringBuilder("");
				boolean arg1OnLeft = isOnLeft(arg1Idx, dl, dr);
				int pathStart = arg1OnLeft ? arg1Idx + 1 : dr + 1;
				int pathEnd = arg1OnLeft ? dl : arg1Idx;
				for (int i = pathStart; i < pathEnd; i++) {
					if (i == pathStart) {
						arg1Path.append(sent.get(i).getForm());
						arg1TagPath.append(sent.get(i).getTag());
					}
					else  {
						arg1Path.append(" " + sent.get(i).getForm());
						arg1TagPath.append(" " + sent.get(i).getTag());
					}
				}
				if (this.path) {
					path.add(this._param_g.toFeature(network,  FeaType.path.name() + "-a1w", relation, arg1Path.toString()));
					path.add(this._param_g.toFeature(network,  FeaType.path.name() + "-a1t", relation, arg1TagPath.toString()));
				}
				
				StringBuilder arg2Path = new StringBuilder("");
				StringBuilder arg2TagPath = new StringBuilder("");
				boolean arg2OnLeft = isOnLeft(arg2Idx, dl, dr);
				pathStart = arg2OnLeft ? arg2Idx + 1 : dr + 1;
				pathEnd = arg2OnLeft ? dl : arg2Idx;
				for (int i = pathStart; i < pathEnd; i++) {
					if (i == pathStart) {
						arg2Path.append(sent.get(i).getForm());
						arg2TagPath.append(sent.get(i).getTag());
					}
					else  {
						arg2Path.append(" " + sent.get(i).getForm());
						arg2TagPath.append(" " + sent.get(i).getTag());
					}
				}
				if (this.path) {
					path.add(this._param_g.toFeature(network,  FeaType.path.name() + "-a2w", relation, arg2Path.toString()));
					path.add(this._param_g.toFeature(network,  FeaType.path.name() + "-a2t", relation, arg2TagPath.toString()));
				}
				int minStart = Math.min(arg1Idx, arg2Idx);
				minStart = Math.min(minStart, dl);
				int maxEnd = Math.max(arg1Idx, arg2Idx);
				maxEnd = Math.max(maxEnd, dr);
				StringBuilder comb = new StringBuilder("");
				StringBuilder combTag = new StringBuilder("");
				for (int i = minStart; i <= maxEnd; i++) {
					if (i == dl) {
						comb.append(relation + " ");
						continue;
					} else if (i > dl && i <= dr) {continue;}
					comb.append(sent.get(i).getForm() + " ");
					combTag.append(sent.get(i).getTag()  + " ")  ;
				}
				if (this.path) {
					path.add(this._param_g.toFeature(network, FeaType.path.name() + "-w", relation, comb.toString()));
					path.add(this._param_g.toFeature(network, FeaType.path.name() + "-t", relation, combTag.toString()));
				}
			}
		}
		FeatureArray fa1 = this.createFeatureArray(network, ugf);
		FeatureArray fa2 = this.createFeatureArray(network, bgf);
		FeatureArray fa3 = this.createFeatureArray(network, tran);
		FeatureArray fa4 = this.createFeatureArray(network, context);
		FeatureArray fa5 = this.createFeatureArray(network, path);
		fa1.addNext(fa2).addNext(fa3).addNext(fa4).addNext(fa5);
		return fa1;
	}
	
	private boolean isOnLeft(int idx, int relLeft, int relRight) {
		if (idx < relLeft) {
			return true; 
		} else if ( idx > relRight) {
			return false;
		} else {
			System.out.println(idx + "," + relLeft + ", " + relRight);
			throw new RuntimeException("index is in between the two arguments?");
		}
	}

}
