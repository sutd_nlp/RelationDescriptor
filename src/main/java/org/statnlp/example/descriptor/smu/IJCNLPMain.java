package org.statnlp.example.descriptor.smu;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Random;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationDescriptorEvaluator;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.data.DataProcessor;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkModel;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class IJCNLPMain {
	
	public static double l2val = 0.01;
	public static int numThreads = 8;
	public static int numIteration = 1000;
	public static String dataFile = "data/ijcnlp/wikipedia.txt";
	public static int allNum = -1;
	public static double portion = 0.9;
	public static String resultFile = "results/nyt_res.txt";
	public static boolean saveModel = false;
	public static boolean readModel = false;
	public static String modelFile = "models/nyt_model.m";
	
	public static boolean contextualFeature = true;
	public static boolean pathFeature = true;
	public static boolean boundaryFeature = true;
	public static long randomSeed = 1234;

	@SuppressWarnings("unused")
	private static void debugMain (IJCNLPDataReader reader) throws InterruptedException, IOException {
		String dataFile = "data/ijcnlp/debug.txt";
		Instance[] trainData = reader.read(dataFile, true, -1);
		
		GlobalNetworkParam gnp = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
		SMUFeatureManager tfm = new SMUFeatureManager(gnp, contextualFeature, pathFeature, boundaryFeature);
		SMUNetworkCompiler tnc = new SMUNetworkCompiler(false);
		NetworkModel model = DiscriminativeNetworkModel.create(tfm, tnc);
		model.train(trainData, numIteration);
	}
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		setArgs(args);
		
		/***
		 * Parameter settings and model configuration
		 */
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2val;
		NetworkConfig.NUM_THREADS = numThreads;
		IJCNLPDataReader reader = new IJCNLPDataReader();
		
		/**debug info**/
//		saveModel = false;
//		readModel = false;
//		data = "wikipedia";
//		debugMain(reader);
//		System.exit(0);
		/***/

		
		Random rand = new Random(randomSeed);
		Instance[] allInstances = reader.read(dataFile, true, allNum);
		DataProcessor preprocessor = new DataProcessor();
		allInstances = preprocessor.shuffle(allInstances, rand);
		System.out.println("#Relations: " + RelationType.RELS.size());
		System.out.println("Relations: " + RelationType.RELS.toString());
		RelationType.lock();
		
		int numFolds = 10;
		Instance[][] splits = preprocessor.splitIntoKFolds(allInstances, numFolds);
		
		double[][] sumMetric = new double[2][4];
		for (int k = 0; k < numFolds; k++) {
			Instance[] testData = splits[numFolds - k - 1];
			for(Instance inst : testData) {
				inst.setUnlabeled();
			}
			Instance[] trainData = new Instance[allInstances.length - testData.length];
			int idx = 0;
			for (int t = 0; t < numFolds; t++) {
				if (t == numFolds - k - 1) continue;
				for (int i = 0; i < splits[t].length; i++) {
					splits[t][i].setLabeled();
					splits[t][i].setPrediction(null);
					trainData[idx++] = splits[t][i];
				}
			}
			NetworkModel model = null;
			if (readModel) {
				System.out.println("[Info] Reading Model....");
				ObjectInputStream in = RAWF.objectReader(modelFile + "."+k);
				model = (NetworkModel)in.readObject();
			} else {
				GlobalNetworkParam gnp = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
				SMUFeatureManager tfm = new SMUFeatureManager(gnp, contextualFeature, pathFeature, boundaryFeature);
				SMUNetworkCompiler tnc = new SMUNetworkCompiler(false);
				model = DiscriminativeNetworkModel.create(tfm, tnc);
				model.train(trainData, numIteration);
			}
			if (saveModel) {
				ObjectOutputStream out = RAWF.objectWriter(modelFile + "."+k);
				out.writeObject(model);
				out.close();
			}
			
			/**
			 * Testing Phase
			 */
			Instance[] results = model.decode(testData);
			RelationDescriptorEvaluator evaluator = new RelationDescriptorEvaluator();
			evaluator.evaluateRelation(results);
			double[][] metrics = evaluator.evaluateOverlapAndExactMatchDescriptor(results);
			for (int r = 0; r < metrics.length; r++) {
				for (int c = 0; c < metrics[r].length; c++)
					sumMetric[r][c] += metrics[r][c];
			}
			
			//print the results
			PrintWriter pw = RAWF.writer(resultFile+"."+k);
			for (Instance res : results) {
				RelInstance inst = (RelInstance)res;
				Sentence sent = inst.getInput().sent;
				RelationDescriptor gold = inst.getOutput();
				RelationDescriptor pred = inst.getPrediction();
				for (int i = 0; i < inst.size(); i++) {
					String goldLabel = i >= gold.getLeft() && i <= gold.getRight() ? 
							i == gold.getLeft() ? "B-R" : "I-R" : "O";
					String predLabel = i >= pred.getLeft() && i <= pred.getRight() ? 
							i == pred.getLeft() ? "B-R" : "I-R" : "O";
					pw.println(sent.get(i).getForm() + "\t" + goldLabel + "\t" + predLabel);
				}
			}
			pw.close();
		}
		for (int r = 0; r < sumMetric.length; r++) {
			for (int c = 0; c < sumMetric[r].length; c++)
				sumMetric[r][c] /= 10;
		}
		System.out.printf("[Result Overlap Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", sumMetric[0][0], sumMetric[0][1], sumMetric[0][2], sumMetric[0][3]);
		System.out.printf("[Result Exact Match] Acc.: %.2f%%, Prec: %.2f%%, Rec.: %.2f%%, F1.: %.2f%%\n", sumMetric[1][0], sumMetric[1][1], sumMetric[1][2], sumMetric[1][3]);
		
		
		
	}
	
	private static void setArgs(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Logistic Regression Model for Relation Extraction");
		parser.addArgument("-t", "--thread").setDefault(8).help("number of threads");
		parser.addArgument("--l2").setDefault(l2val).help("L2 Regularization");
		parser.addArgument("--iter").setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--data").setDefault(dataFile).help("The path of the data file");
		parser.addArgument("--allNum").setDefault(allNum).help("The number of all instances");
		parser.addArgument("--portion").setDefault(portion).help("The portion of data to be used for training");
		parser.addArgument("--saveModel").setDefault(saveModel).help("whether to save the model");
		parser.addArgument("--readModel").setDefault(readModel).help("whether to read the model");
		parser.addArgument("--modelFile").setDefault(modelFile).help("specify the model file");
		parser.addArgument("--resFile").setDefault(resultFile).help("specify the result file");
		parser.addArgument("--contextual").setDefault(contextualFeature).help("whether to use contextual features");
		parser.addArgument("--path").setDefault(pathFeature).help("whether to use path features");
		parser.addArgument("--boundary").setDefault(boundaryFeature).help("whether to use boundary features");
		parser.addArgument("--randomSeed").setDefault(randomSeed).help("the random seed");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = Integer.valueOf(ns.getString("thread"));
        l2val = Double.valueOf(ns.getString("l2"));
        numIteration = Integer.valueOf(ns.getString("iter"));
        dataFile = ns.getString("data");
        allNum = Integer.valueOf(ns.getString("allNum"));
        portion = Double.valueOf(ns.getString("portion"));
        saveModel = Boolean.valueOf(ns.getString("saveModel"));
        readModel = Boolean.valueOf(ns.getString("readModel")) ;
        modelFile = ns.getString("modelFile");
        resultFile = ns.getString("resFile");
        contextualFeature = Boolean.valueOf(ns.getString("contextual")) ;
        pathFeature = Boolean.valueOf(ns.getString("path")) ;
        boundaryFeature = Boolean.valueOf(ns.getString("boundary")) ;
        randomSeed = Long.valueOf(ns.getString("randomSeed"));
        System.err.println(ns.getAttrs().toString());
	}
}
