package org.statnlp.example.descriptor.smu;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.descriptor.CandidatePair;
import org.statnlp.example.descriptor.Config;
import org.statnlp.example.descriptor.RelInstance;
import org.statnlp.example.descriptor.RelationDescriptor;
import org.statnlp.example.descriptor.RelationType;
import org.statnlp.example.descriptor.Span;

public class IJCNLPDataReader {
	
	public IJCNLPDataReader() {
		
	}
	
	/**
	 * TODO: check whether arg1 is always on the left.
	 * @param file
	 * @param isTrainingInsts
	 * @param number
	 * @return
	 */
	public RelInstance[] read(String file, boolean isTrainingInsts, int number) {
		List<RelInstance> insts = new ArrayList<>();
		String line = null;
		int maxDescriptorLen = 0;
		int maxSentSize = 0;
		int numLeft = 0;
		int numRight = 0;
		int numBetween = 0;
		int maxLeftDist = 0; //index difference, largest from start
		int maxRightDist = 0;
		try {
			BufferedReader br = RAWF.reader(file);
			List<WordToken> wts = new ArrayList<WordToken>();
			List<Span> spans = new ArrayList<Span>();
			int arg1Idx = -1;
			int arg2Idx = -1;
			int instId = 1;
			int rdLeft = -1;
			int rdRight = -1;
			String relLabel = null;
			String prevRelLabel = "O";
			while((line = br.readLine()) != null) {
				if (line.startsWith("NYT-") || line.startsWith("Wikipedia-"))  continue; //sentence ID number
				if (line.equals("")) {
					Sentence sent = new Sentence(wts.toArray(new WordToken[wts.size()]));
					int leftSpanIdx = arg1Idx < arg2Idx ? arg1Idx : arg2Idx;
					int rightSpanIdx = arg1Idx < arg2Idx ? arg2Idx : arg1Idx;
					CandidatePair cp = new CandidatePair(sent, spans, leftSpanIdx, rightSpanIdx);
					maxSentSize = Math.max(maxSentSize, sent.length());
					RelationType relType = null;
					if (arg1Idx > arg2Idx) relType = RelationType.get("General" + Config.REV_SUFF);
					else relType = RelationType.get("General");
					if (rdLeft >= 0) {
						int leftMost = Math.min(arg1Idx, arg2Idx);
						int rightMost = Math.max(arg1Idx, arg2Idx);
						if (rdRight < leftMost) {
							numLeft++;
							maxLeftDist = Math.max(maxLeftDist, leftMost - rdLeft);
						} else if (rdLeft > rightMost) {
							numRight++;
							maxRightDist = Math.max(maxRightDist, rdRight - rightMost);
						} else {
							numBetween++;
						}
					}
//					if (rdLeft < 0 || rdRight < 0) {
//						System.out.println(sent.toString());
//						System.out.println("Descriptor span: " + rdLeft + ", " + rdRight);
//						throw new RuntimeException("descriptor length have negative boundary?");
//					}
					RelationDescriptor output = new RelationDescriptor(relType, rdLeft, rdRight);
					maxDescriptorLen = Math.max(rdRight - rdLeft + 1, maxDescriptorLen);
					RelInstance inst = new RelInstance(instId, 1.0, cp, output);
					insts.add(inst);
					if (insts.size() == number) {
						break;
					}
					wts.clear();
					spans.clear();
					arg1Idx = -1;
					arg2Idx = -1;
					rdLeft = -1;
					rdRight = -1;
					prevRelLabel = "O";
					instId++;
				} else {
					String[] vals = line.split("\\t");
					int idx = Integer.valueOf(vals[0]);
					String word = vals[1];
					String posTag = vals[2];
					String phraseTag = vals[3];
					relLabel = vals[4];
					WordToken wt = new WordToken(word, posTag);
					wt.setPhraseTag(phraseTag);
					if (word.equals("arg1")) {
						arg1Idx = idx;
					} else if (word.equals("arg2")) {
						arg2Idx = idx;
					}
					if (relLabel.equals("B-R")) {
						rdLeft = idx;
					} 
					if (relLabel.equals("O") && !prevRelLabel.equals("O")) {
						rdRight = idx - 1; //means previous is endding.
					}
					wts.add(wt);
					prevRelLabel = relLabel;
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("[Info] #Total Instances: " + insts.size());
		System.out.println("[Info] Maximum sentence length: " + maxSentSize);
		System.out.println("[Info] Maximum descriptor length: " + maxDescriptorLen);
		System.out.println("[Info] num des. left: " + numLeft);
		System.out.println("[Info] num des. right: " + numRight);
		System.out.println("[Info] num des. bete: " + numBetween);
		
		System.out.println("[Info] max left diff: " + maxLeftDist);
		System.out.println("[Info] max right diff: " + maxRightDist);
		return insts.toArray(new RelInstance[insts.size()]);
	}
}
