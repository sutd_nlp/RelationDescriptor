load('aadata.mat')

figure1 = figure;
plot(xx(:,1), xx(:, 2),'MarkerFaceColor',[0 0.447058826684952 0.74117648601532],...
    'Marker','o','LineWidth',2)
ylim([0 3])
xlim([0 7])
xlabel('Distance between nominals','FontWeight','bold','FontSize',20)
ylabel('Avg. Description length','FontWeight','bold','FontSize',20)
names = {'0'; '1'; '2'; '3'; '4'; '5'; '6'; '\ge 7'};
set(gca,'XTickLabel',names)
set(gca,'FontSize',15,'FontWeight','bold','XTickLabel',...
    {'0','1','2','3','4','5','6','>=7'});

hold on;
load('yy.mat')

plot(yy(:,1), yy(:, 2),'MarkerFaceColor',[0.850980401039124 0.325490206480026 0.0980392172932625],...
    'Marker','diamond','LineWidth',2)
ylim([0 3])
xlim([0 7])
xlabel('Distance between nominals','FontWeight','bold','FontSize',20)
ylabel('Avg. Description length','FontWeight','bold','FontSize',20)
legend('High agreement subset', 'Medium agreement subset')

set(gca,'FontSize',15,'FontWeight','bold','XTickLabel',...
    {'0','1','2','3','4','5','6', '>=7'});
% text(7,0,'$\ge  \textbf{7}$','interpreter','latex')

