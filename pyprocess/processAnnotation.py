file = "../results/final_annotation.txt"

outputFile = "../results/standard_annotation.txt"

fout = open(outputFile, 'w')

fin = open(file, 'r')

# line = fin.readline()
# while line:
#     line = line.rstrip()
#     fout.write(line + "\n")
#     for i in range(3):
#         line = fin.readline().rstrip()
#         vals = line.split()
#         str = ""
#         for k in range(len(vals)):
#             if vals[k] == "0":
#                 str += "O" if k == 0 else " O"
#             else:
#                 str += "D" if k == 0 else " D"
#         fout.write(str + "\n")

with open(file, 'r') as fin:
    for line in fin:
        line = line.rstrip()
        fout.write(line + "\n")
        for i in range(3):
            line = fin.readline().rstrip()
            vals = line.split()
            str = ""
            prev = "O"
            for k in range(len(vals)):
                if vals[k] == "0":
                    str += "O" if k == 0 else " O"
                    prev = "O"
                else:
                    if prev == "O":
                        str += "B-D" if k == 0 else " B-D"
                        prev = "B-D"
                    elif prev.startswith("B") or prev.startswith("I"):
                        str += " I-D"
                        prev = "I-D"
                    else:
                        raise Exception('previous is ' + prev)


            fout.write(str + "\n")
        fin.readline()
        fout.write("\n")

fout.close()