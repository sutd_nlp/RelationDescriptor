#!/bin/bash


DATA_PREFIX=/home/allan/data/descriptor/

#docs=(wikipedia.txt nyt.txt)
docs=(wikipedia.txt)
l2=(0.01)
seeds=(1234)

for i in ${docs[@]}; do
  doc=${i}
  dataFile=${DATA_PREFIX}${doc}
  echo $dataFile
  for j in ${seeds[@]}; do
    java -cp relation-descriptor-0.1.jar org.statnlp.example.descriptor.smu.IJCNLPLinearMain  --data ${dataFile} --l2 0.01 -t 40 --iter 4000 --saveModel false --readModel false --modelFile models/${doc}-1.m --resFile results/${doc}_res-1.txt --contextual false --path false --randomSeed ${j} > logs/${doc}-m-1-seed${j}.log 2>&1
    java -cp relation-descriptor-0.1.jar org.statnlp.example.descriptor.smu.IJCNLPLinearMain  --data ${dataFile} --l2 0.01 -t 40 --iter 4000 --saveModel false --readModel false --modelFile models/${doc}-2.m --resFile results/${doc}_res-2.txt --contextual true --path false --randomSeed ${j} > logs/${doc}-m-2-seed${j}.log 2>&1
    java -cp relation-descriptor-0.1.jar org.statnlp.example.descriptor.smu.IJCNLPLinearMain  --data ${dataFile} --l2 0.01 -t 40 --iter 4000 --saveModel false --readModel false --modelFile models/${doc}-3.m --resFile results/${doc}_res-3.txt--contextual true --path true --randomSeed ${j} > logs/${doc}-m-3-seed${j}.log 2>&1
  done
done




