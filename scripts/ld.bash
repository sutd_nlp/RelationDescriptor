#!/bin/bash


thread=40
l2vals=(0.0001 0.0005 0.001 0.005 0.01 0.05 0.1)
iter=4000
trainNum=-1
testNum=-1
saveModel=false
readModel=false
useDev=true
jarFile=relation-descriptor-0.1.jar
mainClass=org.statnlp.example.descriptor.linear.LDLinearMain
latentNeighbor=0

for (( i=0; i<${#l2vals[@]}; i++  ))
  l2=${l2vals[$i]}
  logFile=ldlogs/ld_ln${latentNeighbor}_dev${useDev}_l2_${l2}.log
  java -cp $jarFile $mainClass -t ${thread} --l2 ${l2} --iter ${iter} \
      --trainNum ${trainNum} --testNum ${testNum} --saveModel ${saveModel} \
      --readModel ${readModel} --useDev ${useDev} --latentNeighbor ${latentNeighbor} > ${logFile} 2>&1 
done