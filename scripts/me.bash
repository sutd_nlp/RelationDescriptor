#!/bin/bash

jarFile=relation-descriptor-0.1.jar
mainClass=org.statnlp.example.descriptor.me.SemEvalMEMain
thread=40
l2vals=( 0.01)
trainNum=-1
testNum=-1
iteration=4000
saveModel=false
readModel=false
embedding=google
useEmbFeat=true

for (( i=0; i<${#l2vals[@]}; i++)) do
	l2=${l2vals[$i]}
	logFile=melogs/me_l2v${l2}_embF_${useEmbFeat}_${embedding}.log
	java -cp ${jarFile} ${mainClass} --thread ${thread} --l2 ${l2} --trainNum ${trainNum} --testNum ${testNum} \
		 --saveModel ${saveModel} --readModel ${readModel}  --iter ${iteration} \
		 --useEmbFeat ${useEmbFeat} --embedding ${embedding} > ${logFile} 2>&1 &

done

