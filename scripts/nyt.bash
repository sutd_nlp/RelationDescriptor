#!/bin/bash

jarFile=relation-descriptor-0.1.jar
mainClass=org.statnlp.example.descriptor.nytwiki.NWMain
thread=40
l2vals=(0.0001 0.01 0.1 0.05 0.005 0.5)
allNum=-1
iteration=4000
saveModel=false
readModel=false
contextual=true
path=true
latentSpan=false
inbetween=false
leftrightbound=9
fixRelation=true
sources=(nyt wiki all)

for (( i=0; i<${#l2vals[@]}; i++)) do
	l2=${l2vals[$i]}
	for (( s=0; s<${#sources[@]}; s++)) do
		ds=${sources[$s]}
		logFile=nytlogs/${ds}_fixrel_${fixRelation}_lrb_${leftrightbound}_latentSpan_${latentSpan}_l2v${l2}.log
		java -cp ${jarFile} ${mainClass} --thread ${thread} --l2 ${l2} --allNum ${allNum} --saveModel ${saveModel} \
        	 --readModel ${readModel} --contextual ${contextual} --path ${path} -ls ${latentSpan} -ib ${inbetween} \
         	-lrb ${leftrightbound} -fr ${fixRelation} --source ${ds} --iter ${iteration} > ${logFile} 2>&1 &
    done
done

