#!/bin/bash


DATA_PREFIX=data/semeval/

## Best settings
## between args, not cover args
## parse tags and not general
## revlabel is true

l2val=0
trainNum=-1
testNum=-1
numThreads=40
iterations=(3200)
saveModel=false
readModel=false
trainFile=data/semeval/sd_parseTag_train.txt
testFile=data/semeval/sd_parseTag_test.txt
neural=true
os=linux
embedding=turian
jarFile=relation-descriptor-0.1.jar
mainClass=org.statnlp.example.descriptor.semeval.SemEvalLRMain
revLabel=true  ## true gives better performance
enableda=true
dabet=true
dacov=false
gtags=false
nn=rnnpool
retrofit=false
logFile=logs/semeval_lr_embedding_onlye.log
hiddenSizes=(0)
secondHiddenSizes=(0)
depf=false
regNeural=true
optimizer=sgd
learningRate=0.01
evalDev=true
evalKs=(20)
useBatch=true
batchSizes=(100)
dropouts=(0.0)
gpuid=0
reldiscrete=false
descriptorFeat=false
windowSize=3
lowercase=false
zerodigit=false
usePosEmbedding=false
posEmbeddingSize=50
randomBatch=false
nndf=false
ghs=(500)
sw=false
simplerel=false
fixEmbedding=false
wordnetfeat=false
headword=false
epochLims=(8)
tanhGRU=false
gruDropout=0.0
betSent=false
pi=true
embDropout=0.0
add=true
expTimes=1
saveFeatWeights=false
loadFeatWeights=false
fixFeatWeights=false

for (( t=1; t<=${expTimes}; t++ )) do
 for (( h=0; h<${#hiddenSizes[@]}; h++  ))
 do
  ##for h in ${hiddenSizes[@]}; do
  h1=${hiddenSizes[$h]}
  h2=${secondHiddenSizes[$h]}
  for (( b=0; b<${#batchSizes[@]}; b++ )) do
  batchSize=${batchSizes[$b]}
  iteration=${iterations[$b]}
  evalK=${evalKs[$b]}
  epochLim=${epochLims[$b]}
   for dropout in ${dropouts[@]}; do
    for gh in ${ghs[@]}; do
     logFile=logs/lr_${nn}_${neural}_${gh}_${gruDropout}_h1_${h1}_h2_${h2}_${optimizer}_do_${dropout}_nndf_${nndf}_binDesc_${descriptorFeat}_rel_${reldiscrete}_${simplerel}_${embedding}_fE_${fixEmbedding}_${embDropout}_tanhGRU_${tanhGRU}_pi_${pi}_add_${add}_batch_${batchSize}_${t}.log
     java -cp ${jarFile} ${mainClass}  --l2 ${l2val} -t ${numThreads} --iter ${iteration} --trainFile ${trainFile}\
       --saveModel ${saveModel} --readModel ${readModel}  --testFile ${testFile} --testNum ${testNum} --gruDropout ${gruDropout}\
       --neural ${neural} --os ${os} --embedding ${embedding} --gtags ${gtags} --nn ${nn} -depf ${depf} --epochLim ${epochLim} \
       --trainNum ${trainNum} --retrofit ${retrofit} --hidden ${h1} -nndf ${nndf} -gh ${gh} -sw ${sw} -simplerel ${simplerel}\
       --revLabel ${revLabel}  --enableda ${enableda} --dabet ${dabet} --dacov ${dacov} --regNeural ${regNeural} --tanhGRU ${tanhGRU} \
       --dropout ${dropout} --optimizer ${optimizer} ${learningRate} --evalDev ${evalDev} --evalK ${evalK} -fe ${fixEmbedding} \
       --useBatch ${useBatch} --batchSize ${batchSize} --gpuid ${gpuid} -reldiscrete ${reldiscrete} --descritorFeat ${descriptorFeat} \
       --windowSize ${windowSize} --secondHidden ${h2} --lowercase ${lowercase} --zerodigit ${zerodigit} --headword ${headword} \
       --usePosEmbedding ${usePosEmbedding} ${posEmbeddingSize} --randomBatch ${randomBatch} -wnf ${wordnetfeat} \
       --betSent ${betSent} -pi ${pi} -ed ${embDropout} --add ${add} --saveFeatWeights ${saveFeatWeights} \
       --loadFeatWeights ${loadFeatWeights} --fixFeatWeights ${fixFeatWeights}> ${logFile}  2>&1 &
    done
   done
  done
 done
done

