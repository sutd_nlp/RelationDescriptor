### Guidelines to run to code

Run the following command to see the "help" information: 
```
java -cp descriptor.jar:lib/* org.acl.example.descriptor.linear.LDLinearMain -h 
```
You can see various configuration.

### Training
To train our joint model with latent variable (`l=1`), simply run:

```
java -cp descriptor.jar:lib/* org.acl.example.descriptor.linear.LDLinearMain --l2 0.5 --iter 10000 --latentNeighbor 1  --saveModel true --modelFile models/lds_semeval_model.m -t 8
```
You can obtain the exactly same results as in the paper.

### Tuning the global feature
By loading the saved model you can tune the weight of global feature. 
```
java -cp descriptor.jar:lib/* org.acl.example.descriptor.linear.LDLinearMain --readModel true --useDev true --latentNeighbor 1 -pn true --penalty 1.2 --modelFile models/lds_semeval_model.m -t 8
```

### Testing
By testing the saved model also with the tuned feature weight, simply run:
```
java -cp descriptor.jar:lib/* org.acl.example.descriptor.linear.LDLinearMain --readModel true --useDev false --latentNeighbor 1 -pn true --penalty 1.2 --modelFile models/lds_semeval_model.m -t 8
```


### Annotation File
Annotation file is presented in "description_annotation.txt". 
Each 4 lines represent the followings:
1. Sentence represented by words separated by space
2. Annotation by the first annotator
3. Annotation by the second annotator
4. Annotation by the third annotator
