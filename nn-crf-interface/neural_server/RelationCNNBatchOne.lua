local RelationCNNBatchOne, parent = torch.class('RelationCNNBatchOne', 'AbstractNeuralNetwork')

function RelationCNNBatchOne:__init(doOptimization, gpuid)
    parent.__init(self, doOptimization)
    self.data = {}
    self.gpuid = gpuid
end

function RelationCNNBatchOne:defineGlobalString()
    self.padToken = "<PAD>"
    self.unkToken = "<UNK>"
    self.startToken = "<START>"
    self.endToken = "<END>"
end

function RelationCNNBatchOne:initialize(javadata, ...)
    self.data = {}
    self:defineGlobalString()
    local data = self.data
    data.sentences = listToTable(javadata:get("nnInputs"))
    data.embeddingSize = javadata:get("embeddingSize")
    data.windowSize = javadata:get("windowSize")
    data.hiddenSize = javadata:get("hiddenSize")
    data.layer2hiddenSize = javadata:get("layer2hiddenSize")
    data.optimizer = javadata:get("optimizer")
    data.learningRate = javadata:get("learningRate")
    data.clipping = javadata:get("clipping")
    data.positionEmbedding = javadata:get("positionEmbedding")
    self.numLabels = javadata:get("numLabels")
    data.embedding = javadata:get("embedding")
    self.fixEmbedding = javadata:get("fixEmbedding")
    self.dropout = javadata:get("dropout")
    local modelPath = javadata:get("nnModelFile")
    local isTraining = javadata:get("isTraining")
    data.isTraining = isTraining

    if isTraining then
        self.x = self:prepare_input()
        self.numSent = #data.sentences
    end

    if self.net == nil and isTraining then
        -- means is initialized process and we don't have the input yet.
        self:createNetwork()
        if self.fixEmbedding then
            print(self.lt)
        end
        print(self.net)
    end
    if self.net == nil then 
        self:load_model(modelPath)
    end


    if not isTraining then 
        self.testInput = self:prepare_input()
    end
    self.output = torch.Tensor()
    self.gradOutput = torch.Tensor()
    local outputAndGradOutputPtr = {... }
    if #outputAndGradOutputPtr > 0 then
        self.outputPtr = torch.pushudata(outputAndGradOutputPtr[1], "torch.DoubleTensor")
        self.gradOutputPtr = torch.pushudata(outputAndGradOutputPtr[2], "torch.DoubleTensor")
        return self:obtainParams()
    end
end

--The network is only created once is used.
function RelationCNNBatchOne:createNetwork()
    local data = self.data
    local embeddingSize = data.embeddingSize
    local hiddenSize = data.hiddenSize
    local layer2hiddenSize = data.layer2hiddenSize
    local cnnWindowSize = data.windowSize
    local sharedLookupTable
    if data.embedding ~= nil then
        if data.embedding == 'glove' then
            sharedLookupTable = loadGlove(self.idx2word, embeddingSize, true)
        elseif data.embedding == 'google' then
            sharedLookupTable = loadGoogle(self.idx2word, embeddingSize, true)
        elseif data.embedding == 'senna' then
            sharedLookupTable = loadSenna(self.idx2word, self.padToken)
            embeddingSize = 50
        elseif data.embedding == 'turian' then
            sharedLookupTable = loadTurian(self.idx2word, self.padToken, self.startToken, self.endToken)
            embeddingSize = 50
        else -- unknown/no embedding, defaults to random init
            print ("unknown embedding type, use random embedding..")
            sharedLookupTable = nn.LookupTableMaskZero(self.vocabSize, embeddingSize)
            print("lookup table parameter: ".. sharedLookupTable:getParameters():nElement())
        end
    else
        print ("Not using any embedding, just use random embedding")
        sharedLookupTable = nn.LookupTableMaskZero(self.vocabSize, embeddingSize)
    end
    if self.fixEmbedding then 
        sharedLookupTable.accGradParameters = function() end
    end
    self.lt = sharedLookupTable
    print("Embedding layer: " .. self.lt.weight:size(1) .. " x " .. self.lt.weight:size(2))
    local conv = nn.Sequential()
    if not self.fixEmbedding then
        conv:add(self.lt)
        conv:add(nn.Dropout(self.dropout))
    end

    conv:add(nn.TemporalConvolution(embeddingSize, hiddenSize, cnnWindowSize, 1))
    --conv:add(nn.Tanh()):add(nn.Max(2))  --max over the second dimension
    conv:add(nn.Max(1))
    conv:add(nn.Linear(hiddenSize, layer2hiddenSize):noBias())
    conv:add(nn.Tanh())
    conv:add(nn.Linear(layer2hiddenSize, self.numLabels):noBias())
    self.net = conv
    if self.gpuid >= 0 then 
        if self.fixEmbedding then
            self.lt:cuda()
        end
        self.net:cuda() 
    end
end

function RelationCNNBatchOne:obtainParams()
    --make sure we will not replace this variable
    self.params, self.gradParams = self.net:getParameters()
    print("Number of parameters: " .. self.params:nElement())
    if self.doOptimization then
        self:createOptimizer()
        -- no return array if optim is done here
    else
        if self.gpuid >= 0 then
            -- since the the network is gpu network.
            self.paramsDouble = self.params:double()
            self.paramsDouble:retain()
            self.paramsPtr = torch.pointer(self.paramsDouble)
            self.gradParamsDouble = self.gradParams:double()
            self.gradParamsDouble:retain()
            self.gradParamsPtr = torch.pointer(self.gradParamsDouble)
            return self.paramsPtr, self.gradParamsPtr
        else
            self.params:retain()
            self.paramsPtr = torch.pointer(self.params)
            self.gradParams:retain()
            self.gradParamsPtr = torch.pointer(self.gradParams)
            return self.paramsPtr, self.gradParamsPtr
        end
    end
end


function RelationCNNBatchOne:forward(isTraining, batchInputIds)
    if self.gpuid >= 0 and not self.doOptimization and isTraining then
        self.params:copy(self.paramsDouble:cuda())
    end
    local nnInput = self:getForwardInput(isTraining, batchInputIds)
    local nnInput_x = nnInput
    if self.fixEmbedding then
        if type(nnInput) == "table" then
            nnInput_x = {}
            for i=1,#nnInput do
                nnInput_x[i] = self.lt:forward(nnInput[i])
            end
        else 
            nnInput_x =  self.lt:forward(nnInput)
        end
    end
    local cnnOutput
    if type(nnInput_x) == "table" then --because testing might contain more inputs
        for i = 1, #nnInput_x do
            local out = self.net:forward(nnInput_x[i])
            if cnnOutput == nil then
                cnnOutput = out
            else
                cnnOutput = torch.cat(cnnOutput, out, 1)
            end
        end
    else
        --training
        --print(self.net:get(1):forward(nnInput_x))
        cnnOutput = self.net:forward(nnInput_x)
    end
    if self.gpuid >= 0 then
        cnnOutput = cnnOutput:double()
    end 
    self.output = cnnOutput
    if not self.outputPtr:isSameSizeAs(self.output) then
        self.outputPtr:resizeAs(self.output)
    end
    self.outputPtr:copy(self.output)
end

function RelationCNNBatchOne:getForwardInput(isTraining, batchInputIds)
    if isTraining then
        batchInputIds:add(1) -- because the sentence is 0 indexed.
        self.batchInputIds = batchInputIds
        self.batchInput = self.x[batchInputIds[1]]
        if batchInputIds:size(1) ~= 1 then
            print("batch is not 1?")
        end
        return self.batchInput
    else
        return self.testInput
    end
end

function RelationCNNBatchOne:backward()
    self.gradParams:zero()
    local gradOutputTensor = self.gradOutputPtr
    local backwardInput = self.batchInput --since backward only happen in training
    local backwardSentNum = 1
    self.gradOutput = gradOutputTensor
    if self.gpuid >= 0 then
        self.gradOutput = self.gradOutput:cuda()
    end
    local nnInput_x = backwardInput
    if self.fixEmbedding then
        nnInput_x =  self.lt:forward(backwardInput)
    end
    self.net:backward(nnInput_x, self.gradOutput)

    if self.gpuid >= 0 then
        self.gradParamsDouble:copy(self.gradParams:double())
    end

end

function RelationCNNBatchOne:prepare_input()
    local data = self.data
    local positionEmbedding = data.positionEmbedding
    local sentences = data.sentences
    local sentence_toks = {}
    local maxLen = 0
    local paddingSentence = false
    for i=1,#sentences do
        local tokens = stringx.split(sentences[i]," ")
        table.insert(sentence_toks, tokens)
        if #tokens > maxLen then
            maxLen = #tokens
        end
    end

    --note that inside if the vocab is already created
    --just directly return
    self:buildVocab(sentences, sentence_toks)    
    local inputs = {}
    for sId=1,#sentences do
        local tokens = sentence_toks[sId]
        inputs[sId] = torch.LongTensor(#tokens)
        for step=1,#tokens do
            local tok = tokens[step]
            local tok_id = self.word2idx[tok]
            if tok_id == nil then
                tok_id = self.word2idx[self.unkToken]
            end
            inputs[sId][step] = tok_id
        end
        if self.gpuid >= 0 then inputs[sId] = inputs[sId]:cuda() end
    end
    print("number of sentences: "..#sentences)
    return inputs
end

function RelationCNNBatchOne:buildVocab(sentences, sentence_toks)
    if self.idx2word ~= nil then
        --means the vocabulary is already created
        return 
    end
    self.idx2word = {}
    self.word2idx = {}
    self.word2idx[self.padToken] = 0
    self.idx2word[0] = self.padToken
    self.word2idx[self.unkToken] = 1
    self.idx2word[1] = self.unkToken
    self.vocabSize = 2
    --special treatment for senna embedding
    --can ignore the first token
    if self.data.embedding == "senna" or self.data.embedding == "turian" then
        self.word2idx[self.padToken] = 2
        self.idx2word[2] = self.padToken
        self.vocabSize = 3
    end
    for i=1,#sentences do
        local tokens = sentence_toks[i]
        for j=1,#tokens do
            local tok = tokens[j]
            local tok_id = self.word2idx[tok]
            if tok_id == nil then
                self.word2idx[tok] = self.vocabSize
                self.idx2word[self.vocabSize] = tok
                self.vocabSize = self.vocabSize+1
            end
        end
    end
    --self:printTable(self.idx2word)
    print("number of unique words(including pad and unknown):" 
        .. #self.idx2word) --this won't count the 0-indexed
end


function RelationCNNBatchOne:save_model(path)
    --need to save the vocabulary as well.
    torch.save(path, {self.net, self.idx2word, self.word2idx})
end

function RelationCNNBatchOne:load_model(path)
    local object = torch.load(path)
    self.net = object[1]
    self.idx2word = object[2]
    self.word2idx = object[3]
end
