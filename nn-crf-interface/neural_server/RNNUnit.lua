------------------------------------------------------------------------
--[[ RNNUnit ]]--
-- Author: Jin-Hwa Kim
-- License: LICENSE.2nd.txt

-- Gated Recurrent Units architecture.
-- http://www.wildml.com/2015/10/recurrent-neural-network-tutorial-part-4-implementing-a-RNNUnitlstm-rnn-with-python-and-theano/
-- Expects 1D or 2D input.
-- The first input in sequence uses zero value for cell and hidden state
--
-- For p > 0, it becomes Bayesian RNNUnits [Moon et al., 2015; Gal, 2015].
-- In this case, please do not dropout on input as BRNNUnits handle the input with
-- its own dropouts. First, try 0.25 for p as Gal (2016) suggested, presumably,
-- because of summations of two parts in RNNUnits connections.
------------------------------------------------------------------------
local RNNUnit, parent = torch.class('nn.RNNUnit', 'nn.AbstractRecurrent')

function RNNUnit:__init(inputSize, outputSize, rho, p, mono)
   parent.__init(self, rho or 9999)
   self.p = p or 0
   if p and p ~= 0 then
      assert(nn.Dropout(p,false,false,true).lazy, 'only work with Lazy Dropout!')
   end
   self.mono = mono or false
   self.inputSize = inputSize
   self.outputSize = outputSize
   -- build the model
   self.recurrentModule = self:buildModel()
   -- make it work with nn.Container
   self.modules[1] = self.recurrentModule
   self.sharedClones[1] = self.recurrentModule

   -- for output(0), cell(0) and gradCell(T)
   self.zeroTensor = torch.Tensor()

   self.cells = {}
   self.gradCells = {}
end

-------------------------- factory methods -----------------------------
function RNNUnit:buildModel()
   -- input : {input, prevOutput}
   -- output : {output}

   -- Calculate all four gates in one go : input, hidden, forget, output
   local para = nn.ParallelTable()
   local i2g
   local o2g
   if self.p ~= 0 then
   		i2g = nn.Sequential():add(nn.Dropout(self.p,false,false,true,self.mono))
   				:add(nn.Linear(self.inputSize, self.outputSize))
   
   		o2g = nn.Sequential():add(nn.Dropout(self.p,false,false,true,self.mono))
   				:add(nn.LinearNoBias(self.outputSize, self.outputSize))
   else
   	   i2g = nn.Linear(self.inputSize, self.outputSize)
   	   o2g = nn.LinearNoBias(self.outputSize, self.outputSize)
   end
   para:add(i2g)
   para:add(o2g)
   local seq = nn.Sequential()
   seq:add(para)
   seq:add(nn.CAddTable())
   seq:add(nn.Tanh())
   return seq
end

function RNNUnit:getHiddenState(step, input)
   local prevOutput
   if step == 0 then
      prevOutput = self.userPrevOutput or self.outputs[step] or self.zeroTensor
      if input then
         if input:dim() == 2 then
            self.zeroTensor:resize(input:size(1), self.outputSize):zero()
         else
            self.zeroTensor:resize(self.outputSize):zero()
         end
      end
   else
      -- previous output and cell of this module
      prevOutput = self.outputs[step]
   end
   return prevOutput
end


function RNNUnit:setHiddenState(step, hiddenState)
   assert(torch.isTensor(hiddenState))
   self.outputs[step] = hiddenState
end

------------------------- forward backward -----------------------------
function RNNUnit:updateOutput(input)
   local prevOutput = self:getHiddenState(self.step-1, input)

   -- output(t) = RNNUnit{input(t), output(t-1)}
   local output
   if self.train ~= false then
      self:recycle()
      local recurrentModule = self:getStepModule(self.step)
      -- the actual forward propagation
      output = recurrentModule:updateOutput{input, prevOutput}
   else
      output = self.recurrentModule:updateOutput{input, prevOutput}
   end

   self.outputs[self.step] = output

   self.output = output

   self.step = self.step + 1
   self.gradPrevOutput = nil
   self.updateGradInputStep = nil
   self.accGradParametersStep = nil
   -- note that we don't return the cell, just the output
   return self.output
end


function RNNUnit:getGradHiddenState(step)
   local gradOutput
   if step == self.step-1 then
      gradOutput = self.userNextGradOutput or self.gradOutputs[step] or self.zeroTensor
   else
      gradOutput = self.gradOutputs[step]
   end
   return gradOutput
end

function RNNUnit:setGradHiddenState(step, gradHiddenState)
   assert(torch.isTensor(gradHiddenState))
   self.gradOutputs[step] = gradHiddenState
end

function RNNUnit:_updateGradInput(input, gradOutput)
   assert(self.step > 1, "expecting at least one updateOutput")
   local step = self.updateGradInputStep - 1
   assert(step >= 1)

   -- set the output/gradOutput states of current Module
   local recurrentModule = self:getStepModule(step)

   -- backward propagate through this step
   local _gradOutput = self:getGradHiddenState(step)
   assert(_gradOutput)
   self._gradOutputs[step] = nn.rnn.recursiveCopy(self._gradOutputs[step], _gradOutput)
   nn.rnn.recursiveAdd(self._gradOutputs[step], gradOutput)
   gradOutput = self._gradOutputs[step]

   local gradInputTable = recurrentModule:updateGradInput({input, self:getHiddenState(step-1)}, gradOutput)

   self:setGradHiddenState(step-1, gradInputTable[2])

   return gradInputTable[1]
end

function RNNUnit:_accGradParameters(input, gradOutput, scale)
   local step = self.accGradParametersStep - 1
   assert(step >= 1)

   -- set the output/gradOutput states of current Module
   local recurrentModule = self:getStepModule(step)

   -- backward propagate through this step
   local gradOutput = self._gradOutputs[step] or self:getGradHiddenState(step)
   recurrentModule:accGradParameters({input, self:getHiddenState(step-1)}, gradOutput, scale)
end

function RNNUnit:__tostring__()
   return string.format('%s(%d -> %d, %.2f)', torch.type(self), self.inputSize, self.outputSize, self.p)
end

-- migrate RNNUnits params to BRNNUnits params
function RNNUnit:migrate(params)
   error("not support migrate for RNN unit")
end