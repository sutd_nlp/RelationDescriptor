local SemEvalContinuousFeature, parent = torch.class('SemEvalContinuousFeature', 'AbstractNeuralNetwork')


function SemEvalContinuousFeature:__init(doOptimization, gpuid)
    parent.__init(self, doOptimization)
    self.data = {}
    self.gpuid = gpuid
end

function SemEvalContinuousFeature:initialize(javadata, ...)
    local gpuid = self.gpuid

    self.data = {}
    local data = self.data
    self.numLabels = javadata:get("numLabels")
    self.hiddenSize = javadata:get("hiddenSize")
    self.dropout = javadata:get("dropout")
    self.numValues = javadata:get("numValues")
    local isTraining = javadata:get("isTraining")
    self.layer2hiddenSize = javadata:get("layer2hiddenSize")
    self.isTraining = isTraining
    local outputAndGradOutputPtr = {...}
    if #outputAndGradOutputPtr > 0 then  
        self.outputPtr = torch.pushudata(outputAndGradOutputPtr[1], "torch.DoubleTensor")
        self.gradOutputPtr = torch.pushudata(outputAndGradOutputPtr[2], "torch.DoubleTensor")
    end
    self.output = torch.Tensor()
    self.gradOutput = {}
    if isTraining then
        self.x = array2Tensor2D(javadata:get("nnInputs"))
        self:createNetwork()
        print(self.net)
        self.params, self.gradParams = self.net:getParameters()
        print("Number of parameters: " .. self.params:nElement())
        self.params:retain()
        self.paramsPtr = torch.pointer(self.params)
        self.gradParams:retain()
        self.gradParamsPtr = torch.pointer(self.gradParams)
        return self.paramsPtr, self.gradParamsPtr
    else
        self.testInput = array2Tensor2D(javadata:get("nnInputs"))
    end
end


function SemEvalContinuousFeature:createNetwork()
    local fwd = nn.Sequential()
    local lastLayerUnit = self.numValues
    if self.hiddenSize ~= nil and self.hiddenSize > 0 then
        fwd:add(nn.Linear(self.numValues, self.hiddenSize))
        fwd:add(nn.Tanh())
        fwd:add(nn.Dropout(self.dropout))
        lastLayerUnit = self.hiddenSize
        if self.layer2hiddenSize ~= nil and self.layer2hiddenSize > 0 then
            fwd:add(nn.Linear(lastLayerUnit, self.layer2hiddenSize))
            fwd:add(nn.Tanh())
            fwd:add(nn.Dropout(self.dropout))
            lastLayerUnit = self.layer2hiddenSize
        end
    end
    fwd:add(nn.Linear(lastLayerUnit, self.numLabels):noBias())
    self.net = fwd
end



function SemEvalContinuousFeature:forward(isTraining, batchInputIds)
    self.isTraining = isTraining
    if self.isTraining then
        self.net:training()
    else
        self.net:evaluate()
    end
    self.output = self.net:forward(self:getInput(isTraining, batchInputIds))
    if not self.outputPtr:isSameSizeAs(self.output) then
        self.outputPtr:resizeAs(self.output)
    end
    self.outputPtr:copy(self.output)
end

function SemEvalContinuousFeature:backward()
    self.gradParams:zero()
    self.gradOutput = self.gradOutputPtr
    self.net:backward(self:getBackwardInput(), self.gradOutput)
end

function SemEvalContinuousFeature:getBackwardInput()
    if self.batchInputIds ~= nil then
        return self.batchInput
    else
        return self.x
    end
end

function SemEvalContinuousFeature:getInput(isTraining)
    if isTraining then
        if batchInputIds ~= nil then
            batchInputIds:add(1)
            self.batchInputIds = batchInputIds
            self.batchInput = self.x:index(1, batchInputIds)
            return self.batchInput
        else
            return self.x
        end
    else
        return self.testInput
    end
end
