local opt = {}
wd='/Users/allanjie/allan/data/'
opt.binfilename = wd .. 'senna_embeddings.txt'
opt.outfilename = wd .. 'senna_embeddings.t7'

--Reading Header

local encodingsize = -1
local ctr = 0
for line in io.lines(opt.binfilename) do
    if ctr == 0 then
        for i in string.gmatch(line, "%S+") do
            encodingsize = encodingsize + 1
        end
    end
    ctr = ctr + 1

end

words = ctr
size = encodingsize




local w2vvocab = {}
local v2wvocab = {}
local M = torch.FloatTensor(words,size)

--Reading Contents

i = 1
for line in io.lines(opt.binfilename) do
    xlua.progress(i,words)
    local vecrep = {}
    for i in string.gmatch(line, "%S+") do
        table.insert(vecrep, i)
    end
    str = vecrep[1]
    table.remove(vecrep,1)
	vecrep = torch.FloatTensor(vecrep)

	--senna no need to calculate the norm
	--local norm = torch.norm(vecrep,2)
	--if norm ~= 0 then vecrep:div(norm) end
	w2vvocab[str] = i
	v2wvocab[i] = str
	M[{{i},{}}] = vecrep
    i = i + 1
end


--Writing Files
Senna = {}
Senna.M = M
Senna.w2vvocab = w2vvocab
Senna.v2wvocab = v2wvocab
torch.save(opt.outfilename,Senna)
print('Writing t7 File for future usage.')



return Senna


