torch.setdefaulttensortype('torch.DoubleTensor')

-- opt = {
--     binfilename = 'Senna_torch/Senna.6B.50d.txt',
--     outfilename = 'Senna_torch/Senna.6B.50d.t7'
-- }
-- second workstation
local embeddingFile = '/Users/allanjie/allan/data/senna_embeddings.t7'
local Senna = {}
-- if not paths.filep(opt.outfilename) then
-- 	Senna = require('bintot7.lua')
-- else
-- 	Senna = torch.load(opt.outfilename)
-- 	print('Done reading Senna data.')
-- end

Senna.load = function (self)
	local SennaFile = embeddingFile
    --local SennaFile = 'nn-crf-interface/neural_server/goolge/SennaNews-vectors-negative300.bin.t7'
    if not paths.filep(SennaFile) then
        error('Please run bintot7.lua to preprocess Senna data!')
    else
        Senna.Senna = torch.load(SennaFile)
        print('Done reading Senna data.')
    end
end

Senna.distance = function (self,vec,k)
	local k = k or 1	
	--self.zeros = self.zeros or torch.zeros(self.M:size(1));
	local norm = vec:norm(2)
	vec:div(norm)
	local distances = torch.mv(self.Senna.M ,vec)
	distances , oldindex = torch.sort(distances,1,true)
	local returnwords = {}
	local returndistances = {}
	for i = 1,k do
		table.insert(returnwords, self.Senna.v2wvocab[oldindex[i]])
		table.insert(returndistances, distances[i])
	end
	return {returndistances, returnwords}
end

Senna.word2vec = function (self,word,throwerror)
   local throwerror = throwerror or false
   local ind = self.Senna.w2vvocab[word]
   if throwerror then
		assert(ind ~= nil, 'Word does not exist in the dictionary!')
   end   
	if ind == nil then
		ind = self.Senna.w2vvocab['UNKNOWN']
	end
   return self.Senna.M[ind]
end

return Senna