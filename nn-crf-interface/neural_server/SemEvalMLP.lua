include 'SumEach.lua'

local SemEvalMLP, parent = torch.class('SemEvalMLP', 'AbstractNeuralNetwork')

--way to finish
function SemEvalMLP:__init(doOptimization, gpuid)
    parent.__init(self, doOptimization)
    self.data = {}
    self.gpuid = gpuid
end

function SemEvalMLP:defineGlobalString()
    self.padToken = "<PAD>"
    self.unkToken = "<UNK>"
    self.startToken = "<START>"
    self.endToken = "<END>"
end

function SemEvalMLP:initialize(javadata, ...)
    self.data = {}
    self:defineGlobalString()
    local data = self.data
    data.sentences = listToTable(javadata:get("nnInputs"))
    data.embeddingSize = javadata:get("embeddingSize")
    data.hiddenSize = javadata:get("hiddenSize")
    data.layer2hiddenSize = javadata:get("layer2hiddenSize")
    self.numLabels = javadata:get("numLabels")
    data.embedding = javadata:get("embedding")
    self.dropout = javadata:get("dropout")
    self.fixEmbedding = javadata:get("fixEmbedding")
    self.nnDescriptorFeature = javadata:get("nnDescriptorFeature")
    self.bilinear = javadata:get("bilinear")
    local modelPath = javadata:get("nnModelFile")
    local isTraining = javadata:get("isTraining")
    data.isTraining = isTraining
    local testVocabFile = javadata:get("testVocab")
    if isTraining and testVocabFile ~= nil then
        self.testVocab = readVocabs(testVocabFile)
    end

    if isTraining then
        self:loadEmbObj()
        self.x = self:prepare_input(isTraining)
        self.numSent = #data.sentences
    end

    if self.net == nil and isTraining then
        -- means is initialized process and we don't have the input yet.
        self:createNetwork()
        print(self.net)
    end

    if self.net == nil then 
        self:load_model(modelPath)
    end

    if not isTraining then 
        self.testInput = self:prepare_input(isTraining)
    end
    self.output = torch.Tensor()
    self.gradOutput = torch.Tensor()
    local outputAndGradOutputPtr = {... }
    if #outputAndGradOutputPtr > 0 then
        self.outputPtr = torch.pushudata(outputAndGradOutputPtr[1], "torch.DoubleTensor")
        self.gradOutputPtr = torch.pushudata(outputAndGradOutputPtr[2], "torch.DoubleTensor")
        return self:obtainParams()
    end
end

function SemEvalMLP:loadEmbObj()
    local data = self.data
    self.embeddingSize = data.embeddingSize
    if data.embedding == 'google' then
        self.embeddingObject = loadGoogleEmbObj()
        self.embeddingSize = 300
    elseif data.embedding == 'turian' then
        self.embeddingObject = loadTurianEmbObj()
        self.embeddingSize = 50
    else
        error('unknown embedding type: '.. data.embedding)
    end
end

--The network is only created once is used.
function SemEvalMLP:createNetwork()
    local data = self.data
    local hiddenSize = data.hiddenSize
    local embeddingSize = self.embeddingSize
    local layer2hiddenSize = data.layer2hiddenSize
    local sharedLookupTable = nn.LookupTableMaskZero(self.vocabSize, embeddingSize)
    sharedLookupTable.weight[1]:zero()
    for i =1, self.vocabSize do
        sharedLookupTable.weight[i+1] = self.embeddingObject:word2vec(self.idx2word[i])
    end
    self.lt = sharedLookupTable
    print("Word Embedding layer: " .. self.lt.weight:size(1) .. " x " .. self.lt.weight:size(2))
    
    local net = nn.Sequential()
    local outpt = nn.ParallelTable()
    local firstPT = nn.ParallelTable()
    local secondPT = nn.ParallelTable()
    local thirdPT = nn.ParallelTable()
    firstPT:add(self.lt):add(nn.Identity())
    self.lt2 = self.lt:sharedClone()
    secondPT:add(self.lt2):add(nn.Identity())
    self.lt3 = self.lt:sharedClone()
    thirdPT:add(self.lt3):add(nn.Identity())
    if self.fixEmbedding then
        self.lt.accGradParameters = function() end
        self.lt2.accGradParameters = function() end
        self.lt3.accGradParameters = function() end
        self.lt.parameters = function() end
        self.lt2.parameters = function() end
        self.lt3.parameters = function() end
    end
    local firstComp = nn.Sequential():add(firstPT):add(nn.SumEach(2, nil, 2))
    outpt:add(firstComp)
    local secondComp
    if self.nnDescriptorFeature then
        secondComp = nn.Sequential():add(secondPT):add(nn.SumEach(2, nil, 2))
        outpt:add(secondComp)
    end
    local thirdComp = nn.Sequential():add(thirdPT):add(nn.SumEach(2, nil, 2))
    outpt:add(thirdComp)
    net:add(outpt)

    if self.bilinear then
        if self.nnDescriptorFeature then error("cannot use bilinear with descriptor embeddings") end
        bilinear = nn.Bilinear(embeddingSize, embeddingSize, self.numLabels, false) --false: no bias
        net:add(bilinear)
    else
        net:add(nn.JoinTable(2))
        local currentSize = 2 * embeddingSize
        if self.nnDescriptorFeature then
            currentSize = 3 * embeddingSize
        end
        if hiddenSize ~= nil and hiddenSize > 0 then
            net:add(nn.Dropout(self.dropout))
            net:add(nn.Linear(currentSize, hiddenSize))
            net:add(nn.Tanh())
            currentSize = hiddenSize
            if layer2hiddenSize ~= nil and layer2hiddenSize > 0 then
               net:add(nn.Linear(currentSize, layer2hiddenSize))
                net:add(nn.Tanh())
                --net:add(nn.Dropout(self.dropout))
                currentSize = layer2hiddenSize
            end
        end
        net:add(nn.Linear(currentSize, self.numLabels):noBias())
    end
    self.net = net
    if self.gpuid >= 0 then
        self.net:cuda() 
    end
end

function SemEvalMLP:obtainParams()
    --make sure we will not replace this variable
    self.params, self.gradParams = self.net:getParameters()
    print("Number of parameters: " .. self.params:nElement())
    if self.doOptimization then
        self:createOptimizer()
        -- no return array if optim is done here
    else
        if self.gpuid >= 0 then
            -- since the the network is gpu network.
            self.paramsDouble = self.params:double()
            self.paramsDouble:retain()
            self.paramsPtr = torch.pointer(self.paramsDouble)
            self.gradParamsDouble = self.gradParams:double()
            self.gradParamsDouble:retain()
            self.gradParamsPtr = torch.pointer(self.gradParamsDouble)
            return self.paramsPtr, self.gradParamsPtr
        else
            self.params:retain()
            self.paramsPtr = torch.pointer(self.params)
            self.gradParams:retain()
            self.gradParamsPtr = torch.pointer(self.gradParams)
            return self.paramsPtr, self.gradParamsPtr
        end
    end
end

function SemEvalMLP:forward(isTraining, batchInputIds)
    if self.gpuid >= 0 and not self.doOptimization then
        self.params:copy(self.paramsDouble:cuda())
    end
    if isTraining then
        self.net:training()
    else
        self.net:evaluate()
    end
    local nnInput = self:getForwardInput(isTraining, batchInputIds)
    local cnnOutput = self.net:forward(nnInput)
    if self.gpuid >= 0 then
        cnnOutput = cnnOutput:double()
    end 
    self.output = cnnOutput
    if not self.outputPtr:isSameSizeAs(self.output) then
        self.outputPtr:resizeAs(self.output)
    end
    self.outputPtr:copy(self.output)
end

function SemEvalMLP:getForwardInput(isTraining, batchInputIds)
    if isTraining then
        if batchInputIds ~= nil then
            batchInputIds:add(1) -- because the sentence is 0 indexed.
            self.batchInputIds = batchInputIds
            if self.nnDescriptorFeature then
                self.batchInput = {{self.x[1][1]:index(1, batchInputIds), 
                                self.x[1][2]:index(1, batchInputIds)},  
                                {self.x[2][1]:index(1, batchInputIds), 
                                self.x[2][2]:index(1, batchInputIds)} ,
                                 {self.x[3][1]:index(1, batchInputIds),
                                self.x[3][2]:index(1, batchInputIds) }}
            else
                self.batchInput = {{self.x[1][1]:index(1, batchInputIds), 
                                self.x[1][2]:index(1, batchInputIds)},  
                                {self.x[2][1]:index(1, batchInputIds), 
                                self.x[2][2]:index(1, batchInputIds)}}
            end
            return self.batchInput
        else
            return self.x
        end
    else
        return self.testInput
    end
end

function SemEvalMLP:getBackwardInput()
    if self.batchInputIds ~= nil then
        return self.batchInput
    else
        return self.x
    end
end

function SemEvalMLP:backward()
    self.gradParams:zero()
    local gradOutputTensor = self.gradOutputPtr
    local backwardInput = self:getBackwardInput()  --since backward only happen in training
    self.gradOutput = gradOutputTensor
    if self.gpuid >= 0 then
        self.gradOutput = self.gradOutput:cuda()
    end
    self.net:training()
    self.net:backward(backwardInput, self.gradOutput)

    if self.gpuid >= 0 then
        self.gradParamsDouble:copy(self.gradParams:double())
    end
end

function SemEvalMLP:prepare_input()
    local data = self.data
    local positionEmbedding = self.positionEmbedding
    local sentences = data.sentences
    local e1_toks = {}
    local des_toks = {}
    local e2_toks = {}
    local maxE1Len = 0
    local maxDesLen = 0
    local maxE2Len = 0
    local neuralSep = "#SEP#"
    
    local maskE1 = torch.LongTensor(#sentences)
    local maskE2 = torch.LongTensor(#sentences)
    local maskDes
    if self.nnDescriptorFeature then
        maskDes = torch.LongTensor(#sentences)
    end
    for i=1,#sentences do
        local sentence = sentences[i]
        local types = stringx.split(sentences[i], neuralSep)
        local e1Tokens = stringx.split(types[1]," ")
        local e2Idx = 2
        if self.nnDescriptorFeature then
            e2Idx = 3
        end
        local e2Tokens = stringx.split(types[e2Idx]," ")
        table.insert(e1_toks, e1Tokens)
        table.insert(e2_toks, e2Tokens)
        maskE1[i] = #e1Tokens
        maskE2[i] = #e2Tokens
        if #e1Tokens > maxE1Len then
            maxE1Len = #e1Tokens
        end
        if #e2Tokens > maxE2Len then
            maxE2Len = #e2Tokens
        end
        if self.nnDescriptorFeature then
            local desTokens = stringx.split(types[2]," ")
            table.insert(des_toks, desTokens)
            maskDes[i] = #desTokens
            if #desTokens > maxDesLen then
                maxDesLen = #desTokens
            end
        end
    end

    --note that inside if the vocab is already created
    --just directly return
    self:buildVocab(sentences, e1_toks, des_toks, e2_toks)    

    local e1Inputs = torch.IntTensor(#sentences, maxE1Len)
    local e2Inputs = torch.IntTensor(#sentences, maxE2Len)
    self:fillInputs(#sentences, e1Inputs, maxE1Len, e1_toks)
    self:fillInputs(#sentences, e2Inputs, maxE2Len, e2_toks)
    local desInputs
    if self.nnDescriptorFeature then
        desInputs = torch.IntTensor(#sentences, maxDesLen)
        self:fillInputs(#sentences, desInputs, maxDesLen, des_toks)
    end
    if self.gpuid >= 0 then 
        e1Inputs = e1Inputs:cuda() 
        e2Inputs = e2Inputs:cuda() 
        maskE1 = maskE1:cuda()
        maskE2 = maskE2:cuda()
        if self.nnDescriptorFeature then
            desInputs = desInputs:cuda()
            maskDes = maskDes:cuda()
        end
    end
    print("number of sentences: "..#sentences)
    print("max e1 length: "..maxE1Len)
    print("max des length: "..maxDesLen)
    print("max e2 length: "..maxE2Len)
    if self.nnDescriptorFeature then
        return {{e1Inputs, maskE1}, {desInputs, maskDes}, {e2Inputs, maskE2}}
    else
        return {{e1Inputs, maskE1}, {e2Inputs, maskE2}}
    end
end

function SemEvalMLP:fillInputs(numSents, inputTensor, maxLen, toks)
    for sId=1,numSents do
        local tokens = toks[sId]
        for step=1,maxLen do
            if step > #tokens then
                inputTensor[sId][step] = 0 ---padding token, always zero-padding
            else 
                local tok = tokens[step]
                local tok_id = self.word2idx[tok]
                if tok_id == nil then
                    tok_id = self.word2idx[self.unkToken]
                end
                inputTensor[sId][step] = tok_id
            end
        end
    end
end

function SemEvalMLP:buildVocab(sentences, e1_toks, des_toks, e2_toks)
    if self.idx2word ~= nil then
        return 
    end
    local embeddingObject = self.embeddingObject
    local embW2V = embeddingObject.w2vvocab

    self.idx2word = {}
    self.word2idx = {}
    self.vocabSize = 0
    self.vocabSize = self.vocabSize + 1
    self.word2idx[self.unkToken] = self.vocabSize
    self.idx2word[self.vocabSize] = self.unkToken
    self.unkTokens = {}
    self:buildVocabForTokens(sentences, e1_toks, embW2V)
    if self.nnDescriptorFeature then
        self:buildVocabForTokens(sentences, des_toks, embW2V)
    end
    self:buildVocabForTokens(sentences, e2_toks, embW2V)

    print("number of unique words (including unknown):".. self.vocabSize.." (unknown words are replaced by unk)")
    print("number of unknown tokens (not unique): ".. countTable(self.unkTokens))
    if self.testVocab ~= nil then
        print("[Info] Using the vocab in test as well")
        for i = 1, #self.testVocab do
            local tok = self.testVocab[i]
            local tokInEmbId = embW2V[tok]
            if tokInEmbId == nil then
                ---not in the pretrained embedding, just use unk
                self.word2idx[tok] = self.word2idx[self.unkToken]
                if self.unkTokens[tok] == nil then
                    self.unkTokens[tok] = 1 --dummy value
                end
            else
                --in the pretraining embedding table
                local tok_id = self.word2idx[tok]
                if tok_id == nil then 
                    self.vocabSize = self.vocabSize + 1
                    self.word2idx[tok] = self.vocabSize
                    self.idx2word[self.vocabSize] = tok
                end
            end
        end
        print("number of unique words (after testVocab):".. self.vocabSize.." (unknown words are replaced by unk)")
        print("number of unknown tokens (after testVocab): ".. countTable(self.unkTokens))
    end
end

function SemEvalMLP:buildVocabForTokens(sentences, toks, embW2V)
    for i = 1, #sentences do
        local tokens = toks[i]
        for j = 1, #tokens do 
            local tok = tokens[j]
            local tokInEmbId = embW2V[tok]
            if tokInEmbId == nil then
                ---not in the pretrained embedding, just use unk
                self.word2idx[tok] = self.word2idx[self.unkToken]
                if self.unkTokens[tok] == nil then
                    self.unkTokens[tok] = 1 --dummy value
                end
            else
                --in the pretraining embedding table
                local tok_id = self.word2idx[tok]
                if tok_id == nil then 
                    self.vocabSize = self.vocabSize + 1
                    self.word2idx[tok] = self.vocabSize
                    self.idx2word[self.vocabSize] = tok
                end
            end
        end
    end
end

function countTable(table)
    local count = 0
    for _ in pairs(table) do count = count + 1 end
    return count
end

