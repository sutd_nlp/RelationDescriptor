local RNNPool, parent = torch.class('RNNPool', 'AbstractNeuralNetwork')

function RNNPool:__init(doOptimization, gpuid)
    parent.__init(self, doOptimization)
    self.data = {}
    self.gpuid = gpuid
end

function RNNPool:defineGlobalString()
    self.padToken = "<PAD>"
    self.unkToken = "<UNK>"
    self.startToken = "<START>"
    self.endToken = "<END>"
    self.e1Start = "<e1>"
    self.e1End = "</e1>"
    self.e2Start = "<e2>"
    self.e2End = "</e2>"
end

function RNNPool:buildSentAttn(hiddenSize)
    local attn = nn.Sequential()
    local firstPart = nn.Sequential():add(nn.SplitTable(1)) -- {numSents, (numWords x hiddenSize)}
    local mapTable = nn.MapTable()
    local mapOp = nn.Sequential():add(nn.Linear(hiddenSize, hiddenSize)):add(nn.Tanh())
    mapOp:add(nn.CMul(hiddenSize)):add(nn.Sum(2)):add(nn.SoftMax()):add(nn.Unsqueeze(1))
    mapTable:add(mapOp)
    firstPart:add(mapTable) -- {numSents x tensor(numWords)} -- the softmax value 
    firstPart:add(nn.JoinTable(1))
    firstPart:add(nn.Replicate(hiddenSize, 3)) --numSents x numWords x hiddenSize (attentionWeight (same))
    local concat = nn.ConcatTable():add(firstPart):add(nn.Identity())
    attn:add(concat):add(nn.CMulTable()):add(nn.Sum(2))
    return attn
end

function RNNPool:initialize(javadata, ...)
    self.data = {}
    self:defineGlobalString()
    local data = self.data
    data.sentences = listToTable(javadata:get("nnInputs"))
    data.embeddingSize = javadata:get("embeddingSize")
    data.hiddenSize = javadata:get("hiddenSize")
    data.layer2hiddenSize = javadata:get("layer2hiddenSize")
    self.gruHiddenSize = javadata:get("gruHiddenSize")
    self.numLabels = javadata:get("numLabels")
    data.embedding = javadata:get("embedding")
    self.dropout = javadata:get("dropout")
    self.fixEmbedding = javadata:get("fixEmbedding")
    local modelPath = javadata:get("nnModelFile")
    local isTraining = javadata:get("isTraining")
    data.isTraining = isTraining
    self.model = javadata:get("model")
    self.tanhGRU = javadata:get("tanhGRU")
    if self.tanhGRU then print("tanhGRU option is already deprecated") end
    self.gruDropout = javadata:get("gruDropout")
    self.pi = javadata:get("positionIndicator")
    self.embDropout = javadata:get("embDropout")
    self.add = javadata:get("add")
    self.attn = javadata:get("attn")
    local testVocabFile = javadata:get("testVocab")
    if isTraining and testVocabFile ~= nil then
        self.testVocab = readVocabs(testVocabFile)
    end

    if isTraining then
        self:loadEmbObj()
        self.x = self:prepare_input(isTraining)
        self.numSent = #data.sentences
    end

    if self.net == nil and isTraining then
        -- means is initialized process and we don't have the input yet.
        self:createNetwork()
        print(self.net)
    end

    if self.net == nil then 
        self:load_model(modelPath)
    end

    if not isTraining then 
        self.testInput = self:prepare_input(isTraining)
    end
    self.output = torch.Tensor()
    self.gradOutput = torch.Tensor()
    local outputAndGradOutputPtr = {... }
    if #outputAndGradOutputPtr > 0 then
        self.outputPtr = torch.pushudata(outputAndGradOutputPtr[1], "torch.DoubleTensor")
        self.gradOutputPtr = torch.pushudata(outputAndGradOutputPtr[2], "torch.DoubleTensor")
        return self:obtainParams()
    end
end

function RNNPool:loadEmbObj()
    local data = self.data
    self.embeddingSize = data.embeddingSize
    if data.embedding == 'google' then
        self.embeddingObject = loadGoogleEmbObj()
        self.embeddingSize = 300
    elseif data.embedding == 'turian' then
        self.embeddingObject = loadTurianEmbObj()
        self.embeddingSize = 50
    else
        error('unknown embedding type: '.. data.embedding)
    end
end

--The network is only created once is used.
function RNNPool:createNetwork()
    local data = self.data
    local hiddenSize = data.hiddenSize
    local embeddingSize = self.embeddingSize
    local layer2hiddenSize = data.layer2hiddenSize
    local gruHiddenSize = self.gruHiddenSize
    -- local sharedLookupTable = nn.LookupTableMaskZero(self.vocabSize, embeddingSize)
    -- sharedLookupTable.weight[1]:zero()
    -- for i =1, self.vocabSize do
    --     sharedLookupTable.weight[i+1]:copy(self.embeddingObject:word2vec(self.idx2word[i]))
    -- end
    local sharedLookupTable = nn.LookupTableMaskZero(self.vocabSize, embeddingSize)
    for i =1, self.vocabSize do
        sharedLookupTable.weight[i+1]:copy(self.embeddingObject:word2vec(self.idx2word[i]))
    end
    self.lt = sharedLookupTable
    print("Word Embedding layer: " .. self.lt.weight:size(1) .. " x " .. self.lt.weight:size(2))
    if self.fixEmbedding then
        self.lt.accGradParameters = function() end
        self.lt.parameters = function() end
    end

    local merge = nil -- means add table
    if self.add then
        combineSize = gruHiddenSize
    else
        merge = nn.JoinTable(3)
        combineSize = 2 * gruHiddenSize
    end
    local brnn = nn.SeqBRNNGRU(embeddingSize, gruHiddenSize, true, merge)
    brnn.batchfirst = true
    brnn.forwardModule.maskzero = true
    brnn.backwardModule.maskzero = true
    
    local net = nn.Sequential()
    net:add(self.lt)
    if self.embDropout > 0 then
        net:add(nn.Dropout(self.embDropout)) --adding lookup table
    end
    net:add(brnn) -- gointo brnn
    if self.attn  then
        local attn = self:buildSentAttn(combineSize)
        net:add(attn)
    else
        net:add(nn.Max(2)) --select the second dimension  
    end
    local currentSize = combineSize
    if hiddenSize ~= nil and hiddenSize > 0 then
        net:add(nn.Linear(currentSize, hiddenSize))
        net:add(nn.Tanh())
        net:add(nn.Dropout(self.dropout))
        currentSize = hiddenSize
        if layer2hiddenSize ~= nil and layer2hiddenSize > 0 then
            net:add(nn.Linear(currentSize, layer2hiddenSize))
            net:add(nn.Tanh())
            net:add(nn.Dropout(self.dropout))
            currentSize = layer2hiddenSize
        end
    end
    net:add(nn.Linear(currentSize, self.numLabels):noBias())
    self.net = net
    if self.gpuid >= 0 then
        self.net:cuda() 
    end
end

function RNNPool:obtainParams()
    --make sure we will not replace this variable
    self.params, self.gradParams = self.net:getParameters()
    print("Number of parameters: " .. self.params:nElement())
    if self.doOptimization then
        self:createOptimizer()
        -- no return array if optim is done here
    else
        if self.gpuid >= 0 then
            -- since the the network is gpu network.
            self.paramsDouble = self.params:double()
            self.paramsDouble:retain()
            self.paramsPtr = torch.pointer(self.paramsDouble)
            self.gradParamsDouble = self.gradParams:double()
            self.gradParamsDouble:retain()
            self.gradParamsPtr = torch.pointer(self.gradParamsDouble)
            return self.paramsPtr, self.gradParamsPtr
        else
            self.params:retain()
            self.paramsPtr = torch.pointer(self.params)
            self.gradParams:retain()
            self.gradParamsPtr = torch.pointer(self.gradParams)
            return self.paramsPtr, self.gradParamsPtr
        end
    end
end

function RNNPool:forward(isTraining, batchInputIds)
    if self.gpuid >= 0 and not self.doOptimization then
        self.params:copy(self.paramsDouble:cuda())
    end
    if isTraining then
        self.net:training()
    else
        self.net:evaluate()
    end
    local nnInput = self:getForwardInput(isTraining, batchInputIds)
    local cnnOutput
    if isTraining then
        cnnOutput = self.net:forward(nnInput)
    else
        cnnOutput = torch.Tensor()
        if self.gpuid >=0 then cnnOutput = cnnOutput:cuda() end
        local instSize = nnInput:size(1) 
        local testBatchSize = 10
        for i = 1, instSize, testBatchSize do
            if i + testBatchSize - 1 > instSize then testBatchSize =  instSize - i + 1 end
            local tmpOut = self.net:forward(nnInput:narrow(1, i, testBatchSize))
            cnnOutput = torch.cat(cnnOutput, tmpOut, 1)
        end
    end
    if self.gpuid >= 0 then
        cnnOutput = cnnOutput:double()
    end 
    self.output = cnnOutput
    if not self.outputPtr:isSameSizeAs(self.output) then
        self.outputPtr:resizeAs(self.output)
    end
    self.outputPtr:copy(self.output)
end

function RNNPool:getForwardInput(isTraining, batchInputIds)
    if isTraining then
        if batchInputIds ~= nil then
            batchInputIds:add(1) -- because the sentence is 0 indexed.
            self.batchInputIds = batchInputIds
            self.batchInput = self.x:index(1, batchInputIds)
            return self.batchInput
        else
            return self.x
        end
    else
        return self.testInput
    end
end

function RNNPool:getBackwardInput()
    if self.batchInputIds ~= nil then
        return self.batchInput
    else
        return self.x
    end
end

function RNNPool:backward()
    self.gradParams:zero()
    local gradOutputTensor = self.gradOutputPtr
    local backwardInput = self:getBackwardInput()  --since backward only happen in training
    self.gradOutput = gradOutputTensor
    if self.gpuid >= 0 then
        self.gradOutput = self.gradOutput:cuda()
    end
    self.net:training()
    self.net:backward(backwardInput, self.gradOutput)

    if self.gpuid >= 0 then
        self.gradParamsDouble:copy(self.gradParams:double())
    end
end

function RNNPool:prepare_input()
    local data = self.data
    local positionEmbedding = self.positionEmbedding
    local sentences = data.sentences
    local sentence_toks = {}
    local maxLen = 0
    for i=1,#sentences do
        local sentence = sentences[i]
        local tokens = stringx.split(sentence," ")
        table.insert(sentence_toks, tokens)
        if #tokens > maxLen then
            maxLen = #tokens
        end
    end

    --note that inside if the vocab is already created
    --just directly return
    self:buildVocab(sentences, sentence_toks)    
    ---build tensor input
    local inputs = torch.IntTensor(#sentences, maxLen)
    self:fillInputs(#sentences, inputs, maxLen, sentence_toks)
    if self.gpuid >= 0 then 
        inputs = inputs:cuda()
    end
    print("number of sentences: "..#sentences)
    print("max sentence length: "..maxLen)
    return inputs
end

function RNNPool:fillInputs(numSents, inputTensor, maxLen, toks)
    for sId=1,numSents do
        local tokens = toks[sId]
        for step=1,maxLen do
            if step > #tokens then
                inputTensor[sId][step] = 0 ---padding token, always zero-padding
            else 
                local tok = tokens[step]
                local tok_id = self.word2idx[tok]
                if tok_id == nil then
                    tok_id = self.word2idx[self.unkToken]
                end
                inputTensor[sId][step] = tok_id
            end
        end
    end
end

function RNNPool:incrementPositionIndicator()
    self.vocabSize = self.vocabSize + 1
    self.word2idx[self.e1Start] = self.vocabSize
    self.idx2word[self.vocabSize] = self.e1Start
    self.vocabSize = self.vocabSize + 1
    self.word2idx[self.e1End] = self.vocabSize
    self.idx2word[self.vocabSize] = self.e1End
    self.vocabSize = self.vocabSize + 1
    self.word2idx[self.e2Start] = self.vocabSize
    self.idx2word[self.vocabSize] = self.e2Start
    self.vocabSize = self.vocabSize + 1
    self.word2idx[self.e2End] = self.vocabSize
    self.idx2word[self.vocabSize] = self.e2End
end

function RNNPool:buildVocab(sentences, sentence_toks)
    if self.idx2word ~= nil then
        return 
    end
    local embeddingObject = self.embeddingObject
    local embW2V = embeddingObject.w2vvocab

    self.idx2word = {}
    self.word2idx = {}
    self.vocabSize = 0
    self.vocabSize = self.vocabSize + 1
    self.word2idx[self.unkToken] = self.vocabSize
    self.idx2word[self.vocabSize] = self.unkToken
    if self.pi then
        self:incrementPositionIndicator()
    end
    self.unkTokens = {}
    self:buildVocabForTokens(sentences, sentence_toks, embW2V)
    print("number of unique words (including unknown):".. self.vocabSize.." (unknown words are replaced by unk)")
    print("number of unknown tokens (not unique): ".. countTable(self.unkTokens))
    if self.testVocab ~= nil then
        print("[Info] Using the vocab in test as well")
        for i = 1, #self.testVocab do
            local tok = self.testVocab[i]
            local tokInEmbId = embW2V[tok]
            if tokInEmbId == nil then
                ---not in the pretrained embedding, just use unk
                self.word2idx[tok] = self.word2idx[self.unkToken]
                if self.unkTokens[tok] == nil then
                    self.unkTokens[tok] = 1 --dummy value
                end
            else
                --in the pretraining embedding table
                local tok_id = self.word2idx[tok]
                if tok_id == nil then 
                    self.vocabSize = self.vocabSize + 1
                    self.word2idx[tok] = self.vocabSize
                    self.idx2word[self.vocabSize] = tok
                end
            end
        end
        print("number of unique words (after testVocab):".. self.vocabSize.." (unknown words are replaced by unk)")
        print("number of unknown tokens (after testVocab): ".. countTable(self.unkTokens))
    end
end

function RNNPool:buildVocabForTokens(sentences, toks, embW2V)
    for i = 1, #sentences do
        local tokens = toks[i]
        for j = 1, #tokens do 
            local tok = tokens[j]
            local tokInEmbId = embW2V[tok]
            if tokInEmbId == nil and self.word2idx[tok] == nil then
                ---not in the pretrained embedding, just use unk
                self.word2idx[tok] = self.word2idx[self.unkToken]
                if self.unkTokens[tok] == nil then
                    self.unkTokens[tok] = 1 --dummy value
                end
            else
                --in the pretraining embedding table
                local tok_id = self.word2idx[tok]
                if tok_id == nil then 
                    self.vocabSize = self.vocabSize + 1
                    self.word2idx[tok] = self.vocabSize
                    self.idx2word[self.vocabSize] = tok
                end
            end
        end
    end
end

function countTable(table)
    local count = 0
    for _ in pairs(table) do count = count + 1 end
    return count
end

