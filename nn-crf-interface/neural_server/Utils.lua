-- Helper functions --

function listToTable(list)
    local res = {}
    for i = 1, list:size() do
        table.insert(res, list:get(i-1))
    end
    return res
end

function listToTable2D(list)
    local res = {}
    for i = 1, list:size() do
        table.insert(res, listToTable(list:get(i-1)))
    end
    return res
end

function array2Tensor2D(array)
    local res = torch.Tensor(#array, #array[1])
    for i = 1, #array do
        for j = 1, #array[1] do
            res[i][j] = array[i][j]
        end
    end
    return res
end

function loadGlove(wordList, dim, sharedLookupTable)
    sharedLookupTable = sharedLookupTable or false
    if glove == nil then
        ---- TODO: need to make this path more general later.
        glove = require 'nn-crf-interface/neural_server/glove_torch/glove'
    end
    glove:load(dim)

    specialSymbols = {}
    specialSymbols['<PAD>'] = torch.Tensor(dim):normal(0,1)
    specialSymbols['<S>'] = torch.Tensor(dim):normal(0,1)
    specialSymbols['</S>'] = torch.Tensor(dim):normal(0,1)

    local ltw
    local maskZero = false
    if sharedLookupTable then
        ltw = nn.LookupTableMaskZero(#wordList, dim)
        maskZero = true
    else 
        ltw = nn.LookupTable(#wordList, dim)
    end
    for i=1,#wordList do
        local emb = torch.Tensor(dim)

        local p_emb = glove:word2vec(wordList[i])
        if p_emb == nil then
            p_emb = specialSymbols[wordList[i]]
        end
        for j=1,dim do
            emb[j] = p_emb[j]
        end
        if maskZero then
            --becareful about this, check nn.LookupTableMaskZero documentation
            ltw.weight[i + 1] = emb
        else
            ltw.weight[i] = emb
        end
    end
    return ltw
end

function loadGoogleEmbObj()
    local GoogleFile = 'nn-crf-interface/neural_server/google/GoogleNews-vectors-negative300.bin.t7'
    if not paths.filep(GoogleFile) then
        error('Please run bintot7.lua to preprocess Google data!')
    else
        google = torch.load(GoogleFile)
        print('Done reading Google data.')
    end
    google.unkToken = "</s>"
    google.word2vec = function (self,word)
        local ind = self.w2vvocab[word] 
        if ind == nil then
            ind = self.w2vvocab[self.unkToken]
        end
        return self.M[ind]
    end
    return google
end

function loadGoogle(wordList, dim, sharedLookupTable)
    sharedLookupTable = sharedLookupTable or false
    if google == nil then
        google = require 'nn-crf-interface/neural_server/google/google'
    end
    google:load()

    local ltw
    local maskZero = false
    if sharedLookupTable then
        ltw = nn.LookupTableMaskZero(#wordList, dim)
        maskZero = true
    else 
        ltw = nn.LookupTable(#wordList, dim)
    end
    for i=1,#wordList do
        local emb = torch.Tensor(dim)
        local p_emb = google:word2vec(wordList[i])
        if p_emb == nil then
            print("not found word in word2vec, shouldn't happen")
        end
        for j=1,dim do
            emb[j] = p_emb[j]
        end
        if maskZero then
            --becareful about this, check nn.LookupTableMaskZero documentation
            ltw.weight[i + 1] = emb
        else
            ltw.weight[i] = emb
        end
    end
    return ltw
end

function loadTurianEmbObj()
    local turianFile = '/Users/allanjie/allan/data/turian_embeddings_normalized.50.t7'
    --local TurianFile = 'nn-crf-interface/neural_server/goolge/TurianNews-vectors-negative300.bin.t7'
    if not paths.filep(turianFile) then
        error('Please run bintot7.lua to preprocess Turian data!')
    else
        turian = torch.load(turianFile)
        print('Done reading Turian data.')
    end
    turian.unkToken = "*UNKNOWN*"
    turian.word2vec = function (self,word)
        local ind = self.w2vvocab[word] 
        if ind == nil then
            ind = self.w2vvocab['*UNKNOWN*']
        end
        return self.M[ind]
    end
    return turian
end

function loadTurian(wordList, padToken, startToken, endToken)
    if turian == nil then
        turian = require 'nn-crf-interface/neural_server/turian/turian'
    end
    turian:load()
    local dimension = 50
    local specialSymbols = {}
    specialSymbols[padToken] = torch.Tensor(dimension):normal(0,1)
    specialSymbols[startToken] = torch.Tensor(dimension):normal(0,1)
    specialSymbols[endToken] = torch.Tensor(dimension):normal(0,1)
    local ltw = nn.LookupTable(#wordList, dimension)
    for i=1,#wordList do 
    --start from 1 not 0, since we already have padding token in turian
        local emb = torch.Tensor(dimension)
        local p_emb
        if wordList[i] == padToken then
            p_emb = specialSymbols[padToken]  -- turian has no padding token
        elseif wordList[i] == startToken then 
            p_emb = specialSymbols[startToken] 
        elseif wordList[i] == endToken then 
            p_emb = specialSymbols[endToken] 
        else 
            p_emb = turian:word2vec(wordList[i])
            if p_emb == nil then
                print("cannot find an embedding?, should return UNKNOWN")
            end
        end
        for j=1,dimension do
            emb[j] = p_emb[j]
        end
        ltw.weight[i] = emb
    end
    return ltw
end

function printTable(tab) 
    for i,k in pairs(tab) do
        print(i .." " .. k)
    end
end

--usually help us to read test vocab
function readVocabs(file)
    vocab = {}
    for line in io.lines(file) do
        line = string.gsub(line, "\r", "")
        table.insert(vocab, line)
    end
    print("number of words: "..#vocab)
    return vocab
end
