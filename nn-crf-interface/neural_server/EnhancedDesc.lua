local EnhancedDesc, parent = torch.class('nn.EnhancedDesc', 'nn.Module')

--select the descriptor
---assuming always the batch input
---this one is very specific if descUnk is enable.
function EnhancedDesc:__init(dimension, descUnk, desUnkDimension, gpuid)
    parent.__init(self)
    self.dimension = dimension
    self.gradInput = {self.gradInput, self.gradInput.new()}
    self.descUnk = descUnk
    if self.descUnk then
        self.desUnkDimension = desUnkDimension
        self.weight = torch.Tensor(2 * desUnkDimension)
        self.gradWeight = torch.Tensor(2 * desUnkDimension):zero()
        self.selection = torch.LongTensor(6)
        self.gpuid = gpuid
        if self.gpuid >= 0 then
            self.selection = self.selection:cuda()
        end
        self:reset()
    end
end

function EnhancedDesc:reset()
    self.weight:normal(0, 1)
end

function EnhancedDesc:updateOutput(input)
    local t = input[1]
    local descs = input[2]
    local new_sizes = t:size()
    new_sizes[self.dimension] = descs:size(2)
    self.output:resize(new_sizes)
    local firstPart
    local secondPart
    if self.descUnk then
        firstPart = self.weight[{{1, self.desUnkDimension}}]
        secondPart = self.weight[{{self.desUnkDimension + 1, 2 * self.desUnkDimension}}]
    end
    for i = 1, t:size(1) do
        if self.descUnk and descs[i][3] == 0 then
            self.selection:copy(descs[i])
            self.selection[3] = 1 --dummy modify this later
            self.selection[4] = 1
            self.output[i] = t[i]:index(self.dimension-1, self.selection)
            self.output[i][3]:copy(firstPart)
            self.output[i][4]:copy(secondPart)
        else
            self.output[i]:index(t[i], self.dimension-1, descs[i])
        end
    end
    return self.output
end

function EnhancedDesc:updateGradInput(input, gradOutput)
    local t = input[1]
    local desc = input[2]
    self.gradInput[2]:resize(desc:size()):zero()
    local gradInput = self.gradInput[1] -- no gradient for the Desc variable
    local new_sizes = t:size()
    gradInput:resize(t:size()):zero()
    local nonZeroIdx
    local partialDesc
    local partialGradOutput
    if self.descUnk then
        nonZeroIdx = torch.LongTensor{1,2,5,6}
        partialGradOutput = torch.Tensor()
        partialDesc = torch.LongTensor()
        if self.gpuid >= 0 then
            nonZeroIdx = nonZeroIdx:cuda()
            partialGradOutput = partialGradOutput:cuda()
            partialDesc = partialDesc:cuda()
        end
        partialGradOutput:index(gradOutput, 2, nonZeroIdx)
        partialDesc:index(desc, 2, nonZeroIdx)
    end
    for i = 1, t:size(1) do
        if self.descUnk and desc[i][3] == 0 then
            gradInput[i]:indexAdd(self.dimension-1, partialDesc[i], partialGradOutput[i])
        else
            gradInput[i]:indexAdd(self.dimension-1, desc[i], gradOutput[i])
        end
    end
    return self.gradInput
end

function EnhancedDesc:accGradParameters(input, gradOutput, scale)
    if self.descUnk then
        local t = input[1]
        local desc = input[2]
        local firstPart = self.gradWeight[{{1, self.desUnkDimension}}]
        local secondPart = self.gradWeight[{{self.desUnkDimension + 1, 2 * self.desUnkDimension}}]
        for i = 1, t:size(1) do
            if desc[i][3] == 0 then
                firstPart:add(gradOutput[i][3])
                secondPart:add(gradOutput[i][4])
            end
        end
    end
end

function EnhancedDesc:clearState()
    self.gradInput[1]:set()
    self.gradInput[2]:set()
    self.output:set()
    return self
end