local SumEach, parent = torch.class('nn.SumEach', 'nn.Module')

----for this class the input is a table.
function SumEach:__init(dimension, nInputDims, sizeAverage, squeeze)
   parent.__init(self)
   self.dimension   = dimension or 1
   -- do not assign default value to nInputDims or it will break backward compatibility
   self.nInputDims  = nInputDims
   self.sizeAverage = sizeAverage or false
   if squeeze ~= nil then
      assert(type(squeeze) == 'boolean', 'squeeze has to be true/false')
      self.squeeze = squeeze
   else
      self.squeeze = true
   end
   self.gradInput = {self.gradInput, self.gradInput.new()}
end

function SumEach:_getPositiveDimension(tableInput)
   local input = tableInput[1]
   local dimension = self.dimension
   if dimension < 0 then
      dimension = input:dim() + dimension + 1
   elseif self.nInputDims and input:dim()==(self.nInputDims+1) then
      dimension = dimension + 1
   end
   assert(input:dim() >= dimension, "dimension exceeds input dimensions")
   return dimension
end

function SumEach:updateOutput(tableInput)
   local input = tableInput[1]
   local eachDimSize = tableInput[2]
   local dimension = self:_getPositiveDimension(input)
   if type(self.output) == 'number' then
      self.output = input.new()
   end
   self.output:sum(input, dimension)
   if self.sizeAverage then
      for i = 1, input:size(1) do 
         self.output[i]:div(eachDimSize[i])
      end
   end
   if (self.squeeze == nil or self.squeeze) and self.output:nDimension() > 1 then
      self.output:set(self.output:select(dimension, 1))
   end
   return self.output
end

function SumEach:updateGradInput(tableInput, gradOutput)
   local input = tableInput[1]
   local eachDimSize = tableInput[2]
   self.gradInput[2]:resize(eachDimSize:size(1)):zero()
   local dimension = self:_getPositiveDimension(input)
   -- zero-strides don't work with MKL/BLAS, so
   -- don't set self.gradInput to zero-stride tensor.
   -- Instead, do a deepcopy
   local size      = input:size()
   size[dimension] = 1
   if not gradOutput:isContiguous() then
      self._gradOutput = self._gradOutput or gradOutput.new()
      self._gradOutput:resizeAs(gradOutput):copy(gradOutput)
      gradOutput = self._gradOutput
   end
   gradOutput      = gradOutput:view(size)
   self.gradInput[1]:resizeAs(input)
   self.gradInput[1]:copy(gradOutput:expandAs(input))
   if self.sizeAverage then
      for i = 1, input:size(1) do 
         self.gradInput[1][i]:div(eachDimSize[i])
      end
   end
   return self.gradInput
end

function SumEach:clearState()
   nn.utils.clear(self, '_gradOutput')
   return parent.clearState(self)
end