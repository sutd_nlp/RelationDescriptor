include 'Desc.lua'
local RelationCNN, parent = torch.class('RelationCNN', 'AbstractNeuralNetwork')

function RelationCNN:__init(doOptimization, gpuid)
    parent.__init(self, doOptimization)
    self.data = {}
    self.gpuid = gpuid
end

function RelationCNN:defineGlobalString()
    self.padToken = "<PAD>"
    self.unkToken = "<UNK>"
    self.startToken = "<START>"
    self.endToken = "<END>"
end

function RelationCNN:initialize(javadata, ...)
    self.data = {}
    self:defineGlobalString()
    local data = self.data
    data.sentences = listToTable(javadata:get("nnInputs"))
    data.embeddingSize = javadata:get("embeddingSize")
    data.windowSize = javadata:get("windowSize")
    data.hiddenSize = javadata:get("hiddenSize")
    data.layer2hiddenSize = javadata:get("layer2hiddenSize")
    data.optimizer = javadata:get("optimizer")
    data.learningRate = javadata:get("learningRate")
    data.clipping = javadata:get("clipping")
    self.positionEmbedding = javadata:get("positionEmbedding")
    self.posEmbeddingSize = javadata:get("posEmbeddingSize")
    self.numLabels = javadata:get("numLabels")
    data.embedding = javadata:get("embedding")
    self.dropout = javadata:get("dropout")
    self.nnDescriptorFeature = javadata:get("nnDescriptorFeature")
    self.gruHiddenSize = javadata:get("gruHiddenSize")
    local modelPath = javadata:get("nnModelFile")
    local isTraining = javadata:get("isTraining")
    data.isTraining = isTraining

    if isTraining then
        self:loadEmbObj()
        self.x = self:prepare_input()
        self.numSent = #data.sentences
    end

    if self.net == nil and isTraining then
        -- means is initialized process and we don't have the input yet.
        self:createNetwork()
        print(self.net)
    end

    if self.net == nil then 
        self:load_model(modelPath)
    end

    if not isTraining then 
        self.testInput = self:prepare_input()
        print("initailizing test input")
    end
    self.output = torch.Tensor()
    self.gradOutput = torch.Tensor()
    local outputAndGradOutputPtr = {... }
    if #outputAndGradOutputPtr > 0 then
        self.outputPtr = torch.pushudata(outputAndGradOutputPtr[1], "torch.DoubleTensor")
        self.gradOutputPtr = torch.pushudata(outputAndGradOutputPtr[2], "torch.DoubleTensor")
        return self:obtainParams()
    end
end

function RelationCNN:loadEmbObj()
    local data = self.data
    self.embeddingSize = data.embeddingSize
    if data.embedding == 'google' then
        self.embeddingObject = loadGoogleEmbObj()
        self.embeddingSize = 300
    elseif data.embedding == 'turian' then
        self.embeddingObject = loadTurianEmbObj()
        self.embeddingSize = 50
    else
        error('unknown embedding type: '.. data.embedding)
    end
end

--The network is only created once is used.
function RelationCNN:createNetwork()
    local data = self.data
    local hiddenSize = data.hiddenSize
    local embeddingSize = self.embeddingSize
    local layer2hiddenSize = data.layer2hiddenSize
    local cnnWindowSize = data.windowSize
    local positionEmbedding = self.positionEmbedding
    local nnDescriptorFeature = self.nnDescriptorFeature
    local gruHiddenSize = self.gruHiddenSize
    local sharedLookupTable = nn.LookupTableMaskZero(self.vocabSize, embeddingSize)
    sharedLookupTable.weight[1]:zero()
    for i =1, self.vocabSize do
        sharedLookupTable.weight[i+1] = self.embeddingObject:word2vec(self.idx2word[i])
    end
    self.lt = sharedLookupTable
    print("Word Embedding layer: " .. self.lt.weight:size(1) .. " x " .. self.lt.weight:size(2))
    
    local parallel 
    local conv = nn.Sequential()
    if positionEmbedding then
        self.p1lt = nn.LookupTableMaskZero(self.numPosition, self.posEmbeddingSize)
        self.p2lt = self.p1lt:sharedClone()
        print("Position Embedding layer: " .. self.p1lt.weight:size(1) .. " x " .. self.p1lt.weight:size(2))
        local pt = nn.ParallelTable()
        pt:add(self.lt)
        pt:add(self.p1lt)
        pt:add(self.p2lt)
        conv:add(pt)
        local jt = nn.JoinTable(3)
        conv:add(jt) --join the third dimensions
    else
        conv:add(self.lt)
    end
    local embedSize = embeddingSize
    if positionEmbedding then
        embedSize = embedSize + 2 * self.posEmbeddingSize
    end
    conv:add(nn.TemporalConvolution(embedSize, hiddenSize, cnnWindowSize, 1))
    conv:add(nn.Tanh())
    conv:add(nn.Max(2))
    local lastHiddenSize = hiddenSize
    if layer2hiddenSize ~= nil and layer2hiddenSize > 0 then 
        conv:add(nn.Linear(hiddenSize, layer2hiddenSize))
        conv:add(nn.Tanh())
        lastHiddenSize = layer2hiddenSize
    end
    conv:add(nn.Linear(lastHiddenSize, self.numLabels):noBias())

    local gruSeq
    self.net = nn.Sequential()
    if nnDescriptorFeature then
        parallel = nn.ParallelTable()
        local firstComponent = nn.Sequential() --:add(nn.SelectTable(1))
        firstComponent:add(conv)
        parallel:add(firstComponent)
        local secondComponent = nn.Sequential()--:add(nn.SelectTable(2))
        local gruLt = self.lt:sharedClone()
        local gruPre = nn.Sequential():add(gruLt):add(nn.Transpose({1,2})):add(nn.SplitTable(1))
        local myFwdSeq = nn.Sequential():add(nn.Sequencer(nn.GRU(embeddingSize, gruHiddenSize):maskZero(1)))
        local myBwdSeq = nn.Sequential():add(nn.ReverseTable())
        myBwdSeq:add(nn.Sequencer(nn.GRU(embeddingSize, gruHiddenSize):maskZero(1)))
        myBwdSeq:add(nn.ReverseTable())
        local biconcat = nn.ConcatTable():add(myFwdSeq):add(myBwdSeq)
        local brnn = nn.Sequential():add(biconcat):add(nn.ZipTable()):add(nn.Sequencer(nn.JoinTable(1, 1)))
        local mapTable = nn.MapTable():add(nn.Unsqueeze(1)) --in order to add one more dimension
        brnn:add(mapTable):add(nn.JoinTable(1)):add(nn.Transpose({1,2}))
        gruSeq = nn.Sequential():add(gruPre):add(brnn)

        local msParallel = nn.ParallelTable()
        msParallel:add(gruSeq)
        msParallel:add(nn.Identity()) -- process the input
        secondComponent:add(msParallel)
        secondComponent:add(nn.Desc(2)) --select the second dimension, parallel output as input
        secondComponent:add(nn.SplitTable(2))
        secondComponent:add(nn.Bilinear(2 * gruHiddenSize, 2 * gruHiddenSize, self.numLabels, false))
        -- secondComponent:add(nn.View(-1):setNumInputDims(2)) --as we have the batch output
        parallel:add(secondComponent)
        self.net:add(parallel):add(nn.CAddTable()) 
    else
        self.net:add(conv)
    end
    if self.gpuid >= 0 then
        self.net:cuda() 
    end
end

function RelationCNN:obtainParams()
    --make sure we will not replace this variable
    self.params, self.gradParams = self.net:getParameters()
    print("Number of parameters: " .. self.params:nElement())
    if self.doOptimization then
        self:createOptimizer()
        -- no return array if optim is done here
    else
        if self.gpuid >= 0 then
            -- since the the network is gpu network.
            self.paramsDouble = self.params:double()
            self.paramsDouble:retain()
            self.paramsPtr = torch.pointer(self.paramsDouble)
            self.gradParamsDouble = self.gradParams:double()
            self.gradParamsDouble:retain()
            self.gradParamsPtr = torch.pointer(self.gradParamsDouble)
            return self.paramsPtr, self.gradParamsPtr
        else
            self.params:retain()
            self.paramsPtr = torch.pointer(self.params)
            self.gradParams:retain()
            self.gradParamsPtr = torch.pointer(self.gradParams)
            return self.paramsPtr, self.gradParamsPtr
        end
    end
end

function RelationCNN:forward(isTraining, batchInputIds)
    if self.gpuid >= 0 and not self.doOptimization and isTraining then
        self.params:copy(self.paramsDouble:cuda())
    end
    local nnInput = self:getForwardInput(isTraining, batchInputIds)
    local cnnOutput
    if isTraining then
        cnnOutput = self.net:forward(nnInput)
    else
        cnnOutput = torch.Tensor()
        if self.gpuid >=0 then cnnOutput = cnnOutput:cuda() end
        local instSize = nnInput[1][1]:size(1) 
        local testBatchSize = 10
        for i = 1, instSize, testBatchSize do
            if i + testBatchSize - 1 > instSize then testBatchSize =  instSize - i + 1 end
            local tmpOut
            if self.positionEmbedding then
                tmpOut = self.net:forward(
                      {{nnInput[1][1]:narrow(1, i, testBatchSize), nnInput[1][2]:narrow(1, i, testBatchSize),
                    nnInput[1][3]:narrow(1, i, testBatchSize)
                        }, {
                        nnInput[2][1]:narrow(1, i, testBatchSize),
                        nnInput[2][2]:narrow(1, i, testBatchSize)
                    }}
                    )
            else
                tmpOut = self.net:forward(
                  {nnInput[1]:narrow(1, i, testBatchSize), nnInput[2]:narrow(1, i, testBatchSize),
                   nnInput[3]:narrow(1, i, testBatchSize)})
            end
            cnnOutput = torch.cat(cnnOutput, tmpOut, 1)
        end
        print("after testing")
    end
    
    if self.gpuid >= 0 then
        cnnOutput = cnnOutput:double()
    end 
    self.output = cnnOutput
    if not self.outputPtr:isSameSizeAs(self.output) then
        self.outputPtr:resizeAs(self.output)
    end
    self.outputPtr:copy(self.output)
end

function RelationCNN:getForwardInput(isTraining, batchInputIds)
    if isTraining then
        if batchInputIds ~= nil then
            batchInputIds:add(1) -- because the sentence is 0 indexed.
            self.batchInputIds = batchInputIds
            if self.positionEmbedding then
                if self.nnDescriptorFeature then
                    self.batchInput = {{self.x[1][1]:index(1, batchInputIds), 
                                self.x[1][2]:index(1, batchInputIds), 
                                self.x[1][3]:index(1, batchInputIds)}, 
                                        {self.x[2][1]:index(1, batchInputIds),
                                        self.x[2][2]:index(1, batchInputIds)}}
                else
                    self.batchInput = {self.x[1]:index(1, batchInputIds), 
                                self.x[2]:index(1, batchInputIds), 
                                self.x[3]:index(1, batchInputIds)}
                end
            else
                if self.nnDescriptorFeature then
                    self.batchInput = {self.x[1]:index(1, batchInputIds), 
                    {self.x[2][1]:index(1, batchInputIds), self.x[2][2]:index(1, batchInputIds)}}
                else
                    self.batchInput = self.x:index(1, batchInputIds)
                end
            end
            return self.batchInput
        else
            return self.x
        end
    else
        return self.testInput
    end
end

function RelationCNN:getBackwardInput()
    if self.batchInputIds ~= nil then
        return self.batchInput
    else
        return self.x
    end
end

function RelationCNN:backward()
    self.gradParams:zero()
    local gradOutputTensor = self.gradOutputPtr
    local backwardInput = self:getBackwardInput()  --since backward only happen in training
    self.gradOutput = gradOutputTensor
    if self.gpuid >= 0 then
        self.gradOutput = self.gradOutput:cuda()
    end
    self.net:backward(backwardInput, self.gradOutput)

    if self.gpuid >= 0 then
        self.gradParamsDouble:copy(self.gradParams:double())
    end
end

function RelationCNN:prepare_input()
    local data = self.data
    local positionEmbedding = self.positionEmbedding
    local sentences = data.sentences
    local sentence_toks = {}
    local maxLen = 0
    local neuralDescSep = "#DESCSEP#"
    local dleft
    local dright
    
    local masks = ((self.nnDescriptorFeature == true) and torch.LongTensor(#sentences, 2)) or nil
    for i=1,#sentences do
        local sentence = sentences[i]
        if self.nnDescriptorFeature then
            local types = stringx.split(sentences[i], neuralDescSep)
            sentence = types[1]
            dleft = types[2]
            dright = types[3]
            masks[i][1] = dleft
            masks[i][2] = dright
        end
        local tokens = stringx.split(sentence," ")
        table.insert(sentence_toks, tokens)
        if #tokens > maxLen then
            maxLen = #tokens
        end
    end

    --note that inside if the vocab is already created
    --just directly return
    self:buildVocab(sentences, sentence_toks)    

    local inputs = torch.IntTensor(#sentences, maxLen)
    local p1Inputs = ((positionEmbedding == true) and torch.IntTensor(#sentences, maxLen)) or nil
    local p2Inputs = ((positionEmbedding == true) and torch.IntTensor(#sentences, maxLen)) or nil
    
    for sId=1,#sentences do
        local tokens = sentence_toks[sId]
        for step=1,maxLen do
            if step > #tokens then
                inputs[sId][step] = 0 ---padding token, always zero-padding
                if positionEmbedding then
                    p1Inputs[sId][step] = self.pos2idx[self.padToken]
                    p2Inputs[sId][step] = self.pos2idx[self.padToken]
                end
            else 
                local tok
                if positionEmbedding then
                    local preTok = stringx.split(tokens[step], "#SEP#")
                    tok = preTok[1]
                    local p1Id = self.pos2idx[preTok[2]]
                    local p2Id = self.pos2idx[preTok[3]]
                    if p1Id == nil then
                        print("shouldn't happened in testing (1st position): ".. preTok[2])
                        p1Id = self.pos2idx[self.unkToken]
                    end
                    if p2Id == nil then
                        print("shouldn't happened in testing (2nd position): ".. preTok[3])
                        p2Id = self.pos2idx[self.unkToken]
                    end
                    p1Inputs[sId][step] = p1Id
                    p2Inputs[sId][step] = p2Id
                    
                else
                   tok = tokens[step] 
                end
                local tok_id = self.word2idx[tok]
                if tok_id == nil then
                    tok_id = self.word2idx[self.unkToken]
                end
                inputs[sId][step] = tok_id
            end
        end
    end
    if self.gpuid >= 0 then 
        inputs = inputs:cuda() 
        if positionEmbedding then
            p1Inputs:cuda()
            p2Inputs:cuda()
        end
        if self.nnDescriptorFeature then
            masks:cuda()
        end
    end
    print("number of sentences: "..#sentences)
    print("max sentencen length: "..maxLen)
    self.maxLen = maxLen
    if positionEmbedding then
        if self.nnDescriptorFeature then
            return {{inputs, p1Inputs, p2Inputs}, {inputs, masks}}
        else
            return {inputs, p1Inputs, p2Inputs}
        end
    else
        if self.nnDescriptorFeature then
            return {inputs, {inputs, masks}}
        else
            return inputs
        end
    end
end

function RelationCNN:buildVocab(sentences, sentence_toks)
    if self.idx2word ~= nil then
        return 
    end
    local embeddingObject = self.embeddingObject
    local embW2V = embeddingObject.w2vvocab
    self.idx2word = {}
    self.word2idx = {}
    self.vocabSize = 0
    self.vocabSize = self.vocabSize + 1
    self.word2idx[self.unkToken] = self.vocabSize
    self.idx2word[self.vocabSize] = self.unkToken
    local unkTokens = {}
    for i = 1, #sentences do
        local tokens = sentence_toks[i]
        for j = 1, #tokens do 
            local tok = tokens[j]
            if self.positionEmbedding then
                local preTok = stringx.split(tokens[j], "#SEP#")
                tok = preTok[1]
            end
            local tokInEmbId = embW2V[tok]
            if tokInEmbId == nil and tok ~= self.startToken 
                and tok ~= self.endToken then
                ---not in the pretrained embedding, just use unk
                self.word2idx[tok] = self.word2idx[self.unkToken]
                if unkTokens[tok] == nil then
                    unkTokens[tok] = 1 --dummy value
                end
            else
                --in the pretraining embedding table
                local tok_id = self.word2idx[tok]
                if tok_id == nil then 
                    self.vocabSize = self.vocabSize + 1
                    self.word2idx[tok] = self.vocabSize
                    self.idx2word[self.vocabSize] = tok
                end
            end
        end
    end
    print("number of unique words (including unknown):".. self.vocabSize.." (unknown words are replaced by unk)")
    print("number of unknown tokens (not unique): ".. countTable(unkTokens))
    if self.positionEmbedding then
        self.pos2idx = {}
        self.pos2idx[self.padToken] = 0
        self.pos2idx[self.unkToken] = 1  --for now
        self.numPosition = 2
        for i=1,#sentences do
            local tokens = sentence_toks[i]
            for j=1,#tokens do
                local preTok = stringx.split(tokens[j], "#SEP#")
                local p1Id = self.pos2idx[preTok[2]]
                local p2Id = self.pos2idx[preTok[3]]
                if p1Id == nil then
                    self.pos2idx[preTok[2]] = self.numPosition
                    self.numPosition = self.numPosition + 1
                end
                if p2Id == nil then
                    self.pos2idx[preTok[3]] = self.numPosition
                    self.numPosition = self.numPosition + 1
                end
            end
        end
        print("number of unique position: ".. countTable(self.pos2idx) .. " [checked] ".. self.numPosition)
    end
end

function countTable(table)
    local count = 0
    for _ in pairs(table) do count = count + 1 end
    return count
end

