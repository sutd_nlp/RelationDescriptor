local opt = {}
wd='/data/allan/embeddings/'
opt.binfilename = wd .. 'GoogleNews-vectors-negative300.txt'
opt.outfilename = wd .. 'GoogleNews-vectors-negative300.bin.t7'

--Reading Header

local encodingsize = -1
local ctr = 0
for line in io.lines(opt.binfilename) do
    if line ~= '3000000 300' then
        if ctr == 0 then
            for i in string.gmatch(line, "%S+") do
                encodingsize = encodingsize + 1
            end
        end
        ctr = ctr + 1
    end
end

words = ctr
size = encodingsize
print("size is: "..size)




local w2vvocab = {}
local v2wvocab = {}
local M = torch.FloatTensor(words,size)

--Reading Contents

i = 1
for line in io.lines(opt.binfilename) do
    xlua.progress(i,words)
    if line ~= '3000000 300' then
        local vecrep = {}
        for i in string.gmatch(line, "%S+") do
            table.insert(vecrep, i)
        end
        str = vecrep[1]
        table.remove(vecrep,1)
        vecrep = torch.FloatTensor(vecrep)

        local norm = torch.norm(vecrep,2)
        if norm ~= 0 then vecrep:div(norm) end
        w2vvocab[str] = i
        v2wvocab[i] = str
        M[{{i},{}}] = vecrep
        i = i + 1
    end
end


--Writing Files
Google = {}
Google.M = M
Google.w2vvocab = w2vvocab
Google.v2wvocab = v2wvocab
torch.save(opt.outfilename,Google)
print('Writing t7 File for future usage.')



return Google


