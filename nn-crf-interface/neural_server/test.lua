require 'nn'
require 'rnn'
local vocabSize = 3
local hiddenSize = 100
local numLabels = 8
local bidirection = true

local sharedLookupTable = nn.LookupTableMaskZero(vocabSize, hiddenSize)
local fwd = nn.Sequential()
       :add(nn.Linear(vocabSize, hiddenSize))
       :add(nn.FastLSTM(hiddenSize, hiddenSize):maskZero(1))

-- internally, rnn will be wrapped into a Recursor to make it an AbstractRecurrent instance.
local fwdSeq = nn.Sequencer(fwd)

-- backward rnn (will be applied in reverse order of input sequence)
local bwd, bwdSeq
if bidirection then
    bwd = nn.Sequential()
       :add(nn.Linear(vocabSize, hiddenSize))
       :add(nn.FastLSTM(hiddenSize, hiddenSize):maskZero(1))
       
    bwdSeq = nn.Sequential()
        :add(nn.Sequencer(bwd))
        :add(nn.ReverseTable())
end

-- merges the output of one time-step of fwd and bwd rnns.
-- You could also try nn.AddTable(), nn.Identity(), etc.
local merge = nn.JoinTable(1, 1)
local mergeSeq = nn.Sequencer(merge)

-- Assume that two input sequences are given (original and reverse, both are right-padded).
-- Instead of ConcatTable, we use ParallelTable here.
local parallel = nn.ParallelTable()
parallel:add(fwdSeq)

if bidirection then
    parallel:add(bwdSeq)
end
local brnn = nn.Sequential()
   :add(parallel)
   :add(nn.ZipTable())
   :add(mergeSeq)
local mergeHiddenSize = hiddenSize
if bidirection then
    mergeHiddenSize = 2 * hiddenSize
end
local net = nn.Sequential()
    :add(brnn) 
    :add(nn.Sequencer(nn.MaskZero(nn.Linear(mergeHiddenSize, numLabels), 1))) 


local params, gradParams = net:getParameters()
print ("params before forward: "..params[1].." "..params[2].." sum:"..torch.sum(params))

local x = {}
x[1] = torch.DoubleTensor(3); x[1][1] = 2; x[1][2] =7;
x[2] = torch.DoubleTensor(3); x[2][1] = 1; x[2][2] = 3;
x[3] = torch.DoubleTensor(3); x[3][1] = 5; x[3][2] = 7;
x[4] = torch.DoubleTensor(3); x[4][1] = 5; x[4][2] = 8;
local output = net:forward({x,x})
print ("params after forward: "..params[1].." "..params[2].." sum:"..torch.sum(params))
