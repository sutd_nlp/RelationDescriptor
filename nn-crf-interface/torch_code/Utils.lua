

--[[
Input to this read embedding is
1. vocab file, line by line
2. embedding t7 file
--]]
function read_embedding(vocab_path, emb_path)
    local vocab = Vocab(vocab_path)
    local embedding = torch.load(emb_path).M --load the t7 file
    print("[Info] Done reading the Embeddings.")
    return vocab, embedding
end

function read_pre_labels(path)
    --path = "/Users/allanjie/Documents/workspace/descriptor/data/semeval/prevlabel.txt"
    local file = io.open(path, 'r')
    local label2id = {}
    local id2label = {}
    local line
    local id = 1
    while true do
        line = file:read()
        if line == nil then break end
        line = string.gsub(line, "\r", "")
        label2id[line] = id
        id2label[id] = line
        id = id + 1
    end
    file:close()
    print("[Info] read the label list. ")
    return label2id, id2label
end

function read_raw_datasets(path)
    local dataset = {}
    local file = io.open(path, 'r')
    local line
    dataset.sents = {}
    dataset.labels = {}
    local num = 0
    while true do
        line = file:read()
        if line == nil then break end
        line = string.gsub(line, "\r", "")
        local tokens = stringx.split(line)
        local idx = #dataset.sents + 1
        dataset.sents[idx] = tokens
        line = file:read()  --pos
        line = file:read()  --depIdx
        line = file:read()  --depLabel
        line = file:read()  --entity idx
        line = file:read()  --label
        line = string.gsub(line, "\r", "")
        dataset.labels[idx] = line
        line = file:read()
        num  = num + 1
    end
    file:close()
    print("number of instances: "..#dataset.sents.. "  [checked]: ".. num)
    return dataset
end

function read_sentences_into_ids_rnn(dataset, word2idx, unkToken)
    dataset.inputs = {}
    for s = 1, #dataset.sents do
        local tokens = dataset.sents[s]
        local input = torch.IntTensor(1, #tokens)
        for i = 1, #tokens do
            local tok = tokens[i]
            local tokId = word2idx[tok]
            if tokId == nil then
                tokId = word2idx[unkToken]
            end
            input[1][i] = tokId
        end
        dataset.inputs[s] = input
    end
end

function buildVocab(trainDataset, testDataset, startToken, endToken, unkToken, embVocab, emb,
        e1Start, e1End, e2Start, e2End, embSize)
    local word2idx = {}
    local idx2word = {}
    local vocabSize = 0
    local vocabSize = vocabSize + 1
    word2idx[startToken] = vocabSize
    idx2word[vocabSize] = startToken
    vocabSize = vocabSize + 1
    word2idx[endToken] = vocabSize
    idx2word[vocabSize] = endToken
    vocabSize = vocabSize + 1
    word2idx[unkToken] = vocabSize
    idx2word[vocabSize] = unkToken
    if e1Start ~= nil then
        vocabSize = vocabSize + 1
        word2idx[e1Start] = vocabSize
        idx2word[vocabSize] = e1Start
        vocabSize = vocabSize + 1
        word2idx[e1End] = vocabSize
        idx2word[vocabSize] = e1End
        vocabSize = vocabSize + 1
        word2idx[e2Start] = vocabSize
        idx2word[vocabSize] = e2Start
        vocabSize = vocabSize + 1
        word2idx[e2End] = vocabSize
        idx2word[vocabSize] = e2End
    end
    local unkTokens = {}
    vocabSize = buildVocabForDataset(word2idx, idx2word, trainDataset, embVocab, startToken, endToken, unkTokens, vocabSize)
    vocabSize = buildVocabForDataset(word2idx, idx2word, testDataset, embVocab, startToken, endToken, unkTokens, vocabSize)
    --self:printTable(idx2word)
    print("number of unique words(including pad and unknown):"
        .. vocabSize) --this won't count the 0-indexed
    print("number of unknown tokens (not unique): "..countTable(unkTokens))
    local vocabEmb = torch.Tensor(vocabSize, embSize)
    for i = 1, vocabSize do
        if idx2word[i] == startToken or idx2word[i] == endToken or
            idx2word[i] == e1Start or idx2word[i] == e1End or idx2word[i] == e2Start or idx2word[i] == e2End  then
            vocabEmb[i]:copy(emb[embVocab:index(unkToken)])
        else
            vocabEmb[i]:copy(emb[embVocab:index(idx2word[i])])
        end
    end
    print("[Info] Finish building the vocabulary according to training data.")
    return word2idx, idx2word, vocabEmb
end

function buildVocabForDataset(word2idx, idx2word, dataset, embVocab, startToken, endToken, unkTokens, vocabSize)
    for i=1,#dataset.sents do
        local tokens = dataset.sents[i]
        for j=1,#tokens do
            local tok = tokens[j]
            if not embVocab:contains(tok) and tok ~= startToken and tok ~= endToken
                and word2idx[tok] == nil then
                word2idx[tok] = word2idx[unkToken]
                if unkTokens[tok] == nil then
                    unkTokens[tok] = 1
                end
            else
                local tok_id = word2idx[tok]
                if tok_id == nil then
                    vocabSize = vocabSize + 1
                    word2idx[tok] = vocabSize
                    idx2word[vocabSize] = tok
                end
            end
        end
    end
    return vocabSize
end

function countTable(table)
    local count = 0
    for _ in pairs(table) do count = count + 1 end
    return count
end


function argmaxRNN(v)
    local idx = 1
    local max = v[1][1]
    for i = 2, v:size(2) do
        if v[1][i] > max then
            max = v[1][i]
            idx = i
        end
    end
    return idx
end


function evaluate(dataset, label2id, id2label)
    local revSuffix = "-rev"
    local p = {}
    local totalPred = {}
    local totalInData = {}
    for l = 1, #id2label do
        p[l] = 0
        totalPred[l] = 0
        totalInData[l] = 0
    end
    for idx = 1, #dataset.sents do
        local predLabel = dataset.predLabels[idx]
        local goldLabel = dataset.labels[idx]
        local foundRevPred = string.find(predLabel, revSuffix)
        local foundRevGold = string.find(goldLabel, revSuffix)
        local predId = label2id[predLabel]
        local goldId = label2id[goldLabel]
        --print(predId .. " ".. goldId)
        if foundRevPred ~= nil then
            predId = label2id[string.gsub(predLabel, revSuffix, "")]
        end
        if foundRevGold ~= nil then
            goldId = label2id[string.gsub(goldLabel, revSuffix, "")]
        end
        if predId == nil or goldId == nil then
            error("nil value for label id ??")
        end
        if predId == goldId then
            p[predId] = p[predId] + 1
        end
        totalPred[predId] = totalPred[predId] + 1
        totalInData[goldId] = totalInData[goldId] + 1
    end
    local macroP = 0
    local macroR = 0
    local macroF = 0
    local num = 0
    for l = 1, #id2label do
        if string.find(id2label[l], revSuffix) == nil then
            local precision = p[l] / totalPred[l] * 100
            local recall = p[l] / totalInData[l] * 100
            local fscore = 2 * p[l] / (totalPred[l] + totalInData[l]) * 100
            macroP = macroP + precision
            macroR = macroR + recall
            macroF = macroF + fscore
            num = num + 1
        end
    end
    macroP = macroP / num
    macroR = macroR / num
    macroF = macroF / num
    print("[Results] Macro Prec.: " .. macroP .. " Rec.: ".. macroR .. " F1.: "..macroF)
    return macroF
end

function setupGPU(gpuid, seed)
    if gpuid >= 0 then
        local ok, cunn = pcall(require, 'cunn')
        local ok2, cutorch = pcall(require, 'cutorch')
        if not ok then print('package cunn not found!') end
        if not ok2 then print('package cutorch not found!') end
        if ok and ok2 then
            print('using CUDA on GPU ' .. gpuid .. '...')
            cutorch.setDevice(gpuid + 1) -- note +1 to make it 0 indexed! sigh lua
            cutorch.manualSeed(seed)
        else
            print('If cutorch and cunn are installed, your CUDA toolkit may be improperly configured.')
            print('Check your CUDA toolkit installation, rebuild cutorch and cunn, and try again.')
            print('Falling back on CPU mode')
            gpuid = -1 -- overwrite user setting
        end
    else
        print("CPU mode")
    end
end