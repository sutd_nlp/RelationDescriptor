torch.class('RNNPool', 'AbstractModel')

function RNNPool:__init(config)
    self.embeddingSize = config.embeddingSize
    self.gruHiddenSize = config.gruHiddenSize
    self.numLabels = config.numLabels
    self.gpuid = config.gpuid
    self.fixEmbedding = config.fixEmbedding or false
    self.vocabSize = config.vocabSize
    self.attn = config.attn
end

--The network is only created once is used.
function RNNPool:buildModel(w2v)
    local embeddingSize = self.embeddingSize
    local gruHiddenSize = self.gruHiddenSize
    local sharedLookupTable = nn.LookupTableMaskZero(self.vocabSize, embeddingSize)
    sharedLookupTable.weight[1]:zero()
    sharedLookupTable.weight[{ {2, self.vocabSize + 1} , {}  }]:copy(w2v)
    self.lt = sharedLookupTable
    print("Word Embedding layer: " .. self.lt.weight:size(1) .. " x " .. self.lt.weight:size(2))
    if self.fixEmbedding then
        self.lt.accGradParameters = function() end
        self.lt.parameters = function() end
    end
    --torch.manualSeed(1337)
    local myFwdSeq = nn.SeqGRU(embeddingSize, gruHiddenSize)
    local myBwdSeq = nn.Sequential():add(nn.SeqReverseSequence(1))
    myBwdSeq:add(nn.SeqGRU(embeddingSize, gruHiddenSize))
    myBwdSeq:add(nn.SeqReverseSequence(1))
    local biconcat = nn.ConcatTable():add(myFwdSeq):add(myBwdSeq)
    local brnn = nn.Sequential():add(biconcat)  -- numWords x numSents (1) x gruHiddenSize
    brnn:add(nn.CAddTable())
    local combineSize = gruHiddenSize
    brnn:add(nn.Transpose({1,2}))
    local net = nn.Sequential()
    net:add(self.lt):add(nn.Dropout(self.embDropout)) --adding lookup table
    net:add(nn.Transpose({1,2})):add(brnn) -- gointo brnn
    if self.attn then
        net:add(self:buildSentAttn(gruHiddenSize))
    else
        net:add(nn.Max(2)) --select the second dimension
    end
    local currentSize = combineSize
    net:add(nn.Linear(currentSize, self.numLabels):noBias())
    net:add(nn.LogSoftMax())
    self.net = net
    print(self.net)
    if self.gpuid >= 0 then
        self.net:cuda()
    end
    self.params, self.grad_params = self.net:getParameters()
end


function RNNPool:buildSentAttn(hiddenSize)
    local attn = nn.Sequential()
    local firstPart = nn.Sequential():add(nn.SplitTable(1)) -- {numSents, (numWords x hiddenSize)}
    local mapTable = nn.MapTable()
    local mapOp = nn.Sequential():add(nn.Linear(hiddenSize, hiddenSize)):add(nn.Tanh())
    mapOp:add(nn.CMul(hiddenSize)):add(nn.Sum(2)):add(nn.SoftMax()):add(nn.Unsqueeze(1))
    mapTable:add(mapOp)
    firstPart:add(mapTable) -- {numSents x tensor(numWords)} -- the softmax value
    firstPart:add(nn.JoinTable(1))
    firstPart:add(nn.Replicate(hiddenSize, 3)) --numSents x numWords x hiddenSize (attentionWeight (same))
    local concat = nn.ConcatTable():add(firstPart):add(nn.Identity())
    attn:add(concat):add(nn.CMulTable()):add(nn.Sum(2))
    return attn
end