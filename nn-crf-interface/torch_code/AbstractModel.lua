--
-- Created by IntelliJ IDEA.
-- User: 1001981
-- Date: 2/12/17
-- Time: 10:38 PM
-- To change this template use File | Settings | File Templates.
--

torch.class('AbstractModel')




function AbstractModel:forward(input)
    local prediction = self.net:forward(input)
    return prediction
end

function AbstractModel:backward(input, gradOutput)
	self.net:backward(input, gradOutput)
end

function AbstractModel:zeroGrad()
	self.grad_params:zero()
end