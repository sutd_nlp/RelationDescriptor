--
-- Created by IntelliJ IDEA.
-- User: 1001981
-- Date: 7/11/17
-- Time: 1:08 PM
-- To change this template use File | Settings | File Templates.
--

torch.setdefaulttensortype('torch.DoubleTensor')

-- opt = {
--     binfilename = 'Turian_torch/Turian.6B.50d.txt',
--     outfilename = 'Turian_torch/Turian.6B.50d.t7'
-- }
-- second workstation
local embeddingFile = '/Users/allanjie/allan/data/turian_embeddings_normalized.50.t7'
local Turian = {}
-- if not paths.filep(opt.outfilename) then
-- 	Turian = require('bintot7.lua')
-- else
-- 	Turian = torch.load(opt.outfilename)
-- 	print('Done reading Turian data.')
-- end

Turian.load = function (self)
	local turianFile = embeddingFile
    --local TurianFile = 'nn-crf-interface/neural_server/goolge/TurianNews-vectors-negative300.bin.t7'
    if not paths.filep(turianFile) then
        error('Please run bintot7.lua to preprocess Turian data!')
    else
        Turian.Turian = torch.load(turianFile)
        print('Done reading Turian data.')
    end
end

Turian.distance = function (self,vec,k)
	local k = k or 1
	--self.zeros = self.zeros or torch.zeros(self.M:size(1));
	local norm = vec:norm(2)
	vec:div(norm)
	local distances = torch.mv(self.Turian.M ,vec)
	distances , oldindex = torch.sort(distances,1,true)
	local returnwords = {}
	local returndistances = {}
	for i = 1,k do
		table.insert(returnwords, self.Turian.v2wvocab[oldindex[i]])
		table.insert(returndistances, distances[i])
	end
	return {returndistances, returnwords}
end

Turian.word2vec = function (self,word,throwerror)
   local throwerror = throwerror or false
   local ind = self.Turian.w2vvocab[word]
   if throwerror then
		assert(ind ~= nil, 'Word does not exist in the dictionary!')
   end
	if ind == nil then
		ind = self.Turian.w2vvocab['*UNKNOWN*']
	end
   return self.Turian.M[ind]
end

return Turian