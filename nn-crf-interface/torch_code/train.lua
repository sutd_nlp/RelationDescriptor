--
-- Created by IntelliJ IDEA.
-- User: 1001981
-- Date: 2/12/17
-- Time: 10:40 PM
-- To change this template use File | Settings | File Templates.
--

require 'nn'
require 'rnn'
require 'optim'
include 'AbstractModel.lua'
include 'RNNPool.lua'
include 'Utils.lua'
include 'Vocab.lua'


cmd = torch.CmdLine()
cmd:text()
cmd:option('-gpuid', -1, 'GPU device id to use.')
cmd:option('-seed', 1234, 'random seed to use.')
cmd:option('-epoch', 40, 'number of epochs')
cmd:option('-attn', false, 'use attention or max pooling')
cmd:option('-emb', 'turian', 'google or turian')
cmd:option('-gruHidden', 500, 'gru hidden size')
cmd:option('-batchSize', 25, 'batch size')

opt = cmd:parse(arg)
print(opt)

torch.manualSeed(opt.seed)

local trainFile = "../../data/semeval/sd_parseTag_pi_train.txt"
local testFile = "../../data/semeval/sd_parseTag_pi_test.txt"
local embVocabPath = "../neural_server/turian/turian_vocab.txt"  --turian_vocab.txt
local embPath = "../neural_server/turian/turian_embeddings_normalized.50.t7"  --t7 file
local unkToken = "*UNKNOWN*"
local embSize = 50
if opt.emb == 'google' then
    embVocabPath = "../neural_server/google/google_vocab.txt"  --google_vocab.txt
    --second server
    embPath = "/home/allan/data/embeddings/GoogleNews-vectors-negative300.bin.t7"
    unkToken = "</s>"
    embSize = 300
end

local startToken = "<START>"
local endToken = "<END>"

local preLabelFile = "../../data/semeval/prevlabels.txt"
local e1Start = "<e1>"
local e1End = "</e1>"
local e2Start = "<e2>"
local e2End = "</e2>"
local gpuid = opt.gpuid
local attn = opt.attn
if gpuid >= 0 then
    setupGPU(gpuid, opt.seed)
end

label2id, id2label = read_pre_labels(preLabelFile)
--print(label2id)
--print(id2label)
local embVocab, turianEmb = read_embedding(embVocabPath, embPath)
train_dataset = read_raw_datasets(trainFile)
test_dataset = read_raw_datasets(testFile)

local word2idx, idx2word, vocabEmb = buildVocab(train_dataset, test_dataset, startToken, endToken, unkToken,
    embVocab, turianEmb, e1Start, e1End, e2Start, e2End, embSize)
read_sentences_into_ids_rnn(train_dataset, word2idx, startToken, endToken, unkToken, true)

read_sentences_into_ids_rnn(test_dataset, word2idx, startToken, endToken, unkToken, true)
--print(test_dataset.labels)
local epochs = opt.epoch
local train_data_size = #train_dataset.sents
local vocabSize = #idx2word
local gruHiddenSize = opt.gruHidden


myrnn = RNNPool{
	embeddingSize = embSize,
	gruHiddenSize = gruHiddenSize,
	numLabels = 19,
    gpuid = gpuid,
    vocabSize = vocabSize,
    fixEmbedding = false,
    attn = attn
}
myrnn:buildModel(vocabEmb)

local optimState = {
    learningRate = 0.01
}

criterion = nn.ClassNLLCriterion()
myrnn.net:training()  --set the state to training.
bestFscore = -1

if gpuid >=0 then
    for idx = 1, train_data_size do
        train_dataset.inputs[idx] = train_dataset.inputs[idx]:cuda()
    end
    for tidx = 1, #test_dataset.sents do
        test_dataset.inputs[tidx] = test_dataset.inputs[tidx]:cuda()
    end
    criterion:cuda()
end

print (#test_dataset.sents)

local numTrain = train_data_size
local batchSize = opt.batchSize
for e = 1, epochs do
    local epochLoss = 0
    for b = 1, numTrain, batchSize do
        xlua.progress(b, numTrain)
        local batch = math.min(b + batchSize - 1, numTrain) - b + 1
        local feval = function(param)
            myrnn:zeroGrad()
            local batchLoss = 0
            for idx = 1, batch do
                local input = train_dataset.inputs[b + idx - 1]
		        local goldY = label2id[train_dataset.labels[idx]]
		        local prediction = myrnn:forward(input)
		        local sample_loss = criterion:forward(prediction, goldY)
                batchLoss = batchLoss + sample_loss
                epochLoss = epochLoss + sample_loss
                local obj_grad = criterion:backward(prediction, goldY)
                myrnn:backward(input, obj_grad)
            end
--            batchLoss = batchLoss / batch
--            myrnn.grad_params:div(batch)
            return batchLoss, myrnn.grad_params
        end
        optim.adagrad(feval, myrnn.params, optimState)
    end
    myrnn.net:evaluate()
    if test_dataset.predLabels == nil then
        test_dataset.predLabels = {}
    end
    for tidx = 1, #test_dataset.sents do
        local testInput = test_dataset.inputs[tidx]
        local prediction = argmaxRNN(myrnn:forward(testInput), 2)
        local predLabel = id2label[prediction]
        test_dataset.predLabels[tidx] = predLabel
    end
    local fscore = evaluate(test_dataset, label2id, id2label)
    if fscore > bestFscore then
        print("get best fscore [saved]: ".. fscore)
        bestFscore = fscore
    end
    myrnn.net:training()
    print("[Info] Epoch ".. e .. " loss: "..epochLoss)
    xlua.progress(numTrain, numTrain)
end

